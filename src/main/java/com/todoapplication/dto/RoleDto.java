package com.todoapplication.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class RoleDto {

	private Long id;

	@NotEmpty(message = "Role name is required")
	@Size(min = 2, message = "Role name must contain at least 2 characters")
	private String name;

	public RoleDto(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public RoleDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
