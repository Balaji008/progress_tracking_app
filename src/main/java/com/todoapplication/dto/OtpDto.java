package com.todoapplication.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class OtpDto {

	private Long id;

	@NotEmpty(message = "email is required")
	@Size(min = 6, message = "please provide valid email")
	private String email;

	private String otp;

	private LocalDate generatedAt;

	public OtpDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OtpDto(Long id,
			@NotEmpty(message = "email is required") @Size(min = 6, message = "please provide valid email") String email,
			String otp, LocalDate generatedAt) {
		super();
		this.id = id;
		this.email = email;
		this.otp = otp;
		this.generatedAt = generatedAt;
	}

	public LocalDate getGeneratedAt() {
		return generatedAt;
	}

	public void setGeneratedAt(LocalDate generatedAt) {
		this.generatedAt = generatedAt;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

}
