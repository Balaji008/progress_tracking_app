package com.todoapplication.dto;

import javax.validation.constraints.NotEmpty;

import com.todoapplication.util.TaskStatus;

public class UserTaskDto {

	@NotEmpty(message = "User id is required")
	private Long userId;

	@NotEmpty(message = "Task id is required")
	private Long taskId;

	private TaskStatus taskStatus;

	public UserTaskDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserTaskDto(Long userId, Long taskId, TaskStatus taskStatus) {
		super();
		this.userId = userId;
		this.taskId = taskId;
		this.taskStatus = taskStatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public TaskStatus getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(TaskStatus taskStatus) {
		this.taskStatus = taskStatus;
	}

}
