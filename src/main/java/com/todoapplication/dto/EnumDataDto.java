package com.todoapplication.dto;

import javax.validation.constraints.NotNull;

import com.todoapplication.customannotations.ValidEnum;
import com.todoapplication.util.DemoEnum;

public class EnumDataDto {

	@NotNull
	@ValidEnum(enumClass = DemoEnum.class, message = "DemoEnum must be one of VALUE1, VALUE2, VALUE3")
	private DemoEnum demoEnum;

	public DemoEnum getDemoEnum() {
		return demoEnum;
	}

	public void setDemoEnum(DemoEnum demoEnum) {
		this.demoEnum = demoEnum;
	}

}
