package com.todoapplication.dto;

import javax.validation.constraints.NotEmpty;

public class UserRoleDto {

	@NotEmpty(message = "User id is required")
	private Long userId;

	@NotEmpty(message = "Role id is required")
	private Long roleId;

	public UserRoleDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserRoleDto(Long userId, Long roleId) {
		super();
		this.userId = userId;
		this.roleId = roleId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

}
