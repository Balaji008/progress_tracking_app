package com.todoapplication.dto;

public class PaymentRequestDto {

	private Integer amount;

	private String orderId;

	private String receipt;

	public PaymentRequestDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PaymentRequestDto(Integer amount, String orderId, String receipt) {
		super();
		this.amount = amount;
		this.orderId = orderId;
		this.receipt = receipt;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getReceipt() {
		return receipt;
	}

	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}
}
