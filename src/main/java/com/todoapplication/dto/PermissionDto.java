package com.todoapplication.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class PermissionDto {

	private Long id;

	@NotEmpty(message = "Permission name is required")
	@Size(min = 3, message = "Permission name must contain at least 3 characters")
	private String actionName;

	public PermissionDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PermissionDto(Long id,
			@NotEmpty(message = "Permission name is required") @Size(min = 3, message = "Permission name must contain at least 3 characters") String actionName) {
		super();
		this.id = id;
		this.actionName = actionName;
	}

	public PermissionDto(String actionName) {
		super();
		this.actionName = actionName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
}
