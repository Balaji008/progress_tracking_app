package com.todoapplication.dto;

public class FileDto {

	private Long id;

	private String video;

	private String technology;

	public FileDto(Long id, String video, String technology) {
		super();
		this.id = id;
		this.video = video;
		this.technology = technology;
	}

	public FileDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

}
