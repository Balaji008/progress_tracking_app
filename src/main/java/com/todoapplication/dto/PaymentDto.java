package com.todoapplication.dto;

import javax.validation.constraints.NotNull;

public class PaymentDto {

	@NotNull(message = "Amount is required")
	private Long amount;

	public PaymentDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PaymentDto(@NotNull(message = "Amount is required") Long amount) {
		super();
		this.amount = amount;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

}
