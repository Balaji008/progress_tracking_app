package com.todoapplication.dto;

import javax.validation.constraints.NotEmpty;

public class TaskDto {

	private Long id;

	@NotEmpty(message = "Task name is required")
	private String taskName;

	public TaskDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TaskDto(Long id, String taskName) {
		super();
		this.id = id;
		this.taskName = taskName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

}
