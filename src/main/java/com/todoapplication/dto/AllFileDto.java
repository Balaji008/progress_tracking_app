package com.todoapplication.dto;

public class AllFileDto {

	private String fileName;
	private String fileType;
	private byte[] fileBytes;

	public AllFileDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AllFileDto(String fileName, String fileType, byte[] fileBytes) {
		super();
		this.fileName = fileName;
		this.fileType = fileType;
		this.fileBytes = fileBytes;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public byte[] getFileBytes() {
		return fileBytes;
	}

	public void setFileBytes(byte[] fileBytes) {
		this.fileBytes = fileBytes;
	}
}
