package com.todoapplication.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class OtpValidateDto {

	@NotEmpty(message = "Please enter email")
	@Size(min = 6, message = "Please enter valid email")
	private String email;

	@NotEmpty(message = "Please enter otp")
	@Size(min = 6, message = "Please enter 6 digit otp")
	private String otp;

	public OtpValidateDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OtpValidateDto(
			@NotEmpty(message = "Please enter email") @Size(min = 6, message = "Please enter valid email") String email,
			@NotEmpty(message = "Please enter otp") @Size(min = 6, message = "Please enter 6 digit otp") String otp) {
		super();
		this.email = email;
		this.otp = otp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

}
