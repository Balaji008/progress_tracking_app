package com.todoapplication.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.todoapplication.util.ErrorMessageConstants;

public class UserDto {

	private long id;

	@NotEmpty(message = "Name is required")
	private String name;

	@NotEmpty(message = "Email is required")
	@Email(message = ErrorMessageConstants.INVALID_EMAIL_OR_PASSWORD)
	private String email;

	@NotEmpty(message = "Password is required")
	@Size(min = 8, message = ErrorMessageConstants.INVALID_EMAIL_OR_PASSWORD)
	private String password;

	public UserDto() {
		super();
	}

	public UserDto(long id, String name, String email, String password) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
