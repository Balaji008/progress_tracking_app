package com.todoapplication.dto;

import javax.validation.constraints.NotEmpty;

public class AsyncDataDto {

	@NotEmpty
	private Long employeeNumber;

	public AsyncDataDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AsyncDataDto(@NotEmpty Long employeeNumber) {
		super();
		this.employeeNumber = employeeNumber;
	}

	public Long getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(Long employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

}
