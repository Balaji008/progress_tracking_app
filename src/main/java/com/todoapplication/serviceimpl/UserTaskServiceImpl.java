package com.todoapplication.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.todoapplication.dto.UserTaskDto;
import com.todoapplication.entities.TaskEntity;
import com.todoapplication.entities.UserEntity;
import com.todoapplication.entities.UserTaskEntity;
import com.todoapplication.exceptions.ResourceNotFoundException;
import com.todoapplication.ilistdto.IUserTaskListDto;
import com.todoapplication.repositories.TaskRepository;
import com.todoapplication.repositories.UserRepository;
import com.todoapplication.repositories.UserTaskRepository;
import com.todoapplication.service.UserTaskService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;
import com.todoapplication.util.TaskStatus;

@Service
public class UserTaskServiceImpl implements UserTaskService {

	@Autowired
	private UserTaskRepository userTaskRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TaskRepository taskRepository;

//	@Autowired
//	private CacheOperations cacheOperations;

	@Override
	public UserTaskDto assignTaskToUser(UserTaskDto userTaskDto) {

		if (!this.userTaskRepository.existsByUserIdAndTaskId(userTaskDto.getUserId(), userTaskDto.getTaskId())) {
			UserEntity user = this.userRepository.findById(userTaskDto.getUserId())
					.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.USER_E031101,
							ErrorMessageConstants.USER_NOT_FOUND));

			TaskEntity task = this.taskRepository.findById(userTaskDto.getTaskId())
					.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.TASK_E031401,
							ErrorMessageConstants.TASK_NOT_FOUND));

			UserTaskEntity userTask = new UserTaskEntity();

			userTask.setUser(user);
			userTask.setTask(task);
			this.userTaskRepository.save(userTask);

			userTaskDto.setTaskStatus(userTask.getTaskStatus());
			return userTaskDto;
		}
		return null;
	}

	@Override
	public UserTaskDto updateTaskOfUser(UserTaskDto userTaskDto, Long id) {
		UserTaskEntity userTask = this.userTaskRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.USER_TASK_E031701,
						ErrorMessageConstants.USER_TASK_NOT_FOUND));

		UserEntity user = this.userRepository.findById(userTaskDto.getUserId())
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.USER_E031101,
						ErrorMessageConstants.USER_NOT_FOUND));

		TaskEntity task = this.taskRepository.findById(userTaskDto.getTaskId())
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.TASK_E031401,
						ErrorMessageConstants.TASK_NOT_FOUND));

		userTask.setUser(user);
		userTask.setTask(task);
		this.userTaskRepository.save(userTask);

		return userTaskDto;
	}

	@Override
	public void deleteTaskOfUser(Long id) {
		UserTaskEntity userTask = this.userTaskRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.USER_TASK_E031701,
						ErrorMessageConstants.USER_TASK_NOT_FOUND));

		this.userTaskRepository.delete(userTask);

	}

	@Override
	public List<IUserTaskListDto> updateTaskStatus(Long userId, Long taskId) {

		UserTaskEntity userTask = this.userTaskRepository.findByUserIdAndTaskId(userId, taskId);
		if (userTask != null) {
			if (userTask.getTaskStatus().getId() == 1) {
				userTask.setTaskStatus(TaskStatus.IN_PROGRESS);
				this.userTaskRepository.save(userTask);
				return this.userTaskRepository.getTasksOfUserByTaskId(taskId, userId, IUserTaskListDto.class);
			}

			else if (userTask.getTaskStatus().getId() == 2) {
				userTask.setTaskStatus(TaskStatus.COMPLETED);
				this.userTaskRepository.save(userTask);
				return this.userTaskRepository.getTasksOfUserByTaskId(taskId, userId, IUserTaskListDto.class);
			}

			else if (userTask.getTaskStatus().getId() == 3) {
				return null;
			}
		} else
			throw new ResourceNotFoundException(ErrorKeyConstants.USER_TASK_E031701,
					ErrorMessageConstants.USER_TASK_NOT_FOUND);
		return null;

	}

	@Override
	public List<IUserTaskListDto> getTasksOfAllUsers(List<String> permissions, Long userId) {

		List<IUserTaskListDto> userTasks = null;
//		String id = userId.toString();
//		cacheOperations.removeKeyFromCache(CacheConstants.USER_TASK_LIST + id);
		if (permissions.contains("IsAdmin")) {
			userTasks = this.userTaskRepository.getTasksOfAllUsers(IUserTaskListDto.class);
			return userTasks;
		}

		else {
			userTasks = this.userTaskRepository.getTasksOfUserById(userId, IUserTaskListDto.class);

		}
		return userTasks;
	}

	@Override
	public List<IUserTaskListDto> getTaskOfUserById(Long userId) {

		return null;
	}

}
