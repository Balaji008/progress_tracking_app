package com.todoapplication.serviceimpl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.todoapplication.dto.OtpDto;
import com.todoapplication.entities.OtpEntity;
import com.todoapplication.entities.UserEntity;
import com.todoapplication.exceptions.ResourceNotFoundException;
import com.todoapplication.repositories.OtpRepository;
import com.todoapplication.repositories.UserRepository;
import com.todoapplication.service.EmailService;
import com.todoapplication.service.OtpService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;
import com.todoapplication.util.OtpUtil;

@Service
public class OtpServiceImpl implements OtpService {

	@Autowired
	private EmailService emailService;

	@Autowired
	private OtpRepository otpRepository;

	@Autowired
	private UserRepository userRepository;

	@Override
	public String sendOtp(OtpDto otpDto) {

		OtpEntity otp = new OtpEntity();
		otp = this.otpRepository.findByEmailIgnoreCase(otpDto.getEmail());
		System.err.println();
//		OtpEntity otp = new OtpEntity();

		if (otp != null) {
			String otp1 = new String(OtpUtil.otp());
			otp.setOtp(otp1);
			otp.setGeneratedAt(LocalDateTime.now());
			otpRepository.save(otp);
			emailService.sendMail(OtpUtil.SUBJECT, OtpUtil.message(otp1), otpDto.getEmail());
			return otp1;

		} else {
			UserEntity user = null;
			OtpEntity otpEntity = new OtpEntity();
			user = this.userRepository.findByEmailIgnoreCase(otpDto.getEmail());
			if (user != null) {
				otpEntity.setEmail(otpDto.getEmail());
				String otp1 = new String(OtpUtil.otp());
				otpEntity.setOtp(otp1);
				otpEntity.setGeneratedAt(LocalDateTime.now());
				otpRepository.save(otpEntity);
				emailService.sendMail(OtpUtil.SUBJECT, OtpUtil.message(otp1), otpDto.getEmail());
				return otp1;
			}

		}

		return null;
	}

	@Override
	public boolean validateOtp(String email, String otp) {
		OtpEntity otpEntity = null;
		System.err.println(email);
		otpEntity = this.otpRepository.findByEmailIgnoreCase(email);
//		System.err.println(otpEntity.getEmail());
		if (otpEntity == null) {
			throw new ResourceNotFoundException(ErrorKeyConstants.OTP_E032201, ErrorMessageConstants.OTP_NOT_FOUND);
		}
		LocalDateTime currentTime = LocalDateTime.now();
		LocalDateTime otpExpirationTime = otpEntity.getGeneratedAt().plusMinutes(15); // OTP expiration time
		if (currentTime.isAfter(otpExpirationTime)) {
			// OTP has expired
			otpEntity.setOtp(""); // Clear the OTP
			this.otpRepository.save(otpEntity); // Delete the OTP entity from the database
			return false;
		}

		else if (otp.equalsIgnoreCase(otpEntity.getOtp())) {
			otpEntity.setOtp("");
			this.otpRepository.save(otpEntity);
			return true;

		}
		return false;
	}

}
