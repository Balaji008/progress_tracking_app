package com.todoapplication.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.todoapplication.entities.UserEntity;
import com.todoapplication.ilistdto.IListUserDto;
import com.todoapplication.repositories.UserRepository;
import com.todoapplication.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<IListUserDto> getAllUsers() {

		List<IListUserDto> users = this.userRepository.findByOrderByIdDesc(IListUserDto.class);
		return users;
	}

	@Override
	public List<IListUserDto> getUserById(Long userId) {
		List<IListUserDto> user = this.userRepository.findById(userId, IListUserDto.class);
		return user;
	}

	@Override
	public void deleteUser(Long userId) {
		UserEntity user = this.userRepository.findById(userId).orElseThrow();

		this.userRepository.delete(user);
	}


}
