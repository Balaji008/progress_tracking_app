package com.todoapplication.serviceimpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.todoapplication.dto.FileDto;
import com.todoapplication.entities.FileEntity;
import com.todoapplication.exceptions.ResourceNotFoundException;
import com.todoapplication.repositories.FileRepository;
import com.todoapplication.service.FileService;

@Service
public class FileServiceImpl implements FileService {

	@Autowired
	private FileRepository fileRepository;

	@Override
	public String addFile(String path, MultipartFile file) throws Exception {
		FileEntity fileEntity = new FileEntity();
		String name = file.getOriginalFilename();

		// Full Path
		String filePath = path + File.separator + name;

//    // Set content type to video/mp4
//    String contentType = "video/mp4";

		File f = new File(path);

		if (!f.exists()) {
			f.mkdir();
		}

		try {
			// Save file to specified path
			Files.copy(file.getInputStream(), Paths.get(filePath), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			throw new Exception("File upload exception");
		}

		fileEntity.setVideo(name);

		this.fileRepository.save(fileEntity);

		return name;
	}

	@Override
	public InputStream getFile(Long id, String path) {
		FileEntity fileEntity = this.fileRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("FileEntity not found", "FileEntity not found"));
		String fullPath = path + File.separator + fileEntity.getVideo();

		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(fullPath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return inputStream;
	}

	@Override
	public List<InputStream> getFiles(int pageNumber, int pageSize, String path) {
		Pageable pageable = PageRequest.of(pageNumber, pageSize);

		Page<FileEntity> fileEntities = this.fileRepository.findAll(pageable);

		List<InputStream> files = new ArrayList<>();

		for (FileEntity fileEntity : fileEntities) {
			String fullPath = path + File.separator + fileEntity.getVideo();

			try {
				InputStream inputStream = new FileInputStream(fullPath);
				files.add(inputStream);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		return files;
	}

	@Override
	public List<byte[]> getFileBytesList(Integer pageNumber, Integer pageSize, String path) {
		Pageable pageable = PageRequest.of(pageNumber, pageSize);

		Page<FileEntity> fileEntities = this.fileRepository.findAll(pageable);
		System.out.println(fileEntities);
		List<byte[]> fileBytesList = new ArrayList<>();

		for (FileEntity fileEntity : fileEntities) {
			String fullPath = path + File.separator + fileEntity.getVideo();

			try {
				byte[] fileBytes = Files.readAllBytes(Paths.get(fullPath));
				fileBytesList.add(fileBytes);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return fileBytesList;
	}

	@Override
	public FileDto getFileDto(Long id) {
		FileEntity fileEntity = this.fileRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("FileEntity not found", "FileEntity not found"));

		FileDto fileDto = new FileDto();
		fileDto.setId(id);
		fileDto.setTechnology(fileEntity.getTechnology());
		fileDto.setVideo(fileEntity.getVideo());
		return null;
	}

	@Override
	public FileDto addFile(FileDto fileDto, String path, MultipartFile file) {
		FileEntity fileEntity = new FileEntity();
		String name = file.getOriginalFilename();

		// Full Path
		String filePath = path + File.separator + name;

//    // Set content type to video/mp4
//    String contentType = "video/mp4";
		File f = new File(path);

		if (!f.exists()) {
			f.mkdir();
		}
		try {
			// Save file to specified path
			Files.copy(file.getInputStream(), Paths.get(filePath), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}

		fileEntity.setVideo(name);
		fileEntity.setTechnology(fileDto.getTechnology());
		this.fileRepository.save(fileEntity);
		fileDto.setId(fileEntity.getId());
		fileDto.setVideo(name);

		return fileDto;
	}

}
