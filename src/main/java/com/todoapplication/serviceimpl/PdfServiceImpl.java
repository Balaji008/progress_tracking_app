package com.todoapplication.serviceimpl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.todoapplication.service.PdfService;

@Service
public class PdfServiceImpl implements PdfService {

	@Override
	public ByteArrayInputStream createPdf() {
		String title = "Savitribai Phule Pune University";
		String content = "Application for Regular Convocation Degree Certificate";
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		Document document = new Document();

		try {
			PdfWriter.getInstance(document, out);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		document.open();
		Font titleFont = FontFactory.getFont(FontFactory.TIMES_ROMAN, 20, BaseColor.BLACK);
		Paragraph titlePara = new Paragraph(title, titleFont);
		titlePara.setAlignment(Element.ALIGN_CENTER);

		Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN, 15, BaseColor.BLACK);
		Paragraph paragraph = new Paragraph(content, font);
		paragraph.setAlignment(Element.ALIGN_LEFT);

		try {
			document.add(titlePara);
			document.add(paragraph);
			document.add(new Paragraph("Application ID: 23047232"));
			document.add(new Paragraph("College / Institute Details"));
			document.add(new Paragraph("College Pun Code: CEGP019340"));
			document.add(new Paragraph(
					"Name of College/Institute: Dr. D. Y. Patil Educational Academy Dr. D.Y. Patil School Of Engineering Academy"));
			document.add(new Paragraph("Addr: Gut No 124, 126, Ambi, Talegaon Dabhade, Taluka Maval, District Pune"));
			document.add(new Paragraph("Ta: Mawal Dist: Pune"));
			document.add(new Paragraph("Examination: B.E. (Mechanical) Faculty: Engineering"));
			document.add(new Paragraph("Month of Examination: May Year of Examination: 2022"));
			document.add(new Paragraph("Seat No.: B151070853 PRN ( If Any ): 72010883H"));
			document.add(new Paragraph("Special/Principle/Branch/Method: 0"));
			document.add(new Paragraph("Optional or Subsidiary ( If Any ) a]: "));
			document.add(new Paragraph("b]: "));
			document.add(new Paragraph("Class or Grade Obtained: First Class with Distinction"));
			document.add(new Paragraph("Personal Information"));
			document.add(new Paragraph("Full Name: GOSAVI BALAJI SHIVRAJ"));
			document.add(new Paragraph("Mother's Name: GOSAVI MEENA SHIVRAJ"));
			document.add(new Paragraph("Gender: Male"));
			document.add(new Paragraph("Contact No: 8862084507 E-mail: balajigosavi008@gmail.com"));
			document.add(new Paragraph("Correspondence Address"));
			document.add(new Paragraph("Sonal Residency Keshavnagar Vadgaon Maval Pincode 412106"));
			document.add(new Paragraph("*23047232* 23047232"));
			document.add(new Paragraph("Declaration"));
			document.add(new Paragraph("Name in Devnagari Script:"));
			document.add(new Paragraph("Mother's Name: Ȫ ȡȢȢȡͧȡ Ȫ ȡȢȡȡȢͧȡ"));

		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();

		return new ByteArrayInputStream(out.toByteArray());
	}

	@Autowired
	private SpringTemplateEngine templateEngine;

	@Override
	public String renderHtml(Model model) {
		Context context = new Context();
		context.setVariables(model.asMap());
		return templateEngine.process("infotemplate", context);
	}

//	@Override
//	public MultipartFile generatePdf(String html, HttpServletResponse response) throws Exception {
//		ITextRenderer renderer = new ITextRenderer();
//		renderer.setDocumentFromString(html);
//		renderer.layout();
//		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//		renderer.createPDF(outputStream);
//		outputStream.close();
//		byte[] bytes = outputStream.toByteArray();
//
//		FileItem fileItem = new DiskFileItem("certificate.pdf", "application/pdf", false, "certificate.pdf", bytes);
////		FileItemHeaders headers = new FileItemHeadersSupport();
////		headers.setHeader("Content-Type", "application/pdf");
////		headers.setHeader("Content-Disposition", "attachment; filename=certificate.pdf");
////		fileItem.setHeaders(headers);
//		CommonsMultipartFile multipartFile = new CommonsMultipartFile(fileItem);
//		return multipartFile;
//	}

	@Override
	public void generatePdf(String html, HttpServletResponse response) throws Exception {
		ITextRenderer renderer = new ITextRenderer();
		renderer.setDocumentFromString(html);
		renderer.layout();
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename=certificate.pdf");
		ServletOutputStream outputStream = response.getOutputStream();

		renderer.createPDF(outputStream);
		outputStream.close();
	}

}
