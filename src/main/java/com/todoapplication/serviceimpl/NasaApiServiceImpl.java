package com.todoapplication.serviceimpl;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.todoapplication.payloads.KafkaConstants;
import com.todoapplication.util.NasaIssLocation;
import com.todoapplication.util.NasaResponse;

@Service
public class NasaApiServiceImpl {

	@Autowired
	private KafkaTemplate<String, String> template;

	@Value("${nasa.api.key}")
	private String apiKey;

	public NasaResponse getNasaData() {
		RestTemplate restTemplate = new RestTemplate();
		String url = "https://api.nasa.gov/planetary/apod?api_key=" + apiKey;
		ResponseEntity<NasaResponse> response = restTemplate.getForEntity(url, NasaResponse.class);
		return response.getBody();
	}

//	@Scheduled(fixedDelay = 5000) // call the API every 5 seconds
	public void callNasaApi() {
		String url = "http://api.open-notify.org/iss-now.json";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			NasaIssLocation readValue = objectMapper.readValue(response.getBody(), NasaIssLocation.class);
			String value = readValue.toString();
			ListenableFuture<SendResult<String, String>> send = template.send(KafkaConstants.TOPIC, value);
			send.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
				@Override
				public void onSuccess(SendResult<String, String> result) {
					System.out.println("Data sent to Kafka: " + result.getProducerRecord().value().toString());
				}

				@Override
				public void onFailure(Throwable ex) {
					System.err.println("Error sending data to Kafka: " + ex.getMessage());
				}
			});

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
}
