package com.todoapplication.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.todoapplication.dto.RolePermissionDto;
import com.todoapplication.entities.PermissionEntity;
import com.todoapplication.entities.RoleEntity;
import com.todoapplication.entities.RolePermissionEntity;
import com.todoapplication.entities.UserRoleEntity;
import com.todoapplication.exceptions.ResourceNotFoundException;
import com.todoapplication.ilistdto.IRolePermissionListDto;
import com.todoapplication.repositories.PermissionRepository;
import com.todoapplication.repositories.RolePermissionRepository;
import com.todoapplication.repositories.RoleRepository;
import com.todoapplication.repositories.UserRoleRepository;
import com.todoapplication.service.RolePermissionService;
import com.todoapplication.util.CacheConstants;
import com.todoapplication.util.CacheOperations;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;

@Service
public class RolePermissionServiceImpl implements RolePermissionService {

	@Autowired
	private RolePermissionRepository rolePermissionRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PermissionRepository permissionRepository;

	@Autowired
	private CacheOperations cacheOperations;

	@Autowired
	private UserRoleRepository userRoleRepository;

	@Override
	public IRolePermissionListDto assignPermissionsToRole(RolePermissionDto rolePermissionDto) {
		if (!rolePermissionRepository.existsByRoleIdAndPermissionId(rolePermissionDto.getRoleId(),
				rolePermissionDto.getPermissionId())) {
			PermissionEntity permission = this.permissionRepository.findById(rolePermissionDto.getPermissionId())
					.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.PERMISSION_E031301,
							ErrorMessageConstants.PERMISSION_NOT_FOUND));

			RoleEntity role = this.roleRepository.findById(rolePermissionDto.getRoleId())
					.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.ROLE_E031201,
							ErrorMessageConstants.ROLE_NOT_FOUND));

			RolePermissionEntity rolePermission = new RolePermissionEntity();
			rolePermission.setRole(role);
			rolePermission.setPermission(permission);

			List<UserRoleEntity> userRole = this.userRoleRepository.findUserIdsByRoleId(role.getId());

			userRole.forEach(ur -> {

				String userId = ur.getUser().getId().toString();
				System.err.println(ur);
				String key = CacheConstants.PERMISSIONS.concat(userId);
				System.err.print(key);
				cacheOperations.removeKeyFromCache(key);
			});
			this.rolePermissionRepository.save(rolePermission);
			System.out.println(rolePermission.getRole().getId() + "\t" + rolePermission.getPermission().getId());

			IRolePermissionListDto rolePermissionName = this.rolePermissionRepository
					.getAllPermissionsByRolePermissionId(rolePermission.getId(), IRolePermissionListDto.class);

			return rolePermissionName;
		}
		return null;
	}

	@Override
	public IRolePermissionListDto updatePermissionsOfRole(RolePermissionDto rolePermissionDto, Long id) {
		RolePermissionEntity rolePermission = this.rolePermissionRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.ROLE_PERMISSION_E031601,
						ErrorMessageConstants.ROLE_PERMISSION_NOT_FOUND));

		PermissionEntity permission = this.permissionRepository.findById(rolePermissionDto.getPermissionId())
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.PERMISSION_E031301,
						ErrorMessageConstants.PERMISSION_NOT_FOUND));

		RoleEntity role = this.roleRepository.findById(rolePermissionDto.getRoleId())
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.ROLE_E031201,
						ErrorMessageConstants.PERMISSION_NOT_FOUND));

		rolePermission.setRole(role);
		rolePermission.setPermission(permission);

		List<UserRoleEntity> userRole = this.userRoleRepository.findUserIdsByRoleId(role.getId());

		userRole.forEach(ur -> {

			String userId = ur.getUser().getId().toString();
			System.err.println(ur);
			String key = CacheConstants.PERMISSIONS.concat(userId);
			System.err.print(key);
			cacheOperations.removeKeyFromCache(key);
		});
		this.rolePermissionRepository.save(rolePermission);
		IRolePermissionListDto rolePermissionName = this.rolePermissionRepository
				.getAllPermissionsByRolePermissionId(rolePermission.getId(), IRolePermissionListDto.class);
		return rolePermissionName;
	}

	@Override
	public void deletePermissionOfRole(Long id) {
		RolePermissionEntity rolePermission = this.rolePermissionRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.ROLE_PERMISSION_E031601,
						ErrorMessageConstants.ROLE_PERMISSION_NOT_FOUND));
		this.rolePermissionRepository.delete(rolePermission);
	}

	@Override
	public List<IRolePermissionListDto> getAllPermissionsOfRole() {
		List<IRolePermissionListDto> rolePermissions = this.rolePermissionRepository
				.getAllPermissionsOfRoles(IRolePermissionListDto.class);
		System.out.println(rolePermissions);
		return rolePermissions;
	}

	@Override
	public IRolePermissionListDto getAllPermissionsByRoleId(Long roleId) {
		IRolePermissionListDto rolePermissions = this.rolePermissionRepository.getAllPermissionsByRoleId(roleId,
				IRolePermissionListDto.class);
		return rolePermissions;
	}

}
