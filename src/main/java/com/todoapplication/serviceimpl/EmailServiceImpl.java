package com.todoapplication.serviceimpl;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.todoapplication.service.EmailService;

@Service
public class EmailServiceImpl implements EmailService {

	@Value("${spring.mail.host}")
	private String host;

	@Value("${spring.mail.port}")
	private int port;

	@Value("${spring.mail.username}")
	private String username;

	@Value("${spring.mail.password}")
	private String password;

	private final static String from = "balaji995588@gmail.com";

	private final static Logger LOG = LoggerFactory.getLogger(EmailServiceImpl.class);

//	@Override
//	public String sendMail(String subject, String message, String to) {
//		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
//		simpleMailMessage.setFrom(from);
//		simpleMailMessage.setTo(to);
//		// simpleMailMessage.setSubject("Apply sucessfully");
//		simpleMailMessage.setText(message);
//		javaMailSender.send(simpleMailMessage);
//		return "Email Send";
//	}

	@Override
	public boolean sendMail(String subject, String message, String to) {
		boolean flag = false;

		// get the system properties
		LOG.debug("Email service");
		Properties properties = System.getProperties();

		// set information to properties
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.ssl.protocols", "TLSv1.2");
//		properties.put("mail.debug", "true");
		// step 1 : to get session object
		Session session = Session.getInstance(properties, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		// step2 : Compose the message
		MimeMessage message2 = new MimeMessage(session);

		try {
			message2.setFrom(from);
			message2.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message2.setSubject(subject);
			message2.setContent(message, "text/html");

			// step 3: send the message using transport class
			Transport.send(message2);

			flag = true;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return flag;
	}
//	}

}
