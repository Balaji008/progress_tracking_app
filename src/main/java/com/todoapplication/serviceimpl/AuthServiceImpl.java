package com.todoapplication.serviceimpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.todoapplication.configurations.JwtTokenUtil;
import com.todoapplication.dto.UserDto;
import com.todoapplication.entities.UserEntity;
import com.todoapplication.exceptions.InvalidEmailOrPasswordException;
import com.todoapplication.exceptions.ResourceNotFoundException;
import com.todoapplication.payloads.JwtAuthenticationRequest;
import com.todoapplication.payloads.JwtAuthenticationResponse;
import com.todoapplication.repositories.RolePermissionRepository;
import com.todoapplication.repositories.UserRepository;
import com.todoapplication.service.AuthService;
import com.todoapplication.util.CacheConstants;
import com.todoapplication.util.CacheOperations;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;

@Service
public class AuthServiceImpl implements AuthService {

	@Autowired
	private CacheOperations cacheOperations;

	@Autowired
	private UserRepository userRepository;

	@Autowired(required = true)
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;
//	= new JwtTokenUtil();

	@Autowired
	private RolePermissionRepository rolePermissionRepository;

	public Collection<? extends GrantedAuthority> getAuthorities(Long userId) {

		List<String> permissions = this.permissions(userId);
//		System.err.println("This is SimpleGrantedAuthority permissions");
		List<SimpleGrantedAuthority> authority = new ArrayList<SimpleGrantedAuthority>();
		;

		permissions.forEach(a -> {
			authority.add(new SimpleGrantedAuthority("ROLE_" + a));
		});

		return authority;
	}

	@Override
	public UserDto registerUser(UserDto userDto) {

		UserEntity user = new UserEntity();

		user.setEmail(userDto.getEmail());

		user.setName(userDto.getName());

		user.setPassword(this.passwordEncoder.encode(userDto.getPassword()));

		this.userRepository.save(user);

		userDto.setId(user.getId());

		return userDto;
	}

	@Override
	public JwtAuthenticationResponse login(JwtAuthenticationRequest request) {

		UserEntity user = this.userInformation(request.getUsername());

		JwtAuthenticationResponse response = new JwtAuthenticationResponse();

		if (user.getEmail() == null) {
			throw new ResourceNotFoundException(ErrorKeyConstants.USER_E031101, ErrorMessageConstants.USER_NOT_FOUND);
		}

		if (comparePassword(request.getPassword(), user.getPassword())) {
			final UserDetails userDetails = loadUserByUsername(request.getUsername());

			final String accessToken = this.jwtTokenUtil.generateAccessToken(userDetails);

			final String refreshToken = this.jwtTokenUtil.generateRefreshToken(accessToken, userDetails);

			response.setAccessToken(accessToken);
			response.setRefreshToken(refreshToken);
			response.setPermissions(this.permissions(user.getId()));
			return response;
		}

		else {
			response.setAccessToken(null);
			response.setRefreshToken(null);
			response.setPermissions(null);
			return response;
		}
	}

	@Override
	public JwtAuthenticationResponse loginWithOtp(String email) {
		JwtAuthenticationResponse response = new JwtAuthenticationResponse();
		UserEntity user = this.userInformation(email);
		final UserDetails userDetails = loadUserByUsername(email);

		final String accessToken = this.jwtTokenUtil.generateAccessToken(userDetails);

		final String refreshToken = this.jwtTokenUtil.generateRefreshToken(accessToken, userDetails);

		response.setAccessToken(accessToken);
		response.setRefreshToken(refreshToken);
		response.setPermissions(this.permissions(user.getId()));
		return response;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		UserEntity user = this.userInformation(username);

		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
				this.getAuthorities(user.getId()));
	}

	private Boolean comparePassword(String password, String hashPassword) {

		return passwordEncoder.matches(password, hashPassword);

	}

	@Override
	public String refreshAndGetAuthenticationToken(String refreshToken) throws Exception {
		try {
			String email = this.jwtTokenUtil.getUsernameFromToken(refreshToken);
			UserEntity user = this.userInformation(email);
			if (user != null) {

				final UserDetails userDetails = loadUserByUsername(email);

				if (this.jwtTokenUtil.canTokenBeRefreshed(refreshToken)
						&& this.jwtTokenUtil.validateToken(refreshToken, userDetails)
						&& this.jwtTokenUtil.getTokenType(refreshToken).equalsIgnoreCase("refresh")) {
					String accessToken = this.jwtTokenUtil.generateAccessToken(userDetails);

					return accessToken;
				}
			}
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> permissions(Long userId) {
		List<String> permissions;
		String id = userId.toString();
		String key = CacheConstants.PERMISSIONS.concat(id);
		// cacheOperations.removeKeyFromCache(key);
//		System.err.println(key);
		if (cacheOperations.isKeyExist(key, id)) {
			permissions = (List<String>) cacheOperations.getFromCache(key, id);
//			System.err.println("This is cache from permissions ");
//			System.err.println(permissions);
		} else {

			permissions = this.rolePermissionRepository.getAllPermissionsOfUsers(userId);
//			System.out.println("This is non cache permissions");
//			System.out.println(permissions);
			cacheOperations.addInCache(key, id, permissions);
			cacheOperations.putValueInHashWithExpiration(key, id, permissions, 12, TimeUnit.HOURS);
		}
		return permissions;
	}

	@Override
	public UserEntity userInformation(String email) {
		UserEntity user = new UserEntity();

		if (cacheOperations.isKeyExist(CacheConstants.USER + email, email)) {
			String jsonString = (String) cacheOperations.getFromCache(CacheConstants.USER + email, email);

			try {
				ObjectMapper mapper = new ObjectMapper();
				TypeReference<Map<String, Object>> typeReference = new TypeReference<Map<String, Object>>() {
				};
				Map<String, Object> map = mapper.readValue(jsonString, typeReference);
//				System.err.println(map.toString());
				user.setId(((Integer) map.get("id")).longValue());
				user.setEmail((String) map.get("email"));
				user.setPassword((String) map.get("password"));
//				System.err.println("User details from cache");
			} catch (Exception e) {
				e.printStackTrace();
			}
//			
		}

		else if (!this.userRepository.existsByEmail(email)) {
			throw new InvalidEmailOrPasswordException(ErrorKeyConstants.USER_E031105,
					ErrorMessageConstants.INVALID_EMAIL_OR_PASSWORD);
		}

		else {

			user = this.userRepository.findByEmailIgnoreCase(email);
//			cacheOperations.addInCache(CacheConstants.USER + email, email, user.toString());
			cacheOperations.putValueInHashWithExpiration(CacheConstants.USER + email, email, user.toString(), 12,
					TimeUnit.HOURS);

		}

		return user;
	}

}
