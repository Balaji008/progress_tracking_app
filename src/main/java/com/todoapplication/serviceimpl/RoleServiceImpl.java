package com.todoapplication.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.todoapplication.dto.RoleDto;
import com.todoapplication.entities.RoleEntity;
import com.todoapplication.exceptions.ResourceNotFoundException;
import com.todoapplication.ilistdto.IRoleListDto;
import com.todoapplication.repositories.RoleRepository;
import com.todoapplication.service.RoleService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public RoleDto createRole(RoleDto roleDto) {
		if (!this.roleRepository.existsByName(roleDto.getName())) {
			RoleEntity role = new RoleEntity();
			role.setName(roleDto.getName());
			this.roleRepository.save(role);

			RoleDto createdRole = new RoleDto();
			createdRole.setId(role.getId());
			createdRole.setName(role.getName());
			return createdRole;
		}
		return null;
	}

	@Override
	public RoleDto updateRole(RoleDto roleDto, Long roleId) {
		RoleEntity role = this.roleRepository.findById(roleId)
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.ROLE_E031201,
						ErrorMessageConstants.ROLE_NOT_FOUND));

		role.setName(roleDto.getName());
		this.roleRepository.save(role);

		RoleDto updatedRole = new RoleDto();
		updatedRole.setId(roleId);
		updatedRole.setName(role.getName());
		return updatedRole;
	}

	@Override
	public IRoleListDto getRoleById(Long roleId) {
		IRoleListDto role = null;
		role = this.roleRepository.findById(roleId, IRoleListDto.class);

		if (role == null) {
			throw new ResourceNotFoundException(ErrorKeyConstants.ROLE_E031201, ErrorMessageConstants.ROLE_NOT_FOUND);
		}
		return role;
	}

	@Override
	public List<IRoleListDto> getAllRoles() {

		List<IRoleListDto> roles = this.roleRepository.findByOrderByIdDesc(IRoleListDto.class);
		return roles;
	}

	@Override
	public void deleteRole(Long roleId) {
		RoleEntity role = this.roleRepository.findById(roleId)
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.ROLE_E031201,
						ErrorMessageConstants.ROLE_NOT_FOUND));

		this.roleRepository.delete(role);
	}

}
