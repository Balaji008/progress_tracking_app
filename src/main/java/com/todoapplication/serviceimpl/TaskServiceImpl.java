package com.todoapplication.serviceimpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.todoapplication.dto.TaskDto;
import com.todoapplication.entities.TaskEntity;
import com.todoapplication.exceptions.ResourceNotFoundException;
import com.todoapplication.ilistdto.ITaskListDto;
import com.todoapplication.repositories.TaskRepository;
import com.todoapplication.service.TaskService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;
import com.todoapplication.util.TaskUploadUtil;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	private TaskRepository taskRepository;

	@Override
	public TaskDto createTask(TaskDto taskDto) {
		if (!this.taskRepository.existsByTaskNameEqualsIgnoreCase(taskDto.getTaskName())) {
			TaskEntity task = new TaskEntity();
			task.setTaskName(taskDto.getTaskName());
			this.taskRepository.save(task);
			taskDto.setId(task.getId());
			return taskDto;
		}
		return null;
	}

	@Override
	public TaskDto updateTask(TaskDto taskDto, Long id) {
		TaskEntity task = this.taskRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.TASK_E031401,
						ErrorMessageConstants.TASK_NOT_FOUND));
		task.setTaskName(taskDto.getTaskName());
		taskDto.setId(id);
		return taskDto;
	}

	@Override
	public void deleteTask(long id) {
		TaskEntity task = this.taskRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.TASK_E031401,
						ErrorMessageConstants.TASK_NOT_FOUND));

		this.taskRepository.delete(task);
	}

	@Override
	public List<ITaskListDto> getAllTasks() {
		List<ITaskListDto> userTasks = this.taskRepository.findByOrderByIdDesc(ITaskListDto.class);
		return userTasks;
	}

	@Override
	public List<TaskDto> save(MultipartFile file) {
		try {
			List<TaskDto> tasks = TaskUploadUtil.convertExcelToListOfTasks(file.getInputStream());
			List<TaskDto> taskList = new ArrayList<>();
			tasks.forEach(t -> {
				TaskEntity task = new TaskEntity();
				TaskDto taskDto = new TaskDto();
				if (!this.taskRepository.existsByTaskNameEqualsIgnoreCase(t.getTaskName())) {
					task.setTaskName(t.getTaskName());
					this.taskRepository.save(task);
					taskDto.setId(task.getId());
					taskDto.setTaskName(task.getTaskName());
					taskList.add(taskDto);
				}

			});

			return taskList;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}

	}

}
