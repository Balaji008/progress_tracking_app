package com.todoapplication.serviceimpl;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class FootballMatches {

	private static final String API_BASE_URL = "https://jsonmock.hackerrank.com/api/football_matches";

	private CloseableHttpClient httpClient;
	private Gson gson;

	@BeforeClass
	public void setUp() {
		httpClient = HttpClients.createDefault();
		gson = new Gson();
	}

	@Test
	public void testGetNumDraws() {
		int year = 2011;
		int numDraws = getNumDraws(year);
		System.out.println("Number of draws in " + year + ": " + numDraws);
		Assert.assertTrue(numDraws >= 0);
	}

	public int getNumDraws(int year) {
		int totalDraws = 0;
		int currentPage = 1;
		int totalPages;

		do {
			// Construct the API URL for the given year and page
			String apiUrl = API_BASE_URL + "?year=" + year + "&page=" + currentPage;

			// Make an HTTP GET request using HTTPClient (HTTPCore)
			try {
				String responseBody = executeGetRequest(apiUrl);

				// Parse the JSON response using JSON Simple library
				int drawsOnPage = parseDrawsFromJson(responseBody);

				// Update the total draws count
				totalDraws += drawsOnPage;

				// Get the total number of pages
				totalPages = parseTotalPagesFromJson(responseBody);

				// Move to the next page
				currentPage++;

			} catch (Exception e) {
				e.printStackTrace();
				break; // Stop the loop in case of an error
			}

		} while (currentPage <= totalPages);

		return totalDraws;
	}

	private String executeGetRequest(String url) throws Exception {
		HttpGet httpGet = new HttpGet(url);
		try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
			HttpEntity entity = response.getEntity();
			return EntityUtils.toString(entity);
		}
	}

	private int parseDrawsFromJson(String json) {
		// Use JSON Simple library to parse JSON
		try {
			JSONObject jsonObject = (JSONObject) new JSONParser().parse(json);
			return Integer.parseInt(jsonObject.get("total").toString());
		} catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}
	}

	private int parseTotalPagesFromJson(String json) {
		// Use Gson library to parse JSON
		try {
			JsonObject jsonObject = gson.fromJson(json, JsonObject.class);
			return jsonObject.get("total_pages").getAsInt();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
}
