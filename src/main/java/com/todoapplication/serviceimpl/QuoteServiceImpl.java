package com.todoapplication.serviceimpl;

import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.todoapplication.util.QuoteOfTheDay;

@Service
public class QuoteServiceImpl {

	public QuoteOfTheDay getQuoteOfTheDay() {
		try {
			String url = "https://favqs.com/api/qotd";
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
			ObjectMapper objectMapper = new ObjectMapper();
			QuoteOfTheDay readValue = objectMapper.readValue(response.getBody(), QuoteOfTheDay.class);
//			return objectMapper.readValue(response.getBody(), QuoteOfTheDay.class);
			return readValue;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
