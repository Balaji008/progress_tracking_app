package com.todoapplication.serviceimpl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.todoapplication.entities.InviteEntity;
import com.todoapplication.repositories.InviteRepository;
import com.todoapplication.service.InviteService;

@Service
@Transactional
public class InviteServiceImpl implements InviteService {

	@Autowired
	private InviteRepository inviteRepository;

	@Override
	public InviteEntity add(String uuid, String email) {
		InviteEntity entity = new InviteEntity();
		entity.setCode(uuid);
		entity.setEmail(email);
		InviteEntity inviteEntity = this.inviteRepository.save(entity);
		return inviteEntity;
	}

	@Override
	public void deleteByEmail(String mail) throws Exception {
		this.inviteRepository.deleteByEmail(mail);
	}

}
