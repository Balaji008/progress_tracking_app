package com.todoapplication.serviceimpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.io.CharStreams;
import com.todoapplication.service.ZoomApiService;

@Service
public class ZoomApiServiceImpl implements ZoomApiService {

	@Value("${Zoom.key}")
	private String apiKey;

	@Value("${Zoom.secret}")
	private String apiSecret;

	@Override
	public String createMeeting(String topic, String startTime, int duration, String token) throws Exception {
		String url = "https://api.zoom.us/v2/users/me/meetings";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("POST");

		// Set Authorization header
		String auth = apiKey + ":" + apiSecret;
		byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.UTF_8));
		String authHeaderValue = "Basic " + new String(encodedAuth);
		con.setRequestProperty("Authorization", authHeaderValue);

		con.setRequestProperty("Content-Type", "application/json");

		String data = "{\"topic\": \"" + topic + "\", \"type\": 2, \"start_time\": \"" + startTime
				+ "\", \"duration\": " + duration + "}";

		// Send POST request
		con.setDoOutput(true);
		con.getOutputStream().write(data.getBytes(StandardCharsets.UTF_8));

		// Read response
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String response = CharStreams.toString(in);

		return response;
	}

}
