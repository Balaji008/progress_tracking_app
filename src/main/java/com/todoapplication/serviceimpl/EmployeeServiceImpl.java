package com.todoapplication.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.todoapplication.dto.EmployeeDto;
import com.todoapplication.entities.EmployeeEntity;
import com.todoapplication.repositories.EmployeeRepository;
import com.todoapplication.service.EmployeeService;
import com.todoapplication.util.EmployeeBulkUploadUtil;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public List<EmployeeDto> uploadCsvFile(MultipartFile file) {
		try {
			List<EmployeeDto> employees = EmployeeBulkUploadUtil.csvToListOfEmployee(file);

			employees.forEach(e -> {
				EmployeeEntity employee = new EmployeeEntity();
				employee.setFirstName(e.getFirstName());
				employee.setHireDate(e.getHireDate());
				employee.setJobId(e.getJobId());
				employee.setSalary(e.getSalary());
				employee.setLastName(e.getLastName());
				this.employeeRepository.save(employee);
				e.setId(employee.getId());
			});
			return employees;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}

	}

}
