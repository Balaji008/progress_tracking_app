package com.todoapplication.serviceimpl;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.zip.Deflater;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.todoapplication.configurations.JwtTokenUtil;
import com.todoapplication.entities.InviteEntity;
import com.todoapplication.entities.UserEntity;
import com.todoapplication.exceptions.ResourceNotFoundException;
import com.todoapplication.payloads.Constants;
import com.todoapplication.repositories.InviteRepository;
import com.todoapplication.repositories.UserRepository;
import com.todoapplication.service.AuthService;
import com.todoapplication.service.SAMLService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;

@Component
public class SamlService implements SAMLService {

	@Value("${sso.tenant-id}")
	private String TENANT_ID;

	@Value("${sso.assertion-service-url}")
	private String ASSERTION_URL;

	@Value("${sso.issuer}")
	private String ISSUER;

	@Value("${saml.certificate}")
	private String CERTIFICATE;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private InviteRepository inviteRepository;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	private String redirectUrl;

	@Override
	public String generateRedirectUrl() {
		redirectUrl = "https://login.microsoftonline.com/" + TENANT_ID + "/saml2" + "?SAMLRequest=";
		String samlRequest = "<samlp:AuthnRequest xmlns:samlp=\"urn:oasis:names:tc:SAML:2.0:protocol\" "
				+ "xmlns:saml=\"urn:oasis:names:tc:SAML:2.0:assertion\" " + "ID=\"" + "_" + UUID.randomUUID().toString()
				+ "\" " + "Version=\"2.0\" " + "IssueInstant=\"" + DateTime.now() + "\" "
				+ "ProtocolBinding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\" "
				+ "AssertionConsumerServiceURL=\"" + ASSERTION_URL + "\">" + "<saml:Issuer>" + ISSUER + "</saml:Issuer>"
				+ "<samlp:NameIDPolicy Format=\"urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified\"/>"
				+ "</samlp:AuthnRequest>";

		byte[] compressedBytes = new byte[samlRequest.length()];
		Deflater deflater = new Deflater(Deflater.DEFAULT_COMPRESSION, true);
		deflater.setInput(samlRequest.getBytes());
		deflater.finish();
		int compressedSize = deflater.deflate(compressedBytes);
		deflater.end();

		String base64Encoded = Base64.getEncoder()
				.encodeToString(Arrays.copyOfRange(compressedBytes, 0, compressedSize));

		String encodedString = UriUtils.encode(base64Encoded, StandardCharsets.UTF_8);
		String relayStateString = redirectUrl + encodedString;
		return relayStateString;
	}

	@Override
	public String generateRedirectUrlUsingXMLDocument() throws ParserConfigurationException, TransformerException {
		redirectUrl = "https://login.microsoftonline.com/" + TENANT_ID + "/saml2" + "?SAMLRequest=";
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		Document doc = docBuilder.newDocument();
		Element authnRequest = doc.createElementNS(Constants.SAML_PROTOCOL_NAMESPACE, "samlp:AuthnRequest");
		doc.appendChild(authnRequest);

		authnRequest.setAttribute("xmlns:saml", Constants.SAML_ASSERTION_NAMESPACE);
		authnRequest.setAttribute("ID", "_" + UUID.randomUUID().toString());
		authnRequest.setAttribute("Version", "2.0");

		authnRequest.setAttribute("IssueInstant", DateTime.now().toString());
		authnRequest.setAttribute("ProtocolBinding", Constants.SAML_BINDING_HTTP_POST);
		authnRequest.setAttribute("AssertionConsumerServiceURL", ASSERTION_URL);

		Element issuer = doc.createElement("saml:Issuer");
		issuer.appendChild(doc.createTextNode(ISSUER));
		authnRequest.appendChild(issuer);

		Element nameIDPolicy = doc.createElement("samlp:NameIDPolicy");
		nameIDPolicy.setAttribute("Format", Constants.SAML_NAMEID_FORMAT);
		authnRequest.appendChild(nameIDPolicy);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource(doc), new StreamResult(writer));
		String samlRequest = writer.getBuffer().toString();

		// Compress and encode as before
		byte[] compressedBytes = new byte[samlRequest.length()];
		Deflater deflater = new Deflater(Deflater.DEFAULT_COMPRESSION, true);
		deflater.setInput(samlRequest.getBytes());
		deflater.finish();
		int compressedSize = deflater.deflate(compressedBytes);
		deflater.end();

		String base64Encoded = Base64.getEncoder()
				.encodeToString(Arrays.copyOfRange(compressedBytes, 0, compressedSize));
		String encodedString = UriUtils.encode(base64Encoded, StandardCharsets.UTF_8);
		String relayStateString = this.redirectUrl + encodedString;

		return relayStateString;

	}

	@Override
	public String ssoAuthentication(String samlResponse)
			throws ParserConfigurationException, SAXException, IOException, Exception {
		System.out.println("ENTERED SERVICE FILE " + samlResponse);

		byte[] decodedBytes = Base64.getDecoder().decode(samlResponse);// Decode the Base64 string
		String decodedString = new String(decodedBytes); // Convert the decoded bytes to a string
		System.out.println("RESPONSE DECODED " + decodedString);

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		System.out.println("DocumentBuilderFactory instance created " + decodedString);

		try {
			DocumentBuilder builder = factory.newDocumentBuilder();

			System.out.println("NEW BUILDER created " + decodedString);

			Document document = builder.parse(new InputSource(new StringReader(decodedString)));

			System.out.println("DOCUMENT PARSE SUCCESSFUL " + document.toString());

//			NodeList signatureFromXml = document.getElementsByTagName(Constants.SIGNATURE_VALUE);
			NodeList attributes = document.getElementsByTagName(Constants.ATTRIBUTE);
			NodeList certificateFromXml = document.getElementsByTagName(Constants.X509CERTIFICATE);
			NodeList certificateNodes = document.getElementsByTagName(Constants.DSX509CERTIFICATE);
			String certificate;

			if (certificateFromXml.getLength() > 0) {
				Element certificateElement = (Element) certificateFromXml.item(0);
				certificate = certificateElement.getTextContent();
			} else {
				Element certificateElement = (Element) certificateNodes.item(0);
				certificate = certificateElement.getTextContent();
			}
			NodeList statusFromXml = document.getElementsByTagName(Constants.STATUS_TAG);

			System.out.println("STATUS EXTRACTED " + statusFromXml);

			Element statusCodeElement = (Element) document.getElementsByTagName(Constants.STATUS_CODE_TAG).item(0);

			String status = statusCodeElement.getAttribute(Constants.VALUE);

			System.out.println("statusElement " + status);

			if (status.equals(Constants.SUCCESS_STATUS)) {
				String email = null;
//				String userName = null;
				for (int i = 0; i < attributes.getLength(); i++) {
					Element attribute = (Element) attributes.item(i);
					String name = attribute.getAttribute(Constants.NAME);

					if (name.equals(Constants.EMAIL_TAG)) {
						email = attribute.getElementsByTagName(Constants.ATTRIBUTE_VALUE).item(0).getTextContent();
					}
//
//					if (name.equals(Constants.USER_NAME)) {
//						userName = attribute.getElementsByTagName(Constants.ATTRIBUTE_VALUE).item(0).getTextContent();
//					}
				}

				System.err.println(email);

				if (email == null) {
					return null;
				}

				if (certificate.equals(this.CERTIFICATE)) {
					if (this.userRepository.existsByEmail(email)) {
						return email;
					} else {
						throw new ResourceNotFoundException(ErrorKeyConstants.USER_E031101,
								"You are not registered in fast brokerage application,Please contact admin for registration.");
					}

				} else {
					return null;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Autowired
	private AuthService authService;

	@Override
	public Map<Object, Object> generateAccessToken(String email) throws Exception {

		if (email == null) {
			throw new ResourceNotFoundException(ErrorKeyConstants.USER_E031101, ErrorMessageConstants.USER_NOT_FOUND);
		}

		Map<Object, Object> responseMap = new LinkedHashMap<>();
		String token = "";
		UserEntity user = this.authService.userInformation(email);

		if (user != null) {
			final UserDetails userDetails = authService.loadUserByUsername(user.getEmail());

			token = this.jwtTokenUtil.generateAccessToken(userDetails);

			responseMap.put("TOKEN", token);
			responseMap.put("USER", user);

		} else {
			responseMap.put("ERROR", "true");
			responseMap.put("MSG", "No User found in FAST record");
		}

		return responseMap;

	}

	@Override
	public Map<Object, Object> generateAccessToken(Long userId) throws Exception {

		Map<Object, Object> responseMap = new LinkedHashMap<>();
		String token = "";
		Optional<UserEntity> optional = this.userRepository.findById(userId);
		UserEntity user = optional.get();

		if (user != null) {
			final UserDetails userDetails = authService.loadUserByUsername(user.getEmail());

			token = this.jwtTokenUtil.generateAccessToken(userDetails);

			responseMap.put("TOKEN", token);
			responseMap.put("USER", user);

		} else {
			responseMap.put("ERROR", "true");
			responseMap.put("MSG", "No User found in FAST record");
		}
		return responseMap;

	}

	@Override
	public UserEntity getUserByEmail(String email) throws Exception {
		UserEntity user = this.userRepository.findByEmailIgnoreCase(email);

		if (user == null) {
			throw new ResourceNotFoundException(ErrorKeyConstants.USER_E031101,
					"User not found in fast brokerage records");

		}

		return user;
	}

	@Override
	public String validateUUID(String uuid) throws Exception {

		InviteEntity uuidFromDB = this.inviteRepository.findByCode(uuid);
		if (uuidFromDB != null) {
			return uuidFromDB.getEmail();
		} else {
			return null;
		}
	}
}
