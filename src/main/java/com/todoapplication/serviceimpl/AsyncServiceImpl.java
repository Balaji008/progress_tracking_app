package com.todoapplication.serviceimpl;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.opencsv.CSVReader;
import com.todoapplication.entities.AsyncDataEntity;
import com.todoapplication.exceptions.ResourceNotFoundException;
import com.todoapplication.ilistdto.IAsyncDataListDto;
import com.todoapplication.repositories.AsyncDataRepository;

@Service
public class AsyncServiceImpl {

	private static final Logger LOG = LoggerFactory.getLogger(AsyncServiceImpl.class);
	@Autowired
	private AsyncDataRepository asyncDataRepository;

//	@Async("processExecutor")
//	public CompletableFuture<List<AsyncDataEntity>> saveAllData(final InputStream inputStream) throws Exception {
//		try {
//			final long start = System.currentTimeMillis();
//
////		if (parseCSVFile(inputStream) != null) {
//			List<AsyncDataEntity> data = parseCSVFile(inputStream);
//
//			LOG.info("Saving a list of employees of size {} records", data.size());
//
//			data = asyncDataRepository.saveAll(data);
//
//			LOG.info("Elapsed time: {}", (System.currentTimeMillis() - start));
//			return CompletableFuture.completedFuture(data);
//
////		return null;
//		} catch (Exception e) {
//			LOG.error("Failed to parse CSV file {}", e.getMessage());
//			throw new Exception(e.getMessage());
//
//		}
//	}
//
//	@Async // ("processExecutor")
//	public CompletableFuture<List<AsyncDataEntity>> saveAllData(final InputStream inputStream) {
//		try {
//			final long start = System.currentTimeMillis();
//
//			List<AsyncDataEntity> data = parseCSVFile(inputStream);
//
//			LOG.info("Saving a list of employees of size {} records", data.size());
//
//			data = asyncDataRepository.saveAll(data);
//
//			LOG.info("Elapsed time: {}", (System.currentTimeMillis() - start));
//			return CompletableFuture.completedFuture(data);
//
//		} catch (ResourceNotFoundException e) {
//			LOG.error("Failed to parse CSV file {}", e.getMessage());
//			return CompletableFuture.failedFuture(e);
//		} catch (Exception e) {
//			LOG.error("Failed to parse CSV file {}", e.getMessage());
//			return CompletableFuture.failedFuture(new Exception("Error parsing CSV file"));
//		}
//	}

	@Async("processExecutor")
	public CompletableFuture<List<AsyncDataEntity>> saveAllData(final InputStream inputStream) {
		try {
			final long start = System.currentTimeMillis();

			List<AsyncDataEntity> data = parseCSVFile(inputStream);

			LOG.info("Saving a list of employees of size {} records", data.size());

			data = asyncDataRepository.saveAll(data);

			LOG.info("Elapsed time: {}", (System.currentTimeMillis() - start));
			return CompletableFuture.completedFuture(data);

		} catch (ResourceNotFoundException e) {
			LOG.error("Failed to parse CSV file {}", e.getMessage());
			return CompletableFuture.failedFuture(e);
		} catch (Exception e) {
			LOG.error("Failed to parse CSV file {}", e.getMessage());
			return CompletableFuture.failedFuture(new Exception("Error parsing CSV file"));
		}
	}

//	@Async("processExecutor")
	public List<AsyncDataEntity> saveData(final InputStream inputStream) throws Exception {
		try {
			final long start = System.currentTimeMillis();
			parseCSVFile(inputStream);
			List<AsyncDataEntity> data = parseCSVFile(inputStream);

			LOG.info("Saving a list of employees of size {} records", data.size());

			asyncDataRepository.saveAll(data);

			LOG.info("Elapsed time: {}", (System.currentTimeMillis() - start));
			return data;

		} catch (Exception e) {
			LOG.error("Failed to parse CSV file {}", e.getMessage());
			throw new Exception(e.getMessage());

		}
	}

//	@Async("processExecutor")
	@SuppressWarnings({ "resource", "unused" })
	public List<AsyncDataEntity> parseCSVFile(final InputStream inputStream) throws Exception {
		final List<AsyncDataEntity> list = new ArrayList<>();

		CSVReader reader = new CSVReader(new InputStreamReader(inputStream));
		String[] header = reader.readNext(); // skip the first line (header)

		String[] row;
		int rowNumber = 2;
		while ((row = reader.readNext()) != null) {
			AsyncDataEntity data = new AsyncDataEntity();
//			AsyncDataDto dataDto = new AsyncDataDto();
			data.setAge(Long.parseLong(row[0]));
			data.setAttrition(row[1]);
			data.setBusinessTravel(row[2]);
			data.setDailyRate(Long.parseLong(row[3]));
			data.setDepartment(row[4]);
			data.setDistanceFromHome(Long.parseLong(row[5]));
			data.setEducation(Long.parseLong(row[6]));
			data.setEducationField(row[7]);
			data.setEmployeeCount(Long.parseLong(row[8]));
			if (row[9] != null && !row[9].isEmpty()) {
//				System.err.println("If condition");
				data.setEmployeeNumber(Long.parseLong(row[9]));
			} else {
//				System.err.println("else condition");
				throw new ResourceNotFoundException("Column 10 is empty at row " + rowNumber,
						"Column 10 is empty at row " + rowNumber);
			}
			data.setEnvironmentSatisfaction(Long.parseLong(row[10]));
			data.setGender(row[11]);
			data.setHourlyRate(Long.parseLong(row[12]));
			data.setJobInvolver(Long.parseLong(row[13]));
			data.setJobLevel(Long.parseLong(row[14]));
			data.setJobRole(row[15]);
			data.setJobSatisfaction(Long.parseLong(row[16]));
			data.setMaritalStatus(row[17]);
			data.setMonthlyIncome(Long.parseLong(row[18]));
			data.setMonyhlyRate(Long.parseLong(row[19]));
			data.setNumCompaniesWorked(Long.parseLong(row[20]));
			data.setOver18(row[21]);
			data.setOverTime(row[22]);
			data.setPercentSalaryHike(Long.parseLong(row[23]));
			data.setPerformanceRating(Long.parseLong(row[24]));
			data.setRelationshipSatisfaction(Long.parseLong(row[25]));
			data.setStandardHours(Long.parseLong(row[26]));
			data.setStockOptionLevel(Long.parseLong(row[27]));
			data.setTotalWorkingYears(Long.parseLong(row[28]));
			data.setTrainingTimesLastYears(Long.parseLong(row[29]));
			data.setWorkLifeBalance(Long.parseLong(row[30]));
			data.setYearsAtCompany(Long.parseLong(row[31]));
			data.setYearsInCurrentRole(Long.parseLong(row[32]));
			data.setYearsSinceLastPromotion(Long.parseLong(row[33]));
			data.setYearsWithCurrentManager(Long.parseLong(row[34]));

			list.add(data);
			rowNumber++;
		}

		reader.close();

		return list;

//		} catch (final IOException e) {
//			LOG.error("Failed to parse CSV file {}", e);
////			throw new Exception("Failed to parse CSV file {}", e);
//			return null;
//		} catch (NumberFormatException e) {
//			LOG.error("Invalid Numbers", e);
//			return null;
//		} catch (Exception e) {
//			LOG.error("Something went wrong", e);
////			throw new ResourceNotFoundException("Invalid File", "Invalid File");
//			return null;
//		}

	}

	@Async("processExecutor")
	public CompletableFuture<Page<IAsyncDataListDto>> getAllData(Integer pageNumber, Integer pageSize) {

		LOG.info("Request to get a list of data");
		Pageable p = PageRequest.of(pageNumber, pageSize);

		Page<IAsyncDataListDto> dataList = asyncDataRepository.findByOrderById(p, IAsyncDataListDto.class);

		return CompletableFuture.completedFuture(dataList);
	}

	public synchronized Page<IAsyncDataListDto> getData(Integer pageNumber, Integer pageSize) {

		LOG.info("Request to get a list of data");
		Pageable p = PageRequest.of(pageNumber, pageSize);

		Page<IAsyncDataListDto> dataList = asyncDataRepository.findByOrderById(p, IAsyncDataListDto.class);

		return dataList;
	}

	public String runInBackground() {
		// TODO Auto-generated method stub
		return null;
	}

}
