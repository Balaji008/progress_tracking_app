package com.todoapplication.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.todoapplication.dto.PermissionDto;
import com.todoapplication.entities.PermissionEntity;
import com.todoapplication.exceptions.ResourceNotFoundException;
import com.todoapplication.ilistdto.IPermissionListDto;
import com.todoapplication.repositories.PermissionRepository;
import com.todoapplication.service.PermissionService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;

@Service
public class PermissionServiceImpl implements PermissionService {

	@Autowired
	private PermissionRepository permissionRepository;

	@Override
	public PermissionDto addPermission(PermissionDto permissionDto) {
//		this.permissionRepository.changeColumnType();
		if (!permissionRepository.existsByactionNameEqualsIgnoreCase(permissionDto.getActionName())) {
			PermissionEntity permission = new PermissionEntity();
			permission.setActionName(permissionDto.getActionName());
			this.permissionRepository.save(permission);
			permissionDto.setId(permission.getId());
			return permissionDto;
		}
		return null;
	}

	@Override
	public PermissionDto updatePermission(PermissionDto permissionDto, Long id) {
		PermissionEntity permission = this.permissionRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.PERMISSION_E031301,
						ErrorMessageConstants.PERMISSION_NOT_FOUND));
		permission.setActionName(permissionDto.getActionName());
		this.permissionRepository.save(permission);
		return permissionDto;
	}

	@Override
	public void deletePermission(Long id) {
		PermissionEntity permission = this.permissionRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.PERMISSION_E031301,
						ErrorMessageConstants.PERMISSION_NOT_FOUND));
		this.permissionRepository.delete(permission);
	}

	@Override
	public List<IPermissionListDto> getAllPermissions() {
		List<IPermissionListDto> permissions = this.permissionRepository.findByOrderByIdDesc(IPermissionListDto.class);
		return permissions;
	}

}
