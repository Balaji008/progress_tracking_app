package com.todoapplication.serviceimpl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.todoapplication.dto.AllFileDto;
import com.todoapplication.entities.AllFilesEntity;
import com.todoapplication.repositories.AllFilesRepository;
import com.todoapplication.service.AllFileService;

@Service
public class AllFileServiceImpl implements AllFileService {

	@Value("${images}")
	private String imagesPath;

	@Value("${videos}")
	private String videosPath;

	@Value("${excel}")
	private String excelPath;

	@Value("${pdf}")
	private String pdfPath;

	@Value("${word}")
	private String wordPath;

	@Value("${documents}")
	private String documentsPath;

	@Value("${project.files}")
	private String path;
	@Autowired
	private AllFilesRepository allFilesRepository;

	@Override
	@Async
	public CompletableFuture<Void> uploadFiles(List<AllFileDto> fileDTOs) {
		fileDTOs.forEach(fileDTO -> {
			String folderPath = path;
			String filePath = folderPath + "/" + fileDTO.getFileName();
			File file = new File(filePath);
			try {
				FileUtils.writeByteArrayToFile(file, fileDTO.getFileBytes());
				AllFilesEntity filesEntity = new AllFilesEntity();
				filesEntity.setFileName(fileDTO.getFileName());
				allFilesRepository.save(filesEntity);
			} catch (IOException e) {
				throw new RuntimeException("Error uploading file");
			}
		});
		return CompletableFuture.completedFuture(null);
	}

	@Override
	public List<Resource> getFiles() {
		List<AllFilesEntity> allFiles = allFilesRepository.findAll();
		List<Resource> resources = new ArrayList<>();

		for (AllFilesEntity file : allFiles) {
			String fileName = file.getFileName();
			@SuppressWarnings("unused")
			String fileType = getFileType(fileName);
			byte[] fileData = null;
			try {
				fileData = getFileData(fileName);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			ByteArrayResource resource = new ByteArrayResource(fileData);
			resources.add(resource);
		}

		return resources;
	}

	private String getFileType(String fileName) {
		String fileType = null;

		String extension = StringUtils.getFilenameExtension(fileName);
		switch (extension) {
		case "jpg":
		case "jpeg":
			fileType = "image/jpeg";
			break;
		case "png":
			fileType = "image/png";
			break;
		case "gif":
			fileType = "image/gif";
			break;
		case "pdf":
			fileType = "application/pdf";
			break;
		case "doc":
		case "docx":
			fileType = "application/msword";
			break;
		case "xls":
		case "xlsx":
			fileType = "application/vnd.ms-excel";
			break;
		case "ppt":
		case "pptx":
			fileType = "application/vnd.ms-powerpoint";
			break;
		case "txt":
			fileType = "text/plain";
			break;
		default:
			fileType = "application/octet-stream";
			break;
		}

		return fileType;
	}

	private byte[] getFileData(String fileName) throws IOException {
		Path path = Paths.get(fileName);
		return Files.readAllBytes(path);
	}

	@SuppressWarnings("unused")
	private String getFolderPath(String fileType) {
		String folderPath;
		switch (fileType) {
		case "image/jpeg":
		case "image/png":
			folderPath = imagesPath;
			break;
		case "video/mp4":
		case "video/avi":
		case "video/x-msvideo":
		case "video/quicktime":
		case "video/x-ms-wmv":
		case "video/x-flv":
		case "video/x-matroska":
		case "video/webm":
		case "video/mpeg":
		case "video/3gpp":
		case "video/x-ms-asf":
			folderPath = videosPath;
			break;
		case "application/vnd.ms-excel":
			folderPath = excelPath;
			break;
		case "application/pdf":
			folderPath = pdfPath;
			break;
		case "application/msword":
		case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
			folderPath = wordPath;
			break;
		default:
			folderPath = documentsPath;
			break;
		}
		return folderPath;
	}

}
