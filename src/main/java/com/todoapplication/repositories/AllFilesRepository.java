package com.todoapplication.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.todoapplication.entities.AllFilesEntity;

public interface AllFilesRepository extends JpaRepository<AllFilesEntity, Long> {

//	@Query(value = "select file_name from all_files", nativeQuery = true)
//	List<String> getAllFileNames();
}
