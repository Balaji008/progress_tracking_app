package com.todoapplication.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import com.todoapplication.entities.InviteEntity;

public interface InviteRepository extends JpaRepository<InviteEntity, Long> {

	InviteEntity findByEmail(String email);

	@Transactional
	@Modifying
	void deleteByEmail(String mail);

	InviteEntity findByCode(String code);
}
