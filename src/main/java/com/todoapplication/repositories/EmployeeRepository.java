package com.todoapplication.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.todoapplication.entities.EmployeeEntity;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {

}
