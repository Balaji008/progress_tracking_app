package com.todoapplication.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.todoapplication.entities.AsyncDataEntity;
import com.todoapplication.ilistdto.IAsyncDataListDto;

public interface AsyncDataRepository extends JpaRepository<AsyncDataEntity, Long> {

	Page<IAsyncDataListDto> findByOrderById(Pageable pageable, Class<IAsyncDataListDto> class1);
}
