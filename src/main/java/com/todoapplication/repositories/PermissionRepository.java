package com.todoapplication.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.todoapplication.entities.PermissionEntity;
import com.todoapplication.ilistdto.IPermissionListDto;

public interface PermissionRepository extends JpaRepository<PermissionEntity, Long> {

	@Query(value = "select String_agg (p.action_name,',') as PermissionName from permissions p where p.is_active=true", nativeQuery = true)
	List<IPermissionListDto> findByOrderByIdDesc(Class<IPermissionListDto> class1);

	boolean existsByactionNameEqualsIgnoreCase(String permissionName);

//	@Modifying
//	@Transactional
//	@Query(value = "ALTER TABLE permissions ALTER COLUMN action_name TYPE Varchar(500)", nativeQuery = true)
//	void changeColumnType();
}
