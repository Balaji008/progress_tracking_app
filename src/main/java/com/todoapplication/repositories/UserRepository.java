package com.todoapplication.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.todoapplication.entities.UserEntity;
import com.todoapplication.ilistdto.IListUserDto;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

	List<IListUserDto> findByOrderByIdDesc(Class<IListUserDto> class1);

	List<IListUserDto> findById(Long id, Class<IListUserDto> class1);

	UserEntity findByEmailIgnoreCase(String username);

	Boolean existsByEmail(String email);
}
