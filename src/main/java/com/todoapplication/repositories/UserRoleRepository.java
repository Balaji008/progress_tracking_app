package com.todoapplication.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.RequestParam;

import com.todoapplication.entities.UserRoleEntity;
import com.todoapplication.ilistdto.IUserRoleListDto;

public interface UserRoleRepository extends JpaRepository<UserRoleEntity, Long> {

	@Query(value = "select u.name as UserName, string_agg(r.name,',')as RoleName from user_roles ur join users u on ur.user_id=u.id join roles r on \r\n"
			+ "ur.role_id=r.id where ur.is_active=true group by u.id", nativeQuery = true)
	List<IUserRoleListDto> getRolesOfAllUsers(Class<IUserRoleListDto> class1);

	@Query(value = "select u.name as UserName,   string_agg(r.name,',')as RoleName from user_roles ur join users u on ur.user_id=u.id join roles r on \r\n"
			+ "ur.role_id=r.id where ur.id=:id and ur.is_active=true group by u.id", nativeQuery = true)
	List<IUserRoleListDto> getUserRoleById(@RequestParam Long id, Class<IUserRoleListDto> class1);

	boolean existsByUserIdAndRoleId(Long userId, Long roleId);

	List<UserRoleEntity> findUserIdsByRoleId(Long roleId);
}
