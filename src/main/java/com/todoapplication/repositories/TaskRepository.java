package com.todoapplication.repositories;

import java.util.*;
import org.springframework.data.jpa.repository.JpaRepository;

import com.todoapplication.entities.TaskEntity;
import com.todoapplication.ilistdto.ITaskListDto;

public interface TaskRepository extends JpaRepository<TaskEntity, Long> {

	List<ITaskListDto> findByOrderByIdDesc(Class<ITaskListDto> class1);

	boolean existsByTaskNameEqualsIgnoreCase(String taskName);
}
