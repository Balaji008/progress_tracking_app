package com.todoapplication.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.todoapplication.entities.RoleEntity;
import com.todoapplication.ilistdto.IRoleListDto;

public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

	List<IRoleListDto> findByOrderByIdDesc(Class<IRoleListDto> class1);

	IRoleListDto findById(Long roleId, Class<IRoleListDto> class1);

	boolean existsByName(String roleName);
}
