package com.todoapplication.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.todoapplication.entities.OtpEntity;

public interface OtpRepository extends JpaRepository<OtpEntity, Long> {

	OtpEntity findByEmailIgnoreCase(String email);
}
