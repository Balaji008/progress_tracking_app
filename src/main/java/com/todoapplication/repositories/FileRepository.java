package com.todoapplication.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.todoapplication.entities.FileEntity;

public interface FileRepository extends JpaRepository<FileEntity, Long> {

}
