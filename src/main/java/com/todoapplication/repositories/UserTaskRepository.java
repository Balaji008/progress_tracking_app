package com.todoapplication.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.todoapplication.entities.UserTaskEntity;
import com.todoapplication.ilistdto.IUserTaskListDto;

public interface UserTaskRepository extends JpaRepository<UserTaskEntity, Long> {

	UserTaskEntity findByUserIdAndTaskId(Long userId, Long taskId);

	@Query(value = "select u.name as UserName,t.id as TaskId,t.task_name  as TaskName,ut.task_status as TaskStatus from user_tasks ut join users u on ut.user_id=u.id\r\n"
			+ "join tasks t on t.id=ut.task_id where ut.is_active=true order by u.name desc", nativeQuery = true)
	List<IUserTaskListDto> getTasksOfAllUsers(Class<IUserTaskListDto> class1);

	@Query(value = "select u.name as UserName,t.id as TaskId,t.task_name  as TaskName,ut.task_status as TaskStatus from user_tasks ut join users u on ut.user_id=u.id\r\n"
			+ "join tasks t on t.id=ut.task_id where u.id=:userId and ut.is_active=true order by u.name desc", nativeQuery = true)
	List<IUserTaskListDto> getTasksOfUserById(@Param("userId") Long userId, Class<IUserTaskListDto> class1);

	@Query(value = "\r\n"
			+ "select u.name as UserName,t.id as TaskId,t.task_name  as TaskName,ut.task_status as TaskStatus from user_tasks ut join users u \r\n"
			+ "on ut.user_id=u.id join tasks t on t.id=ut.task_id where t.id=:taskId and u.id=:userId and ut.is_active=true order by u.name desc", nativeQuery = true)
	List<IUserTaskListDto> getTasksOfUserByTaskId(@Param("taskId") Long taskId, @Param("userId") Long userId,
			Class<IUserTaskListDto> class1);

	boolean existsByUserIdAndTaskId(Long userId, Long taskId);
}
