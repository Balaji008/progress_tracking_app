package com.todoapplication.configurations;

import javax.servlet.MultipartConfigElement;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;

@Configuration
public class FileUploadConfiguration {

	@SuppressWarnings("unused")
	private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(FileUploadConfiguration.class);

	@Value("${spring.servlet.multipart.max-file-size}")
	private String maxFileSize;

	@Value("${spring.servlet.multipart.max-request-size}")
	private String maxRequestSize;

//	@Autowired
//	private ConversionService conversionService;

	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
//		LOG.info(maxFileSize);
//		LOG.info(maxRequestSize);
		factory.setMaxFileSize(DataSize.parse(convertToCharSequence(maxFileSize)));
		factory.setMaxRequestSize(DataSize.parse(convertToCharSequence(maxRequestSize)));
		return factory.createMultipartConfig();
	}

	private CharSequence convertToCharSequence(String size) {
		return size;
	}
}
