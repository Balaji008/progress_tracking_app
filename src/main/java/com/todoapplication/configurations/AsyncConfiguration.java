package com.todoapplication.configurations;

import java.util.concurrent.Executor;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class AsyncConfiguration implements AsyncConfigurer {

	private static final Logger LOGGER = Logger.getLogger(AsyncConfiguration.class);

	@Override
	@Bean(name = "processExecutor")
	public Executor getAsyncExecutor() {
		LOGGER.debug("Creating Async Task Executor");
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(100);
		executor.setQueueCapacity(100);
		executor.setThreadNamePrefix("Process-");
		executor.initialize();
		return executor;
//		executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
//		executor.setThreadFactory(new CustomThreadFactory("Async Thread Error"));
//		executor.setTaskDecorator(new CustomTaskDecorator());
//		executor.setWaitForTasksToCompleteOnShutdown(true);
//		executor.setAwaitTerminationSeconds(30);

//		executor.setKeepAliveSeconds(60);
//		executor.setAllowCoreThreadTimeOut(true);
//		executor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
//		executor.setWaitForTasksToCompleteOnShutdown(true);
//		executor.setThreadPriority(Thread.NORM_PRIORITY);
//		executor.setDaemon(true);
//		executor.setThreadGroupName("MyAsyncExecutorGroup");
//	 executor.setUncaughtExceptionHandler(new CustomUncaughtExceptionHandler());

//		LOGGER.debug(executor.createThread(thread()).getThreadGroup().getName());

	}

	@Bean
	public Thread thread() {
		return new Thread();
	}
//    @Bean (name = "taskExecutor")
//    public Executor taskExecutor() {
//        LOGGER.debug("Creating Async Task Executor");
//        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
//        executor.setCorePoolSize(2);
//        executor.setMaxPoolSize(2);
//        executor.setQueueCapacity(100);
//        executor.setThreadNamePrefix("CarThread-");
//        executor.initialize();
//        return executor;
//    }

}
