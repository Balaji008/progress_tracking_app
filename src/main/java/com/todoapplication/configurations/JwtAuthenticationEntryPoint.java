package com.todoapplication.configurations;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {

		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
//		Collection<String> headerNames = response.getHeaderNames();
//		headerNames.forEach(e -> {
//			System.err.println(e + "\t" + response.getHeader(e));
//
//		});
//		String header = response.getHeader("Error");
//		System.out.println(header);
//		System.out.println(response.containsHeader("Error"));
//		String message = authException.getMessage();
//		System.err.println(message);
//		System.err.println(response.getHeader("Error") != null);
//		System.err.println(response.getHeader("Error").equals("JWT Expired"));
		if (response.getHeader("Error") != null && response.getHeader("Error").equals("JWT Expired")) {
			response.getOutputStream().println(
					"{\r\n" + "	\"message\": \"Jwt Expired\",\r\n" + "	\"msgKey\": \"Jwt Expired\"\r\n" + "}");
		} else {

			response.getOutputStream().println("{\r\n" + "	\"message\": \"Unauthorized user\",\r\n"
					+ "	\"msgKey\": \"Unauthorized\"\r\n" + "}");
		}
	}

}
