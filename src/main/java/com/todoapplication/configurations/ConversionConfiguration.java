package com.todoapplication.configurations;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;

@Configurable
public class ConversionConfiguration {

	@Bean
	public ConversionService conversionService() {
		return new DefaultConversionService();
	}
}
