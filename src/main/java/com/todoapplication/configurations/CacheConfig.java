package com.todoapplication.configurations;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
@EnableCaching
public class CacheConfig {

	@Value("${spring.redis.host}")
	private String redisHost;

	@Value("${spring.redis.port}")
	private int redisPort;

	@Bean
	public LettuceConnectionFactory redisConnectionFactory() {
//		RedisStandaloneConfiguration redisConf = new RedisStandaloneConfiguration();
//		redisConf.setHostName(redisHost);
//		redisConf.setPort(redisPort);
//		return new LettuceConnectionFactory(redisConf);

		return new LettuceConnectionFactory(redisHost, redisPort);
	}

	@Bean
	public RedisTemplate<String, Object> redisTemplate() {

		RedisTemplate<String, Object> template = new RedisTemplate<>();
		RedisSerializer<String> stringSerializer = new StringRedisSerializer();
		JdkSerializationRedisSerializer jdkSerializationRedisSerializer = new JdkSerializationRedisSerializer();
		LettuceConnectionFactory lcf = this.redisConnectionFactory();
		lcf.afterPropertiesSet();
		template.setConnectionFactory(lcf);
		template.setKeySerializer(stringSerializer);
		template.setHashKeySerializer(stringSerializer);
		template.setValueSerializer(jdkSerializationRedisSerializer);
		template.setHashValueSerializer(jdkSerializationRedisSerializer);
		template.setEnableTransactionSupport(true);
		template.afterPropertiesSet();

		return template;
	}

	@Bean
	public RedisCacheConfiguration cacheConfiguration() {
		RedisCacheConfiguration cacheConfig = RedisCacheConfiguration.defaultCacheConfig()
				.entryTtl(Duration.ofSeconds(15)).disableCachingNullValues();
		return cacheConfig;
	}

	@Bean
	public RedisCacheManager cacheManager() {
		RedisCacheManager rcm = RedisCacheManager.builder(redisConnectionFactory()).cacheDefaults(cacheConfiguration())
				.transactionAware().build();
		return rcm;
	}
//
//	@Bean(destroyMethod = "shutdown")
//	public DefaultClientResources defaultClientResources() {
//		return DefaultClientResources.create();
//	}
//
//	@Bean(destroyMethod = "close")
//	public StatefulRedisConnection<String, String> redisConnection() {
//		RedisClient client = RedisClient.create(defaultClientResources(), "redis://localhost");
//		return client.connect();
//	}
//
//	@Bean
//	public RedisCommands<String, String> redisCommands() {
//		return redisConnection().sync();
//	}

//	@Bean
//	public RedisTemplate<String, UserEntity> redisTemplateUser() {
//		RedisTemplate<String, UserEntity> redisTemplate = new RedisTemplate<>();
//		redisTemplate.setConnectionFactory(redisConnectionFactory());
//		redisTemplate.setKeySerializer(new StringRedisSerializer());
//		redisTemplate.setValueSerializer(new UserEntitySerializer());
//		redisTemplate.setHashKeySerializer(new StringRedisSerializer());
//		redisTemplate.setHashValueSerializer(new UserEntitySerializer());
//		redisTemplate.afterPropertiesSet();
//
//		return redisTemplate;
//	}

//	@Bean
//	public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
//		RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
//		redisTemplate.setConnectionFactory(redisConnectionFactory);
//
//		// Use Jackson2JsonRedisSerializer to serialize objects
//		Jackson2JsonRedisSerializer<Object> jsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
//		redisTemplate.setDefaultSerializer(jsonRedisSerializer);
//		redisTemplate.setKeySerializer(new StringRedisSerializer());
//
//		// Set value serializer to skip serialization of
//		// DefaultMethodInvokingMethodInterceptor
//		redisTemplate.setValueSerializer(new RedisSerializer<Object>() {
//			private final ObjectMapper objectMapper = new ObjectMapper()
//					.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
//
//			@Override
//			public byte[] serialize(Object value) throws SerializationException {
//				if (value instanceof DefaultMethodInvokingMethodInterceptor) {
//					return new byte[0];
//				} else {
//					try {
//						return objectMapper.writeValueAsBytes(value);
//					} catch (JsonProcessingException e) {
//						throw new SerializationException("Could not serialize object: " + e.getMessage(), e);
//					}
//				}
//			}
//
//			@Override
//			public Object deserialize(byte[] bytes) throws SerializationException {
//				if (bytes == null || bytes.length == 0) {
//					return null;
//				}
//				try {
//					return objectMapper.readValue(bytes, Object.class);
//				} catch (IOException e) {
//					throw new SerializationException("Could not deserialize object: " + e.getMessage(), e);
//				}
//			}
//		});
//
//		return redisTemplate;
//	}

}
