package com.todoapplication.configurations;

//@Configuration
//@EnableBatchProcessing
public class BatchConfiguration {

//	// step builder object
//	@Autowired
//	private StepBuilderFactory stepBuilderFactory;
//
//	// job builder object
//	@Autowired
//	private JobBuilderFactory jobBuilderFactory;
//
//	@Autowired
//	private EntityManagerFactory entityManagerFactory;
//
//	// reader class object
//	@Bean
//	public FlatFileItemReader<EmployeeDto> reader() {
//
//		FlatFileItemReader<EmployeeDto> reader = new FlatFileItemReader<>();
//
//		reader.setResource(new ClassPathResource("employees.csv"));
//		reader.setResource(new FileSystemResource(""));
//		reader.setLinesToSkip(1);
//		reader.setLineMapper(new DefaultLineMapper<>() {
//			{
//				setLineTokenizer(new DelimitedLineTokenizer() {
//					{
//						setDelimiter(DELIMITER_COMMA);
//						setNames("firstName", "lastName", "hireDate", "jobId", "salary");
//						setIncludedFields(1, 2, 5, 6, 7);
//					}
//				});
//
//				setFieldSetMapper(new BeanWrapperFieldSetMapper<>() {
//					{
//						setTargetType(EmployeeDto.class);
//
//						setCustomEditors(Map.of(LocalDate.class, new PropertyEditorSupport() {
//							@Override
//							public void setAsText(String text) throws IllegalArgumentException {
//								DateTimeFormatter formatter = new DateTimeFormatterBuilder().parseCaseInsensitive()
//										.appendPattern("dd-MMM-yy").toFormatter(Locale.ENGLISH);
//								LocalDate date = LocalDate.parse(text, formatter);
//								setValue(date);
//							}
//						}, Double.class, new PropertyEditorSupport() {
//							@Override
//							public void setAsText(String text) throws IllegalArgumentException {
//								setValue(Double.parseDouble(text));
//							}
//						}));
//					}
//
//					@Override
//					public EmployeeDto mapFieldSet(FieldSet fieldSet) throws BindException {
//						DataBinder dataBinder = new DataBinder(new EmployeeDto());
//						dataBinder.setConversionService(new DefaultFormattingConversionService());
//						dataBinder.setValidator(getValidator());
//						dataBinder.setMessageCodesResolver(getMessageCodesResolver());
//						dataBinder.bind(new MutablePropertyValues() {
//							{
//								add("firstName", fieldSet.readString("firstName"));
//								add("lastName", fieldSet.readString("lastName"));
//								add("hireDate", fieldSet.readString("hireDate"));
//								add("jobId", fieldSet.readString("jobId"));
//								add("salary", fieldSet.readString("salary"));
//							}
//						});
//						return (EmployeeDto) dataBinder.getTarget();
//					}
//
//					private Validator getValidator() {
//						return null; // add a custom validator if needed
//					}
//
//					private MessageCodesResolver getMessageCodesResolver() {
//						return new DefaultMessageCodesResolver();
//					}
//				});
//			}
//		});
//
//		return reader;
//	}
//
//	// Item processor is functional interface so we returned lambda expression
//	@Bean
//	public ItemProcessor<EmployeeDto, EmployeeEntity> processor() {
//
//		// item is a object of EmployeeDto
//
//		return item -> {
//			System.err.println(item.getFirstName() + "\t" + item.getLastName() + "\t" + item.getHireDate() + "\t"
//					+ item.getSalary());
//			EmployeeEntity employee = new EmployeeEntity();
//			employee.setFirstName(item.getFirstName());
//			employee.setLastName(item.getLastName());
////			employee.setHireDate(item.getHireDate());
//			employee.setHireDate(item.getHireDate());
//			employee.setJobId(item.getJobId());
//			employee.setSalary(item.getSalary());
////			employee.setSalary(item.getSalary());
//			return employee;
//		};
//	}
//
//	// writer class object
//	@Bean
//	public JpaItemWriter<EmployeeEntity> writer() {
//		JpaItemWriter<EmployeeEntity> writer = new JpaItemWriter<>();
//		writer.setEntityManagerFactory(entityManagerFactory);
//		return writer;
//	}
//
////	@Bean
////	public PlatformTransactionManager transactionManager() {
////		JpaTransactionManager transactionManager = new JpaTransactionManager();
////		transactionManager.setEntityManagerFactory(entityManagerFactory);
////		return transactionManager;
////	}
//
//	// listener class object
//	@Bean
//	public JobExecutionListener listener() {
//
//		return new EmployeeJobListener();
//	}
//
//	// step object
//	@Bean
//	public Step step() {
//		return this.stepBuilderFactory.get("step").<EmployeeDto, EmployeeEntity>chunk(5).reader(this.reader())
//				.processor(this.processor()).writer(this.writer()).build();
//	}
//
//	// job object
//	@Bean
//	public Job job() {
//		return this.jobBuilderFactory.get("job").incrementer(new RunIdIncrementer()).listener(this.listener())
//				.start(this.step())
////				.next(this.stepB())
////				.next(this.stepC())
//				.build();
//	}
}
