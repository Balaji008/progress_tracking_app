package com.todoapplication.configurations;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.todoapplication.entities.UserEntity;
import com.todoapplication.exceptions.ResourceNotFoundException;
import com.todoapplication.service.AuthService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

	private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

	private static final String AUTHORIZATION_HEADER = "Authorization";
	private static final String ACCESS_TOKEN_TYPE = "access";
	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private AuthService authService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String requestToken = request.getHeader(AUTHORIZATION_HEADER);
		try {
			if (requestToken != null && requestToken.startsWith("Bearer ")) {
				String accessToken = requestToken.substring(7);
				String username = jwtTokenUtil.getUsernameFromToken(accessToken);

				if (jwtTokenUtil.getTokenType(accessToken).equals(ACCESS_TOKEN_TYPE)) {
					Optional<UserEntity> user = Optional.ofNullable(authService.userInformation(username));
					user.orElseThrow(() -> new ResourceNotFoundException(ErrorKeyConstants.USER_E031101,
							ErrorMessageConstants.USER_NOT_FOUND));

					if (SecurityContextHolder.getContext().getAuthentication() == null) {
						UserDetails userDetails = authService.loadUserByUsername(username);
						boolean isTokenValid = jwtTokenUtil.validateToken(accessToken, userDetails);

						if (isTokenValid) {
							UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
									userDetails, null, userDetails.getAuthorities());

							WebAuthenticationDetailsSource webAuthenticationDetailsSource = new WebAuthenticationDetailsSource();
							usernamePasswordAuthenticationToken
									.setDetails(webAuthenticationDetailsSource.buildDetails(request));
							SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
						}
					}
				}
			}
		} catch (ExpiredJwtException e) {
			response.setHeader("Error", "JWT Expired");
		} catch (MalformedJwtException e) {
			logger.error("Invalid JWT");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		filterChain.doFilter(request, response);
	}

//	@Override
//	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
//			throws ServletException, IOException {
//		String requestToken = request.getHeader("Authorization");
//		String username = null;
//		String accessToken = null;
//		String type = "access";
//
//		try {
//			if (requestToken != null && requestToken.startsWith("Bearer ")
//					&& jwtTokenUtil.getTokenType(requestToken.substring(7)).equals(type)) {
//				accessToken = requestToken.substring(7);
//				username = this.jwtTokenUtil.getUsernameFromToken(accessToken);
//
//				UserEntity user = this.authService.userInformation(username);
////				System.err.println("user in filter class");
//				if (user == null) {
//					throw new ResourceNotFoundException(ErrorKeyConstants.USER_E031101,
//							ErrorMessageConstants.USER_NOT_FOUND);
//				}
//			}
//		} catch (ExpiredJwtException e) {
//			response.setHeader("Error", "JWT Expired");
//		} catch (MalformedJwtException e) {
//			System.out.println("Invalid jwt");
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//		}
//
//		if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
//			UserDetails userDetails = this.authService.loadUserByUsername(username);
//
//			if (this.jwtTokenUtil.validateToken(accessToken, userDetails)) {
//				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
//						userDetails, null, userDetails.getAuthorities());
//
//				WebAuthenticationDetailsSource webAuthenticationDetailsSource = new WebAuthenticationDetailsSource();
//				usernamePasswordAuthenticationToken.setDetails(webAuthenticationDetailsSource.buildDetails(request));
//				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
//			}
//		}
//
//		filterChain.doFilter(request, response);
//
//	}

}
