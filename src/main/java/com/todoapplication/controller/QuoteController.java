package com.todoapplication.controller;

import java.io.IOException;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todoapplication.service.EmailService;
import com.todoapplication.serviceimpl.QuoteServiceImpl;
import com.todoapplication.util.QuoteOfTheDay;

@RestController
@RequestMapping("/api/quotes")
public class QuoteController {

	@Autowired
	private QuoteServiceImpl quoteService;

	@Autowired
	private EmailService emailService;

//	@Autowired
//	private JavaMailSender javaMailSender;

	private static final Logger LOG = LoggerFactory.getLogger(QuoteController.class);

	@Scheduled(cron = "0 00 10  * * MON-SAT") // run every day except Sundays at 10:00 AM
	public void sendQuoteOfTheDay() throws MessagingException, IOException {

		try {
			LOG.info("Email sender");
			QuoteOfTheDay quoteOfTheDay = quoteService.getQuoteOfTheDay();
			String url = quoteOfTheDay.getQuote().getUrl();
			String subject = "Quote of the day";
			String body = "Here is the quote of the day : " + url;
			System.out.println(url);
			emailService.sendMail(subject, body, "balaji@nimapinfotech.com");

//			this.sendEmail("balaji@nimapinfotech.com", subject, body);

		} catch (Exception e) {
			System.err.println("Could not send mail because " + e.getMessage());
		}
	}

//	private void sendEmail(String recipient, String subject, String body) throws MessagingException {
//		SimpleMailMessage message = new SimpleMailMessage();
//		message.setTo(recipient);
//		message.setSubject(subject);
//		message.setText(body);
//		javaMailSender.send(message);
//	}
//	@GetMapping("/qotd")
//	public ResponseEntity<?> getQuoteOfTheDay() throws IOException {
//		QuoteOfTheDay quoteOfTheDay = quoteService.getQuoteOfTheDay();
//		return ResponseEntity.ok().body(quoteOfTheDay.getQuote().getUrl());
//	}

}
