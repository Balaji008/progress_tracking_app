package com.todoapplication.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todoapplication.dto.UserDto;
import com.todoapplication.entities.UserEntity;
import com.todoapplication.payloads.ResponseDto;
import com.todoapplication.service.InviteService;
import com.todoapplication.serviceimpl.SamlService;

@RestController
@RequestMapping("/saml")
public class SamlController {

	@Autowired
	private SamlService samlService;

	@Value("${sso.redirect.url}")
	private String REDIRECT_URL;

	@Autowired
	private InviteService inviteService;

	@GetMapping(path = "saml-login")
	public ResponseEntity<?> generateSAMLRequest(HttpServletResponse response) throws IOException {
		ResponseDto responseDto = new ResponseDto();
		try {
			String url = samlService.generateRedirectUrl();
			responseDto.setStatus(true);
			responseDto.setMessage("SAML request generated successfully");
			responseDto.setResult(Arrays.asList(url));
			return new ResponseEntity<>(responseDto, HttpStatus.OK);
		} catch (Exception e) {
			responseDto.setStatus(false);
			responseDto.setMessage(e.getMessage());
			return new ResponseEntity<>(responseDto, HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping(path = "saml-response", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public void processSamlResponse(HttpServletResponse response, @RequestBody MultiValueMap<String, String> paramMap)
			throws Exception {
		String samlResponse = paramMap.get("SAMLResponse").get(0);
		String email = samlService.ssoAuthentication(samlResponse);
		if (email != null) {
			UserEntity user = this.samlService.getUserByEmail(email);
			UUID uuid = UUID.randomUUID();
			String code = uuid.toString();
			this.inviteService.deleteByEmail(user.getEmail());
			this.inviteService.add(code, user.getEmail());
			String redirectUrl = this.REDIRECT_URL + code;
			response.sendRedirect(redirectUrl);
		}
	}

	@PostMapping(path = "validateUserBySSO")
	public ResponseEntity<?> validatedUserBySSO(@RequestBody Map<String, String> map) {
		ResponseDto responseDto = new ResponseDto();
		try {
			String value = map.get("id");
			if (value == null) {
				throw new IllegalArgumentException("Invalid creadential,Please login again.");
			}
			boolean matches = value
					.matches("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[89aAbB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}");
			if (matches) {
				String email = this.samlService.validateUUID(value);
				System.err.println(email);
				if (email == null) {
					return new ResponseEntity<ResponseDto>(
							new ResponseDto("Invalid creadential,Please login again.", false, null), HttpStatus.OK);
				}
				try {
					Map<Object, Object> accessToken = samlService.generateAccessToken(email);
					if (accessToken.containsKey("ERROR")) {

						responseDto.setStatus(false);
						responseDto.setMessage(accessToken.get("MSG").toString());
					} else {
						responseDto.setStatus(true);
						responseDto.setMessage(accessToken.get("TOKEN").toString());
						List<UserDto> list = new ArrayList<>();
						UserDto userDto = (UserDto) accessToken.get("USER");
						System.err.println(userDto);
						list.add((UserDto) accessToken.get("USER"));
						responseDto.setResult(list);
						this.inviteService.deleteByEmail(email);
					}
				} catch (Exception e) {
					this.inviteService.deleteByEmail(email);
					responseDto.setMessage(e.getMessage());
					responseDto.setStatus(false);
					return ResponseEntity.ok(responseDto);
				}
			} else {
				responseDto.setMessage("Invalid creadential,Please login again.");
				responseDto.setStatus(false);
			}
			return ResponseEntity.ok(responseDto);
		} catch (Exception e) {
			responseDto.setMessage(e.getMessage());
			responseDto.setStatus(false);
			return ResponseEntity.ok(responseDto);
		}
	}

	@GetMapping(path = "samlRequest")
	public ResponseEntity<?> samlRequest() {
		ResponseDto response = new ResponseDto();
		try {
			String samlRequest = this.samlService.generateRedirectUrlUsingXMLDocument();
			response.setMessage("SAML Request Generated Successfully");
			response.setStatus(true);
			response.setResult(Arrays.asList(samlRequest));
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			response.setStatus(false);
			response.setMessage(e.getMessage());
			return ResponseEntity.ok(response);
		}
	}
}
