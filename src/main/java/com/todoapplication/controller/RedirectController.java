package com.todoapplication.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.todoapplication.payloads.SuccessResponseDto;

@RestController
public class RedirectController {

	@GetMapping("/redirect")
	public ResponseEntity<?> redirectToUrl(HttpServletResponse response) throws IOException {
		@SuppressWarnings("unused")
		String url = "https://gplalchemy-qa.nimapinfotech.com/redirect";
		Cookie cookie = new Cookie("my-cookie", "cookie-value");
//		cookie.setPath("/");
		String redirectUrl = "https://gplalchemy-qa.nimapinfotech.com/redirect";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Jwt-Token",
				"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJiYWxhamlAbmltYXBpbmZvdGVjaC5jb20iLCJuYW1lIjoiQmFsYWppIEdvc2F2aSIsInR5cGUiOiJhY2Nlc3MiLCJleHAiOjE2ODA2OTMyMzUsImlhdCI6MTY4MDY5MjMzNSwianRpIjoiMSJ9.lNRkwD04wQYW6UEZKS8tdzpyr-nTRg2oOiyfva-AFDsV0Xgc6ldeOHOR12m0HNJcpZi_HtiivobSCEKj6XMqEw");
		response.addCookie(cookie);
		response.sendRedirect(redirectUrl);
		response.addHeader("Jwt-Token",
				"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJiYWxhamlAbmltYXBpbmZvdGVjaC5jb20iLCJuYW1lIjoiQmFsYWppIEdvc2F2aSIsInR5cGUiOiJhY2Nlc3MiLCJleHAiOjE2ODA2OTMyMzUsImlhdCI6MTY4MDY5MjMzNSwianRpIjoiMSJ9.lNRkwD04wQYW6UEZKS8tdzpyr-nTRg2oOiyfva-AFDsV0Xgc6ldeOHOR12m0HNJcpZi_HtiivobSCEKj6XMqEw");
		headers.add("Set-Cookie", String.format("my-cookie=%s; Path=/", "cookie-value"));
		headers.add("Location", redirectUrl);

		return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto("success", "success", headers),
				HttpStatus.FOUND);
	}
//
//	@GetMapping("/redirect")
//	public RedirectView redirectToUrl(HttpServletResponse response) throws IOException {
//		String url = "https://gplalchemy-qa.nimapinfotech.com/redirect";
//		String redirectUrl = "https://gplalchemy-qa.nimapinfotech.com/redirect";
//		RedirectView redirectView = new RedirectView(redirectUrl, true);
//		Cookie cookie = new Cookie("my-cookie", "cookie-value");
//		cookie.setPath("/");
//		response.addCookie(cookie);
//		redirectView.addStaticAttribute("Jwt-Token",
//				"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJiYWxhamlAbmltYXBpbmZvdGVjaC5jb20iLCJuYW1lIjoiQmFsYWppIEdvc2F2aSIsInR5cGUiOiJhY2Nlc3MiLCJleHAiOjE2ODA2OTMyMzUsImlhdCI6MTY4MDY5MjMzNSwianRpIjoiMSJ9.lNRkwD04wQYW6UEZKS8tdzpyr-nTRg2oOiyfva-AFDsV0Xgc6ldeOHOR12m0HNJcpZi_HtiivobSCEKj6XMqEw");
//		return redirectView;
//	}

	@PostMapping("/process-form")
	public String processForm(ServerHttpRequest request) {
		// Get the parameter names from the request
//		System.out.println(request.toString());
//		System.err.print("Form data");
		// Iterate through headers
		Map<String, Object> requestData = new HashMap<>();
		Map<String, String> headers = new HashMap<>();
		request.getHeaders().forEach((key, value) -> headers.put(key, value.get(0)));
		requestData.put("headers", headers);

		// Iterate through query parameters
		Map<String, String> queryParams = new HashMap<>();
		request.getQueryParams().forEach((key, value) -> queryParams.put(key, value.get(0)));
		requestData.put("queryParameters", queryParams);

		for (Map.Entry<String, Object> map : requestData.entrySet()) {
			System.err.println(map.getKey());
			System.err.println(map.getValue());
		}
		// Iterate through path variables
//		Map<String, String> pathVariables = new HashMap<>();
//		request.getPathVariables().forEach((key, value) -> pathVariables.put(key, value));
//		requestData.put("pathVariables", pathVariables);

		return "success";
	}

	@PostMapping(value = "/url-encoded", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity<String> postExample(@RequestBody MultiValueMap<String, String> formData) {
		// Do something with the form data
//		List<String> list = formData.get("SAMLResponse");
		String first = formData.getFirst("SAMLResponse");
//		String name = formData.getFirst("name");
//		String email = formData.getFirst("email");

		// Return a response
		return ResponseEntity.ok().body("Received form data: " + first);
	}
}
