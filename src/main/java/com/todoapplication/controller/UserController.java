package com.todoapplication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todoapplication.ilistdto.IListUserDto;
import com.todoapplication.payloads.ErrorResponseDto;
import com.todoapplication.payloads.SuccessResponseDto;
import com.todoapplication.service.UserService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.SuccessKeyConstants;
import com.todoapplication.util.SuccessMessageConstants;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;

	@PreAuthorize("hasRole('UserList')")
	@GetMapping
	public ResponseEntity<?> getAllUsers() {
		try {
			List<IListUserDto> users = this.userService.getAllUsers();

			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.USER_S031104,
					SuccessMessageConstants.USER_FETCHED, users), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(ErrorKeyConstants.USER_E031101, e.getMessage()),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('UserView')")
	@GetMapping("/{userId}")
	public ResponseEntity<?> getUserById(@PathVariable Long userId) {
		try {
			List<IListUserDto> user = this.userService.getUserById(userId);
			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.USER_S031104,
					SuccessMessageConstants.USER_FETCHED, user), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(ErrorKeyConstants.USER_E031101, e.getMessage()),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('UserDelete')")
	@DeleteMapping("/{userId}")
	public ResponseEntity<?> deleteUser( @PathVariable Long userId) {
		try {
			this.userService.deleteUser(userId);

			return new ResponseEntity<>(
					new SuccessResponseDto(SuccessKeyConstants.USER_S031103, SuccessMessageConstants.USER_DELETED),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(ErrorKeyConstants.USER_E031101, e.getMessage()),
					HttpStatus.BAD_REQUEST);
		}
	}
}
