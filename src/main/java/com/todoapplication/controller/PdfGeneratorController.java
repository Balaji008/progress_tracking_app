package com.todoapplication.controller;

import java.io.ByteArrayInputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todoapplication.service.PdfService;

@RestController
@RequestMapping("/pdf")
public class PdfGeneratorController {

	@Autowired
	private PdfService pdfService;

	@GetMapping
	public ResponseEntity<?> createPdf() {
		ByteArrayInputStream pdf = this.pdfService.createPdf();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("Content", "inline;file=abc.pdf");

		return ResponseEntity.ok().headers(httpHeaders).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(pdf));
	}

	@GetMapping("/generate-pdf")
	public void generatePdf(Model model, HttpServletResponse response) throws Exception {
		// retrieve data
		// ...
		// add data to the model
		model.addAttribute("image", "classpath:/templates/sppulogo1.jpg");
		model.addAttribute("fullName", "Balaji Gosavi");
		model.addAttribute("motherName", "Meena");
		model.addAttribute("gender", "Male");
		model.addAttribute("contactNo", "8862084507");
		model.addAttribute("email", "balaji995588@gmail.com");
		model.addAttribute("address", "Vadgaon Maval");
		model.addAttribute("pincode", "412106");
		model.addAttribute("collegeCode", "CEGP019340");
		model.addAttribute("collegeName",
				"Dr. D. Y. Patil Educational Academy Dr. D.Y. Patil School Of Engineering \r\n" + "Academy");
		model.addAttribute("collegeAddress",
				" Gut No 124, 126, Ambi, Talegaon Dabhade, Taluka Maval, District \r\n" + "Pune Ta: Mawal Dist: Pune");
		model.addAttribute("examination", "B.E. (Mechanical)");
		model.addAttribute("examMonth", "May");
		model.addAttribute("examYear", "2022");
		model.addAttribute("seatNo", "B151070853");
		model.addAttribute("prn", "72010883H");
		model.addAttribute("classOrGradeObtained", "First Class with Distinction");

		// ...

		// render the template using Thymeleaf
		String html = pdfService.renderHtml(model);

		// generate PDF using Flying Saucer
		pdfService.generatePdf(html, response);
	}
}
