package com.todoapplication.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.engine.jdbc.StreamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.todoapplication.dto.FileDto;
import com.todoapplication.payloads.Constants;
import com.todoapplication.payloads.SuccessResponseDto;
import com.todoapplication.service.FileService;

@RestController
@RequestMapping("/files")
public class FileController {

	@Autowired
	private FileService fileService;

	@Value("${project.files}")
	private String path;

	@PostMapping("/addFile")
	public ResponseEntity<?> addFile(@RequestParam(name = "file") MultipartFile file) throws Exception {
		String file2 = this.fileService.addFile(path, file);

		return new ResponseEntity<SuccessResponseDto>(
				new SuccessResponseDto("Success", "File uploaded successfully", file2), HttpStatus.OK);
	}

	@PostMapping("/addFileDto")
	public ResponseEntity<?> addFileDto(@RequestParam(name = "file") MultipartFile file, FileDto fileDto) {
		FileDto file2 = this.fileService.addFile(fileDto, path, file);

		return new ResponseEntity<SuccessResponseDto>(
				new SuccessResponseDto("Success", "File uploaded successfully", file2), HttpStatus.OK);
	}

//	@GetMapping(value = "/{id}")
//	public ResponseEntity<?> getFile(@PathVariable("id") Long id, HttpServletResponse response) {
//		InputStream file = this.fileService.getFile(id, path);
//
//		try {
//			final String type = "video/mp4";
//			response.setContentType(type);
//			StreamUtils.copy(file, response.getOutputStream());
//			response.flushBuffer();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		return ResponseEntity.ok().build();
//	}

	@GetMapping("/all")
	public ResponseEntity<?> getAllFiles(
			@RequestParam(value = "pageNumber", defaultValue = Constants.PAGE_NUMBER, required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = Constants.PAGE_SIZE, required = false) Integer pageSize,
			HttpServletResponse response) {

		List<byte[]> fileBytesList = this.fileService.getFileBytesList(pageNumber, pageSize, path);

		try {
			final String type = "video/mp4";
			response.setContentType(type);
			for (byte[] fileBytes : fileBytesList) {
				ByteArrayResource resource = new ByteArrayResource(fileBytes);
				StreamUtils.copy(resource.getInputStream(), response.getOutputStream());
			}
			response.flushBuffer();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ResponseEntity.ok().build();
	}

//	@GetMapping("/all")
//	public ResponseEntity<List<Resource>> getAllFiles(
//			@RequestParam(value = "pageNumber", defaultValue = Constants.PAGE_NUMBER, required = false) Integer pageNumber,
//			@RequestParam(value = "pageSize", defaultValue = Constants.PAGE_SIZE, required = false) Integer pageSize,
//			HttpServletResponse response) {
//		List<InputStream> files = this.fileService.getFiles(pageNumber, pageSize, path);
//
//		List<Resource> resources = new ArrayList<>();
//
//		try {
//			final String type = "video/mp4";
//			response.setContentType(type);
//
//			for (InputStream file : files) {
//				Resource resource = new InputStreamResource(file);
//				resources.add(resource);
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return ResponseEntity.ok().body(resources);
//	}

}
