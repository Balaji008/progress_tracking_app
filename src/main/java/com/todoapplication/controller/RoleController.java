package com.todoapplication.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todoapplication.dto.RoleDto;
import com.todoapplication.ilistdto.IRoleListDto;
import com.todoapplication.payloads.ErrorResponseDto;
import com.todoapplication.payloads.SuccessResponseDto;
import com.todoapplication.service.RoleService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;
import com.todoapplication.util.SuccessKeyConstants;
import com.todoapplication.util.SuccessMessageConstants;

@RestController
@RequestMapping("/roles")
public class RoleController {

	@Autowired
	private RoleService roleService;

	@PreAuthorize("hasRole('RoleAdd')")
	@PostMapping
	public ResponseEntity<?> createRole(@Valid @RequestBody RoleDto roleDto) {
		try {
			RoleDto createdRole = this.roleService.createRole(roleDto);
			if (createdRole != null) {
				return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.ROLE_S031201,
						SuccessMessageConstants.ROLE_ADDED, createdRole), HttpStatus.CREATED);
			}
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.ROLE_E031202, ErrorMessageConstants.ROLE_ALREADY_EXISTS),
					HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.ROLE_E031201, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('RoleUpdate')")
	@PutMapping("/{roleId}")
	public ResponseEntity<?> updateRole(@Valid @RequestBody RoleDto roleDto, @PathVariable Long roleId) {

		try {
			RoleDto updatedRole = this.roleService.updateRole(roleDto, roleId);

			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.ROLE_S031202,
					SuccessMessageConstants.ROLE_UPDATED, updatedRole), HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.ROLE_E031201, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('RoleList')")
	@GetMapping
	public ResponseEntity<?> getAllRoles() {

		try {
			List<IRoleListDto> roles = this.roleService.getAllRoles();
			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.ROLE_S031204,
					SuccessMessageConstants.ROLE_FETCHED, roles), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.ROLE_E031201, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('RoleDelete')")
	@DeleteMapping("/{roleId}")
	public ResponseEntity<?> deleteRoleById(@PathVariable Long roleId) {
		try {
			this.roleService.deleteRole(roleId);
			return new ResponseEntity<SuccessResponseDto>(
					new SuccessResponseDto(SuccessKeyConstants.ROLE_S031203, SuccessMessageConstants.ROLE_DELETED),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.ROLE_E031201, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('RoleView')")
	@GetMapping("/{roleId}")
	public ResponseEntity<?> getRoleById(@PathVariable Long roleId) {
		try {
			IRoleListDto role = this.roleService.getRoleById(roleId);
			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.ROLE_S031204,
					SuccessMessageConstants.ROLE_FETCHED, role), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.ROLE_E031201, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}
