package com.todoapplication.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todoapplication.dto.UserRoleDto;
import com.todoapplication.ilistdto.IUserRoleListDto;
import com.todoapplication.payloads.ErrorResponseDto;
import com.todoapplication.payloads.SuccessResponseDto;
import com.todoapplication.service.UserRoleService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;
import com.todoapplication.util.SuccessKeyConstants;
import com.todoapplication.util.SuccessMessageConstants;

@RestController
@RequestMapping("/userRole")
public class UserRoleController {

	@Autowired
	private UserRoleService userRoleService;

	@PreAuthorize("hasRole('UserRoleAdd')")
	@PostMapping
	public ResponseEntity<?> assignRoleToUser(@Valid @RequestBody UserRoleDto userRoleDto) {
		try {
			List<IUserRoleListDto> userRole = this.userRoleService.assignRoleToUser(userRoleDto);
			if (userRole != null) {
				return new ResponseEntity<SuccessResponseDto>(
						new SuccessResponseDto(SuccessKeyConstants.USER_ROLE_S031501,
								SuccessMessageConstants.USER_ROLE_ADDED, userRole),
						HttpStatus.CREATED);
			}
			return new ResponseEntity<ErrorResponseDto>(new ErrorResponseDto(ErrorKeyConstants.USER_ROLE_E031502,
					ErrorMessageConstants.USER_ROLE_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.USER_ROLE_E031501, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('UserRoleUpdate')")
	@PutMapping("/{id}")
	public ResponseEntity<?> updateUserRole(@Valid @RequestBody UserRoleDto userRoleDto, @PathVariable Long id) {
		try {
			List<IUserRoleListDto> updatedUserRole = this.userRoleService.updateUserRole(userRoleDto, id);

			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.USER_ROLE_S031502,
					SuccessMessageConstants.USER_ROLE_UPDATED, updatedUserRole), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.USER_ROLE_E031501, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('UserRoleDelete')")
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteUserRole(@PathVariable long id) {
		try {
			this.userRoleService.deleteUserRole(id);

			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.USER_ROLE_S031503,
					SuccessMessageConstants.USER_ROLE_DELETED), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.USER_ROLE_E031501, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('UserRoleList')")
	@GetMapping
	public ResponseEntity<?> getRolesOfAllUsers() {
		try {
			List<IUserRoleListDto> userRoles = this.userRoleService.getAllUserRoles();
			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.USER_ROLE_S031504,
					SuccessMessageConstants.USER_ROLE_FETCHED, userRoles), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.USER_ROLE_E031501, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}
