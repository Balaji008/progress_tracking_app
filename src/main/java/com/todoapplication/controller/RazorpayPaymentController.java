package com.todoapplication.controller;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.razorpay.Order;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.todoapplication.util.PaymentRequest;
import com.todoapplication.util.PaymentResponse;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@RestController
@RequestMapping("/api/payments")
public class RazorpayPaymentController {

	@Autowired
	private RazorpayClient razorpayClient;

	@Value("${Razorpay.key}")
	private String key;

	@Value("${Razorpay.secret}")
	private String secret;

	@PostMapping("/create")
	public ResponseEntity<?> createPayment(@RequestBody PaymentRequest paymentRequest) {
		try {

			JSONObject options = new JSONObject();
			options.put("amount", paymentRequest.getAmount()); // amount in paisa
			options.put("currency", "INR");
			options.put("receipt", paymentRequest.getReceiptId());
			options.put("payment_capture", 1);

			Order order = razorpayClient.orders.create(options);

			// Generate a payment ID using the order ID and a custom prefix
			String paymentId = "PAY_" + order.get("id");

			// Return the payment ID in the response
			return ResponseEntity.ok(new PaymentResponse(paymentId));
		} catch (RazorpayException e) {
			return ResponseEntity.badRequest().body("Failed to create payment: " + e.getMessage());
		}
	}

	@SuppressWarnings("deprecation")
	@GetMapping("/pay")
	public String initiatePayment(Model model) throws IOException {
		String orderId = "order_LQxeKpqRxzGr5C"; // replace with the actual order ID
		int amount = 50000; // amount in paise
		String currency = "INR";
		String receipt = "order_rcptid_11";
		boolean capture = true;

		// Create a RazorpayClient object
		String authString = this.key + ":" + this.secret;
		String base64AuthString = Base64.getEncoder().encodeToString(authString.getBytes());
		String razorpayBaseUrl = "https://api.razorpay.com/v1";
		OkHttpClient httpClient = new OkHttpClient();
//		RazorpayClient razorpayClient = new RazorpayClient(httpClient, base64AuthString, razorpayBaseUrl);

		// Set up the payment details
		Map<String, Object> paymentMap = new HashMap<>();
		paymentMap.put("amount", amount);
		paymentMap.put("currency", currency);
		paymentMap.put("receipt", receipt);
		paymentMap.put("capture", capture);
		paymentMap.put("notes", new HashMap<String, String>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				put("order_id", orderId);
			}
		});

		// Create the payment using the Razorpay API
		Request.Builder builder = new Request.Builder().url(razorpayBaseUrl + "/payments")
				.addHeader("Authorization", "Basic " + base64AuthString)
				.addHeader("Content-Type", "application/json; charset=utf-8").post(okhttp3.RequestBody
						.create(MediaType.parse("application/json; charset=utf-8"), new Gson().toJson(paymentMap)));
		Response response = httpClient.newCall(builder.build()).execute();
		String responseBody = response.body().string();
		Map<String, Object> paymentResponseMap = new Gson().fromJson(responseBody,
				new TypeToken<Map<String, Object>>() {
				}.getType());
		String paymentId = (String) paymentResponseMap.get("id");

		// Return the payment details to the client
		model.addAttribute("paymentId", paymentId);
		System.err.println(paymentId);
		model.addAttribute("orderId", orderId);
		model.addAttribute("amount", amount);
		model.addAttribute("currency", currency);
		model.addAttribute("key", this.key);

		return paymentId;
	}

	@PostMapping("/capture")
	public ResponseEntity<?> capturePayment(@RequestParam("payment_id") String paymentId,
			@RequestParam("amount") int amount) {
		try {
			JSONObject captureRequest = new JSONObject();
			captureRequest.put("amount", amount * 100);
			Payment payment = razorpayClient.payments.capture(paymentId, captureRequest);
			return ResponseEntity.ok(payment);
		} catch (RazorpayException e) {
			return ResponseEntity.badRequest().body("Failed to capture payment: " + e.getMessage());
		}
	}

//	@PostMapping("/create_order")
//	public ResponseEntity<String> createOrder(@RequestParam(name = "amount") int amount) {
//		try {
//			// Set up the HTTP connection
//			URL url = new URL("https://api.razorpay.com/v1/orders");
//			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//			conn.setDoOutput(true);
//			conn.setRequestMethod("POST");
//			conn.setRequestProperty("Content-Type", "application/json");
//			conn.setRequestProperty("Authorization", "Basic " + getBase64EncodedApiKeySecret());
//
//			// Set up the request body
//			String requestBody = "{\"amount\":" + amount
//					+ ",\"currency\":\"INR\",\"receipt\":\"receipt1\",\"payment_capture\":1}";
//			OutputStream os = conn.getOutputStream();
//			os.write(requestBody.getBytes());
//			os.flush();
//
//			// Get the response body and signature
//			BufferedReader br = new BufferedReader(
//					new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8));
//			StringBuilder sb = new StringBuilder();
//			String responseLine;
//			while ((responseLine = br.readLine()) != null) {
//				sb.append(responseLine.trim());
//			}
//			String responseBody = sb.toString();
//			String signature = conn.getHeaderField("x-razorpay-signature");
//
//			// Create the response JSON object with the response body and signature
//			JSONObject responseJson = new JSONObject();
//			responseJson.put("responseBody", responseBody);
//			responseJson.put("signature", signature);
//
//			return new ResponseEntity<String>(responseJson.toString(), HttpStatus.OK);
//
//		} catch (IOException e) {
//			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//
////	@GetMapping("/get-payment-id")
////	public ResponseEntity<String> getPaymentId(@RequestParam(name = "razorpayOrderId") String razorpayOrderId) {
////
////		try {
////			// Initialize the Razorpay client with your API key and secret key
////			RazorpayClient client = new RazorpayClient(this.razorpayApiKey, this.razorpayApiSecret);
////
////			// Fetch the payment details using the Razorpay order ID
////			Payment payment = client.payments.fetch(razorpayOrderId);
////
////			// Return the payment ID
////			return new ResponseEntity<>(payment.get("id"), HttpStatus.OK);
////		} catch (RazorpayException e) {
////			// Handle the exception as per your application's needs
////			return new ResponseEntity<>("Error fetching payment ID: " + e.getMessage(),
////					HttpStatus.INTERNAL_SERVER_ERROR);
////		}
////	}
//
//	@PostMapping("/create-payment")
//	public ResponseEntity<String> createPayment(@RequestBody PaymentRequestDto paymentRequest) {
//		try {
//			// Initialize the Razorpay client with your API key and secret key
//			RazorpayClient client = new RazorpayClient(this.razorpayApiKey, this.razorpayApiSecret);
//
//			// Create a payment request with the given order ID and amount
//			Payment payment = client.payments
//					.createJsonPayment(new JSONObject().put("amount", paymentRequest.getAmount()).put("currency", "INR")
//							.put("receipt", paymentRequest.getReceipt()).put("payment_capture", 1)
//							.put("order_id", paymentRequest.getOrderId()));
//
//			// Return the payment ID
//			return ResponseEntity.ok(payment.get("id").toString());
//		} catch (RazorpayException e) {
//			// Handle any exceptions
//			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
//		}
//	}
//
//	@PostMapping("/verify_signature")
//	public ResponseEntity<String> verifySignature(@RequestParam(name = "razorpay_order_id") String orderId,
//			@RequestParam(name = "razorpay_payment_id") String paymentId,
//			@RequestParam(name = "razorpay_signature") String signature) {
//		try {
//			// Create the signature payload
//			String payload = orderId + "|" + paymentId;
//
//			// Create the HMAC-SHA256 signature
//			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
//			SecretKeySpec secret_key = new SecretKeySpec(razorpayApiSecret.getBytes(), "HmacSHA256");
//			sha256_HMAC.init(secret_key);
//			byte[] signatureBytes = sha256_HMAC.doFinal(payload.getBytes());
//			String expectedSignature = Hex.encodeHexString(signatureBytes);
//
//			// Compare the expected signature with the received signature
//			if (expectedSignature.equals(signature)) {
//				return new ResponseEntity<String>("Signature is valid.", HttpStatus.OK);
//			} else {
//				return new ResponseEntity<String>("Signature is not valid.", HttpStatus.BAD_REQUEST);
//			}
//		} catch (Exception e) {
//			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//
//	@PostMapping("/complete_payment")
//	public ResponseEntity<String> completePayment(@RequestParam(name = "razorpay_order_id") String orderId,
//			@RequestParam(name = "razorpay_payment_id") String paymentId,
//			@RequestParam(name = "razorpay_signature") String signature) {
//		try {
//			// Verify the signature first
//			ResponseEntity<String> signatureResponse = verifySignature(orderId, paymentId, signature);
//			if (signatureResponse.getStatusCode() != HttpStatus.OK) {
//				return signatureResponse;
//			}
//
//			// Set up the HTTP connection
//			URL url = new URL("https://api.razorpay.com/v1/payments/" + paymentId + "/capture");
//			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//			conn.setRequestMethod("POST");
//			conn.setRequestProperty("Content-Type", "application/json");
//			conn.setRequestProperty("Authorization", "Basic " + getBase64EncodedApiKeySecret());
//
//			// Set up the request body
//			String requestBody = "{\"amount\":null,\"currency\":null}";
//			OutputStream os = conn.getOutputStream();
//			os.write(requestBody.getBytes());
//			os.flush();
//
//			// Get the response body
//			BufferedReader br = new BufferedReader(
//					new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8));
//			StringBuilder sb = new StringBuilder();
//			String responseLine;
//			while ((responseLine = br.readLine()) != null) {
//				sb.append(responseLine.trim());
//			}
//			JSONObject responseJson = new JSONObject(sb.toString());
//
//			return new ResponseEntity<String>("Payment successful.", HttpStatus.OK);
//
//		} catch (IOException e) {
//			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//
//	private String getBase64EncodedApiKeySecret() {
//
//		String plainCredentials = this.razorpayApiKey + ":" + this.razorpayApiSecret;
//		byte[] plainCredentialsBytes = plainCredentials.getBytes(StandardCharsets.UTF_8);
//		String base64Credentials = Base64.getEncoder().encodeToString(plainCredentialsBytes);
//		return base64Credentials;
//	}

}
