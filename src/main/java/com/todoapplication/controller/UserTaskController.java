package com.todoapplication.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todoapplication.dto.UserTaskDto;
import com.todoapplication.ilistdto.IUserTaskListDto;
import com.todoapplication.payloads.ErrorResponseDto;
import com.todoapplication.payloads.SuccessResponseDto;
import com.todoapplication.service.UserTaskService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;
import com.todoapplication.util.SuccessKeyConstants;
import com.todoapplication.util.SuccessMessageConstants;

@RestController
@RequestMapping("/userTasks")
public class UserTaskController {

	@Autowired
	private UserTaskService userTaskService;

	@PreAuthorize("hasRole('UserTaskAdd')")
	@PostMapping
	public ResponseEntity<?> assignTaskToUser(@Valid @RequestBody UserTaskDto userTaskDto) {
		try {
			UserTaskDto userTask = this.userTaskService.assignTaskToUser(userTaskDto);
			if (userTask != null) {
				return new ResponseEntity<SuccessResponseDto>(
						new SuccessResponseDto(SuccessKeyConstants.USER_TASK_S031701,
								SuccessMessageConstants.USER_TASK_ADDED, userTask),
						HttpStatus.CREATED);
			}

			return new ResponseEntity<ErrorResponseDto>(new ErrorResponseDto(ErrorKeyConstants.USER_TASK_E031702,
					ErrorMessageConstants.USER_TASK_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.USER_ROLE_E031501, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('UserTaskUpdate')")
	@PutMapping("/{id}")
	public ResponseEntity<?> updateTaskOfUser(@Valid @RequestBody UserTaskDto userTaskDto, @PathVariable Long id) {
		try {
			UserTaskDto userTask = this.userTaskService.updateTaskOfUser(userTaskDto, id);

			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.USER_TASK_S031702,
					SuccessMessageConstants.USER_TASK_UPDATED, userTask), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.USER_TASK_E031701, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('UserTaskDelete')")
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteTaskOfUser(@PathVariable Long id) {
		try {
			this.userTaskService.deleteTaskOfUser(id);

			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.USER_TASK_S031703,
					SuccessMessageConstants.USER_TASK_DELETED), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.USER_TASK_E031701, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PatchMapping("/{taskId}")
	public ResponseEntity<?> updateTaskStatus(@RequestAttribute("User-id") Long userId, @PathVariable Long taskId) {

		try {
			List<IUserTaskListDto> userTask = this.userTaskService.updateTaskStatus(userId, taskId);

			if (userTask != null) {
				return new ResponseEntity<SuccessResponseDto>(
						new SuccessResponseDto(SuccessKeyConstants.USER_TASK_S031705,
								SuccessMessageConstants.USER_TASK_STATUS_UPDATED, userTask),
						HttpStatus.OK);
			}

			else
				return new ResponseEntity<ErrorResponseDto>(new ErrorResponseDto(ErrorKeyConstants.USER_TASK_E031703,
						ErrorMessageConstants.USER_TASK_STATUS_ALREADY_COMPLETED), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.USER_TASK_E031701, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	// @PreAuthorize("hasRole('UserTaskList')")
	@GetMapping("/all")
	public ResponseEntity<?> getTasksOfAllUsers(@RequestAttribute("User-permissions") List<String> permissions,
			@RequestAttribute("User-id") Long userId) {
		try {

			List<IUserTaskListDto> userTasks = this.userTaskService.getTasksOfAllUsers(permissions, userId);

			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.USER_TASK_S031704,
					SuccessMessageConstants.USER_TASK_FETCHED, userTasks), HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.USER_TASK_E031701, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('UserTaskView')")
	@GetMapping("/{userId}")
	public ResponseEntity<?> getTasksOfUserById(@PathVariable Long userId) {
		try {
			List<IUserTaskListDto> userTasks = this.userTaskService.getTaskOfUserById(userId);
			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.USER_TASK_S031704,
					SuccessMessageConstants.USER_TASK_FETCHED, userTasks), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.USER_TASK_E031701, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}
