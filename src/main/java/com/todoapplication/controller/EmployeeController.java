package com.todoapplication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.todoapplication.dto.EmployeeDto;
import com.todoapplication.payloads.ErrorResponseDto;
import com.todoapplication.payloads.SuccessResponseDto;
import com.todoapplication.service.EmployeeService;
import com.todoapplication.util.EmployeeBulkUploadUtil;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;
import com.todoapplication.util.SuccessKeyConstants;
import com.todoapplication.util.SuccessMessageConstants;

@RestController
@RequestMapping("employees")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@PreAuthorize("hasRole('EmployeeBulkUpload')")
	@PostMapping("/uploadcsv")
	public ResponseEntity<?> uploadCsv(@RequestParam("file") MultipartFile file) {
		try {
			if (EmployeeBulkUploadUtil.checkCsvFormat(file) && !file.isEmpty()) {
				List<EmployeeDto> employees = this.employeeService.uploadCsvFile(file);
				if (employees != null) {
					ResponseEntity<SuccessResponseDto> responseEntity = new ResponseEntity<SuccessResponseDto>(
							new SuccessResponseDto(SuccessKeyConstants.FILE_S031901,
									SuccessMessageConstants.FILE_UPLOADED),
							HttpStatus.CREATED);

					return responseEntity;
				} else {
					ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
							new ErrorResponseDto(ErrorKeyConstants.FILE_E032101, ErrorMessageConstants.INVALID_FILE),
							HttpStatus.BAD_REQUEST);

					return responseEntity;
				}
			} else {
				ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
						new ErrorResponseDto(ErrorKeyConstants.FILE_E032101, ErrorMessageConstants.INVALID_FILE),
						HttpStatus.BAD_REQUEST);

				return responseEntity;
			}
		}

		catch (Exception e) {
			ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.FILE_E032101, e.getMessage()), HttpStatus.BAD_REQUEST);

			return responseEntity;
		}
	}
}
