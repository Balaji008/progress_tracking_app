package com.todoapplication.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.todoapplication.dto.TaskDto;
import com.todoapplication.ilistdto.ITaskListDto;
import com.todoapplication.payloads.ErrorResponseDto;
import com.todoapplication.payloads.SuccessResponseDto;
import com.todoapplication.service.TaskService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;
import com.todoapplication.util.SuccessKeyConstants;
import com.todoapplication.util.SuccessMessageConstants;
import com.todoapplication.util.TaskUploadUtil;

@RestController
@RequestMapping("tasks")
public class TaskController {

	@Autowired
	private TaskService taskService;

	@PreAuthorize("hasRole('TaskAdd')")
	@PostMapping
	public ResponseEntity<?> createTask(@Valid @RequestBody TaskDto taskDto) {
		try {
			TaskDto createdTask = this.taskService.createTask(taskDto);
			if (createdTask != null) {
				return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.TASK_S031401,
						SuccessMessageConstants.TASK_ADDED, createdTask), HttpStatus.CREATED);
			}
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.TASK_E031402, ErrorMessageConstants.TASK_ALREADY_EXISTS),
					HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.TASK_E031401, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('TaskUpdate')")
	@PutMapping("/{id}")
	public ResponseEntity<?> updateTask(@Valid @RequestBody TaskDto taskDto, @PathVariable Long id) {

		try {
			TaskDto updatedTask = this.taskService.updateTask(taskDto, id);

			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.TASK_S031402,
					SuccessMessageConstants.TASK_UPDATED, updatedTask), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.TASK_E031401, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('TaskDelete')")
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteTask(@PathVariable Long id) {
		try {
			this.taskService.deleteTask(id);

			return new ResponseEntity<SuccessResponseDto>(
					new SuccessResponseDto(SuccessKeyConstants.TASK_S031403, SuccessMessageConstants.TASK_DELETED),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.TASK_E031401, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('TaskList')")
	@GetMapping
	public ResponseEntity<?> getAllTasks() {
		try {
			List<ITaskListDto> allTasks = this.taskService.getAllTasks();

			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.TASK_S031404,
					SuccessMessageConstants.TASK_FETCHED, allTasks), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.TASK_E031401, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('TaskUpload')")
	@PostMapping("/upload")
	public ResponseEntity<?> uploadTasks(@RequestParam("file") MultipartFile file) {
		try {
			if (TaskUploadUtil.checkExcelFormat(file) && !file.isEmpty()) {
				List<TaskDto> tasks = this.taskService.save(file);

				if (tasks != null) {
					ResponseEntity<SuccessResponseDto> responseEntity = new ResponseEntity<SuccessResponseDto>(
							new SuccessResponseDto(SuccessKeyConstants.FILE_S031901,
									SuccessMessageConstants.FILE_UPLOADED, tasks),
							HttpStatus.OK);
					return responseEntity;
				}

				else {
					ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
							new ErrorResponseDto(ErrorKeyConstants.FILE_E032101, ErrorMessageConstants.INVALID_FILE),
							HttpStatus.BAD_REQUEST);

					return responseEntity;
				}
			} else {
				ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
						new ErrorResponseDto(ErrorKeyConstants.FILE_E032101, ErrorMessageConstants.INVALID_FILE),
						HttpStatus.BAD_REQUEST);

				return responseEntity;
			}
		} catch (Exception e) {
			ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.FILE_E032101, e.getMessage()), HttpStatus.BAD_REQUEST);

			return responseEntity;
		}
	}

}