package com.todoapplication.controller;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiCallController {

	@GetMapping("/gitlab")
	public ResponseEntity<?> gitlab() throws IOException, InterruptedException {
		String url = "https://gitlab.com/Balaji008/todo-application";
		HttpRequest request = HttpRequest.newBuilder().GET().uri(URI.create(url)).build();

		HttpClient client = HttpClient.newBuilder().build();
		HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

		System.out.println(response.body());

		return ResponseEntity.ok(response.body());

	}

}
