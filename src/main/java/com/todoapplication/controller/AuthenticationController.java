package com.todoapplication.controller;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.todoapplication.dto.OtpDto;
import com.todoapplication.dto.OtpValidateDto;
import com.todoapplication.dto.UserDto;
import com.todoapplication.payloads.Constants;
import com.todoapplication.payloads.ErrorResponseDto;
import com.todoapplication.payloads.JwtAuthenticationRequest;
import com.todoapplication.payloads.JwtAuthenticationResponse;
import com.todoapplication.payloads.SuccessResponseDto;
import com.todoapplication.repositories.UserRepository;
import com.todoapplication.service.AuthService;
import com.todoapplication.service.OtpService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;
import com.todoapplication.util.SuccessKeyConstants;
import com.todoapplication.util.SuccessMessageConstants;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {

	@Autowired
	private AuthService authService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private OtpService otpService;

	private static Logger log = Logger.getLogger(AuthenticationController.class);

//	@GetMapping("decrypt")
//	public String decrypt() {
//
//	}

	@PostMapping("/register")
	public ResponseEntity<?> registerUser(@Valid @RequestBody UserDto userDto) {
		try {
			if (userRepository.existsByEmail(userDto.getEmail())) {

				return new ResponseEntity<>(new ErrorResponseDto(ErrorKeyConstants.USER_E031102,
						ErrorMessageConstants.EMAIL_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);
			}

			if (userDto.getEmail().matches(Constants.EMAIL_REGEX)
					&& userDto.getPassword().matches(Constants.PASSWORD_REGEX)) {
				UserDto registerdUser = this.authService.registerUser(userDto);
				ResponseEntity<SuccessResponseDto> responseEntity = new ResponseEntity<>(
						new SuccessResponseDto(SuccessKeyConstants.USER_S031101,
								SuccessMessageConstants.USER_REGISTERED, registerdUser),
						HttpStatus.OK);
				log.debug(responseEntity);
				return responseEntity;
			}

			else if (!userDto.getEmail().matches(Constants.EMAIL_REGEX)) {
				ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<>(
						new ErrorResponseDto(ErrorKeyConstants.USER_E031103,
								ErrorMessageConstants.INVALID_EMAIL_OR_PASSWORD),
						HttpStatus.BAD_REQUEST);
				// log.debug(responseEntity);
				return responseEntity;
			}

			else if (!userDto.getPassword().matches(Constants.PASSWORD_REGEX)) {
				ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<>(
						new ErrorResponseDto(ErrorKeyConstants.USER_E031104,
								ErrorMessageConstants.INVALID_EMAIL_OR_PASSWORD),
						HttpStatus.BAD_REQUEST);
				// log.debug(responseEntity);
				return responseEntity;
			}

			ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<>(
					new ErrorResponseDto(ErrorKeyConstants.USER_E031105,
							ErrorMessageConstants.INVALID_EMAIL_OR_PASSWORD),
					HttpStatus.BAD_REQUEST);
			log.debug(responseEntity);
			return responseEntity;
		} catch (Exception e) {
			ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<>(
					new ErrorResponseDto(ErrorKeyConstants.USER_E031105, e.getMessage()), HttpStatus.BAD_REQUEST);
			log.debug(e.getStackTrace());
			return responseEntity;
		}
	}

	@PostMapping("/login")
	public ResponseEntity<?> createToken(@Valid @RequestBody JwtAuthenticationRequest request) {
		try {
			JwtAuthenticationResponse response = this.authService.login(request);

			if (response.getAccessToken() != null) {
				SuccessResponseDto responseDto = new SuccessResponseDto();
				responseDto.setKey(SuccessKeyConstants.USER_S031107);
				responseDto.setMessage(SuccessMessageConstants.LOGIN_SUCCESSFUL);
				responseDto.setData(response);

				ResponseEntity<SuccessResponseDto> responseEntity = new ResponseEntity<SuccessResponseDto>(responseDto,
						HttpStatus.OK);

				log.debug(responseEntity);
				return responseEntity;
			}
			ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.USER_E031105,
							ErrorMessageConstants.INVALID_EMAIL_OR_PASSWORD),
					HttpStatus.BAD_REQUEST);
			log.debug(responseEntity);
			return responseEntity;
		} catch (Exception e) {
			ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.USER_E031105, e.getMessage()), HttpStatus.BAD_REQUEST);

			return responseEntity;
		}
	}

	@PostMapping("/refreshToken")
	public ResponseEntity<?> generateAccessTokenFromRefreshToken(@RequestParam(defaultValue = "") String refreshToken) {

		try {
			String accessToken = this.authService.refreshAndGetAuthenticationToken(refreshToken);

			if (accessToken != null) {
				return new ResponseEntity<>(new SuccessResponseDto(SuccessKeyConstants.TOKEN_S031801,
						SuccessMessageConstants.TOKEN_GENERATED, accessToken), HttpStatus.OK);
			}

			return new ResponseEntity<>(
					new ErrorResponseDto(ErrorKeyConstants.TOKEN_E031901, ErrorMessageConstants.INVALID_TOKEN),
					HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<>(
					new ErrorResponseDto(ErrorKeyConstants.TOKEN_E031901, ErrorMessageConstants.INVALID_TOKEN),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/generateOtp")
	public ResponseEntity<?> generateOtp(@Valid @RequestBody OtpDto otpDto) {
		try {
			String otp = null;
			otp = this.otpService.sendOtp(otpDto);
//			System.err.println(otp);
			if (otp != null) {
				return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.OTP_S032001,
						SuccessMessageConstants.OTP_SENT, "Otp is valid for 15 minutes"), HttpStatus.OK);
			}
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.USER_E031103, ErrorMessageConstants.INVALID_EMAIL),
					HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.USER_E031103, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/validateOtp")
	public ResponseEntity<?> validateOtp(@Valid @RequestBody OtpValidateDto otpValidateDto) {
		try {
			boolean flag = this.otpService.validateOtp(otpValidateDto.getEmail(), otpValidateDto.getOtp());
			System.out.println(flag);
			if (flag) {
				JwtAuthenticationResponse response = this.authService.loginWithOtp(otpValidateDto.getEmail());

				ResponseEntity<SuccessResponseDto> responseEntity = new ResponseEntity<SuccessResponseDto>(
						new SuccessResponseDto(SuccessKeyConstants.USER_S031107,
								SuccessMessageConstants.LOGIN_SUCCESSFUL, response),
						HttpStatus.OK);
				return responseEntity;
			}

			ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.OTP_E032202, ErrorMessageConstants.INVALID_OTP),
					HttpStatus.BAD_REQUEST);

			return responseEntity;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("otp");
			ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.OTP_E032202, e.getMessage()), HttpStatus.BAD_REQUEST);

			return responseEntity;
		}
	}
}
