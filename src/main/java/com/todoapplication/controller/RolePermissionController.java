package com.todoapplication.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todoapplication.dto.RolePermissionDto;
import com.todoapplication.ilistdto.IRolePermissionListDto;
import com.todoapplication.payloads.ErrorResponseDto;
import com.todoapplication.payloads.SuccessResponseDto;
import com.todoapplication.service.RolePermissionService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;
import com.todoapplication.util.SuccessKeyConstants;
import com.todoapplication.util.SuccessMessageConstants;

@RestController
@RequestMapping("/rolePermissions")
public class RolePermissionController {

	@Autowired
	private RolePermissionService rolePermissionService;

	@PreAuthorize("hasRole('RolePermissionAdd')")
	@PostMapping
	public ResponseEntity<?> assignPermissionToRole(@Valid @RequestBody RolePermissionDto rolePermissionDto) {

		try {
			IRolePermissionListDto rolePermission = this.rolePermissionService
					.assignPermissionsToRole(rolePermissionDto);

			if (rolePermission != null) {

				return new ResponseEntity<SuccessResponseDto>(
						new SuccessResponseDto(SuccessKeyConstants.ROLE_PERMISSION_S031601,
								SuccessMessageConstants.ROLE_PERMISSION_ADDED, rolePermission),
						HttpStatus.CREATED);
			}

			return new ResponseEntity<ErrorResponseDto>(new ErrorResponseDto(ErrorKeyConstants.ROLE_PERMISSION_E031602,
					ErrorMessageConstants.ROLE_PERMISSION_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.ROLE_PERMISSION_E031602, e.getMessage()),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('RolePermissionUpdate')")
	@PutMapping("/{id}")
	public ResponseEntity<?> updatePermissionsOfRole(@Valid @RequestBody RolePermissionDto rolePermissionDto,
			@PathVariable Long id) {
		try {
			IRolePermissionListDto rolePermission = this.rolePermissionService
					.updatePermissionsOfRole(rolePermissionDto, id);

			return new ResponseEntity<SuccessResponseDto>(
					new SuccessResponseDto(SuccessKeyConstants.ROLE_PERMISSION_S031602,
							SuccessMessageConstants.ROLE_PERMISSION_UPDATED, rolePermission),
					HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.ROLE_PERMISSION_E031601, e.getMessage()),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('RolePermissionDelete')")
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deletePermissionOfRole(@PathVariable Long id) {

		try {
			this.rolePermissionService.deletePermissionOfRole(id);

			return new ResponseEntity<SuccessResponseDto>(
					new SuccessResponseDto(SuccessKeyConstants.ROLE_PERMISSION_S031603,
							SuccessMessageConstants.ROLE_PERMISSION_DELETED),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.ROLE_PERMISSION_E031601, e.getMessage()),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('RolePermissionList')")
	@GetMapping
	public ResponseEntity<?> getAllRolePermissions() {

		try {
			List<IRolePermissionListDto> rolePermissions = this.rolePermissionService.getAllPermissionsOfRole();

			return new ResponseEntity<SuccessResponseDto>(
					new SuccessResponseDto(SuccessKeyConstants.ROLE_PERMISSION_S031604,
							SuccessMessageConstants.ROLE_PERMISSION_FETCHED, rolePermissions),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.ROLE_PERMISSION_E031601, e.getMessage()),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('RolePermissionView')")
	@GetMapping("/{roleId}")
	public ResponseEntity<?> getAllPermissionsByRoleId(@PathVariable Long roleId) {
		try {
			IRolePermissionListDto rolePermissions = this.rolePermissionService.getAllPermissionsByRoleId(roleId);

			return new ResponseEntity<SuccessResponseDto>(
					new SuccessResponseDto(SuccessKeyConstants.ROLE_PERMISSION_S031604,
							SuccessMessageConstants.ROLE_PERMISSION_FETCHED, rolePermissions),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.ROLE_PERMISSION_E031601, e.getMessage()),
					HttpStatus.BAD_REQUEST);
		}
	}
}
