package com.todoapplication.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.razorpay.Order;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.todoapplication.dto.PaymentDto;

@RestController
@RequestMapping("/api/payment")
public class PaymentController {

	@Value("${Razorpay.key}")
	private String key;

	@Value("${Razorpay.secret}")
	private String secret;

	@PostMapping("/create-order")
	public ResponseEntity<?> createOrder(@RequestBody PaymentDto data) throws IOException {
		try {
			Long amount = data.getAmount();
			System.err.println(amount);
			RazorpayClient razorpayClient = new RazorpayClient(key, secret);

			// Create the payment

			// Create the order
			JSONObject orderRequest = new JSONObject();
			orderRequest.put("amount", amount * 100);
			orderRequest.put("currency", "INR");
			orderRequest.put("receipt", "txn_123");
			orderRequest.put("payment_capture", true);
//			orderRequest.put("payment_id", paymentId); // Add the payment ID to the order
//			System.err.println(paymentId);
			Order order = razorpayClient.orders.create(orderRequest);
			Object object = order.get("id");
			// Return the response

			JSONObject paymentRequest = new JSONObject();
			paymentRequest.put("amount", amount * 100);
			paymentRequest.put("currency", "INR");
			paymentRequest.put("receipt", "txn_123");
			System.err.println("payment");
			Payment payment = razorpayClient.payments.capture(object.toString(), paymentRequest);

			// Get the payment ID from the response
			String paymentId = payment.get("id");
			System.err.println("payment");
			JSONObject response = new JSONObject();
			response.put("orderId", object);
			response.put("paymentId", paymentId); // Return the payment ID in the response

			return new ResponseEntity<String>(response.toString(), HttpStatus.OK);

		} catch (RazorpayException e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public String generateSignature(String orderId, String paymentId, String secret) throws Exception {
		String signatureString = orderId + "|" + paymentId + "|" + secret;
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(signatureString.getBytes(StandardCharsets.UTF_8));
		return Base64.getEncoder().encodeToString(hash);
	}

//	@PostMapping("/create-order")
//	public ResponseEntity<?> createOrder(@RequestBody PaymentDto data) throws IOException {
//		try {
//			Long amount = data.getAmount();
//			System.err.println(amount);
//			RazorpayClient razorpayClient = new RazorpayClient(key, secret);
//			JSONObject jsonObject = new JSONObject();
//			jsonObject.put("amount", amount * 100);
//			jsonObject.put("currency", "INR");
//			jsonObject.put("receipt", "txn_123");
//
//			Order order = razorpayClient.orders.create(jsonObject);
//
//			// create a new JSONObject for payment options
//			JSONObject paymentOptions = new JSONObject();
//			Object orderId = null, currency = null, receipt = null;
//			Integer new_amount = 0;
//			if (order.has("id")) {
//				orderId = order.get("id");
//			}
//			if (order.has("amount")) {
//				new_amount = order.get("amount");
//			}
//			if (order.has("currency")) {
//				currency = order.get("currency");
//			}
//			if (order.has("receipt")) {
//				receipt = order.get("receipt");
//			}
//
//			paymentOptions.put("orderId", orderId);
//			paymentOptions.put("amount", new_amount / 100);
//			paymentOptions.put("currency", currency);
//			paymentOptions.put("receipt", receipt);
//
//			JSONObject customer = new JSONObject();
//			customer.put("name", "+919000090000");
//			customer.put("contact", "Gaurav Kumar");
//			customer.put("email", "gaurav.kumar@example.com");
//			paymentOptions.put("customer", customer);
//			JSONObject notify = new JSONObject();
//			notify.put("sms", true);
//			notify.put("email", true);
//			paymentOptions.put("notify", notify);
//			paymentOptions.put("reminder_enable", true);
//			JSONObject notes = new JSONObject();
//			notes.put("policy_name", "Jeevan Bima");
//			paymentOptions.put("notes", notes);
//			paymentOptions.put("callback_url", "https://example-callback-url.com/");
//			paymentOptions.put("callback_method", "get");
//
//			PaymentLink payment = razorpayClient.paymentLink.create(paymentOptions);
//			// add additional payment options
////			JSONObject paymentMethods = new JSONObject();
//
//			// add UPI payment option
////			JSONObject upi = new JSONObject();
////			upi.put("id", "8552024658@paytm");
////			upi.put("name", "UPI");
////			upi.put("description", "Pay using UPI");
////			paymentMethods.put("upi", upi);
////
////			// add card payment option
////			JSONObject card = new JSONObject();
////			card.put("id", "card");
////			card.put("name", "Debit/Credit Card");
////			card.put("description", "Pay using Debit/Credit Card");
////			paymentMethods.put("card", card);
////
////			// add QR code payment option
////			JSONObject qr = new JSONObject();
////			qr.put("id", "qr");
////			qr.put("name", "QR Code");
////
////			// read QR code image file as bytes and encode as Base64 string
////			byte[] imageBytes = Files.readAllBytes(Paths.get("src/main/resources/files/IMG-20230309-WA0002.jpg"));
////			String imageData = Base64.getEncoder().encodeToString(imageBytes);
////
////			qr.put("image", imageData);
////			qr.put("description", "Scan QR code to pay");
////
////			paymentMethods.put("qr", qr);
////
////			paymentOptions.put("paymentMethods", paymentMethods);
//			System.out.println(payment.toString());
//
//			return new ResponseEntity<String>(payment.toString(), HttpStatus.OK);
//		} catch (RazorpayException e) {
//			e.getMessage();
//			return new ResponseEntity<ErrorResponseDto>(new ErrorResponseDto("error", "Payment error"),
//					HttpStatus.BAD_REQUEST);
//		}
//	}

}
