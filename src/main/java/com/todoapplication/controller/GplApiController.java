package com.todoapplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todoapplication.util.GplApiIntegrationServiceImpl;

@RestController
@RequestMapping("/api/gpl")
public class GplApiController {

	@Autowired
	private GplApiIntegrationServiceImpl apiIntegrationServiceImpl;

	@GetMapping
	public ResponseEntity<?> gplApi() {
		String data = this.apiIntegrationServiceImpl.gplApi();

		return ResponseEntity.ok(data);
	}

//	@PostMapping("/getEmployeeData")
//	public ResponseEntity<String> getEmployeeData() {
//		String someMethod = dataService.someMethod();
//
//		return ResponseEntity.ok(someMethod);
//	}
}
