package com.todoapplication.controller;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.todoapplication.entities.AsyncDataEntity;
import com.todoapplication.ilistdto.IAsyncDataListDto;
import com.todoapplication.payloads.Constants;
import com.todoapplication.payloads.ErrorResponseDto;
import com.todoapplication.payloads.ListResponseDto;
import com.todoapplication.payloads.PeginationResponse;
import com.todoapplication.payloads.SuccessResponseDto;
import com.todoapplication.serviceimpl.AsyncServiceImpl;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.SuccessKeyConstants;
import com.todoapplication.util.SuccessMessageConstants;

@RestController
@RequestMapping("/async")
public class AsyncController {

	@Autowired
	private AsyncServiceImpl asyncService;

	private static final Logger LOGGER = LoggerFactory.getLogger(AsyncController.class);

//	@GetMapping
//	public ResponseEntity<?> getString() {
//		String reverse = this.asyncService.runInBackground();
//		System.out.println(reverse);
//		return ResponseEntity.ok(reverse);
//	}

//	@PostMapping
//	public ResponseEntity<?> uploadAllFile(@RequestParam(value = "files") MultipartFile files) {
//		try {
//
//			CompletableFuture<List<AsyncDataEntity>> future = asyncService.saveAllData(files.getInputStream());
//
//	        List<AsyncDataEntity> data = future.get();
//			ResponseEntity<SuccessResponseDto> responseEntity = new ResponseEntity<SuccessResponseDto>(
//					new SuccessResponseDto(SuccessKeyConstants.FILE_S031901, SuccessMessageConstants.FILE_UPLOADED),
//					HttpStatus.CREATED);
//			return responseEntity;
//
//		} catch (final Exception e) {
//			System.err.println("Message" + e.getMessage());
//			ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
//					new ErrorResponseDto(ErrorKeyConstants.FILE_E032101, e.getMessage()), HttpStatus.BAD_REQUEST);
//
//			return responseEntity;
//		}
//
//	}

	@PostMapping
	public ResponseEntity<?> uploadAllFile(@RequestParam(value = "files") MultipartFile files) {
		try {
			CompletableFuture<List<AsyncDataEntity>> future = asyncService.saveAllData(files.getInputStream());

			future.get();
			ResponseEntity<SuccessResponseDto> responseEntity = new ResponseEntity<SuccessResponseDto>(
					new SuccessResponseDto(SuccessKeyConstants.FILE_S031901, SuccessMessageConstants.FILE_UPLOADED),
					HttpStatus.CREATED);
			return responseEntity;

		} catch (final Exception e) {
//			String[] split2 = e.getMessage().split(":");
			String[] split = e.getMessage().split(":");
			if (split.length > 1) {
				LOGGER.error("Failed to upload file {}", e.getMessage());
				ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
						new ErrorResponseDto(ErrorKeyConstants.FILE_E032101, split[1].trim()), HttpStatus.BAD_REQUEST);
				return responseEntity;
			} else {
				ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
						new ErrorResponseDto(ErrorKeyConstants.FILE_E032101, e.getMessage()), HttpStatus.BAD_REQUEST);
				return responseEntity;
			}
		}
	}

//	@PostMapping
//	public ResponseEntity<?> uploadAllFile(@RequestParam(value = "files") MultipartFile files) {
//		try {
//			// Divide the file into 10 equal divisions
//			List<InputStream> divisions = divideFile(files.getInputStream(), 10);
//
//			// Save each division in background
//			List<CompletableFuture<List<AsyncDataEntity>>> futures = new ArrayList<>();
//			for (InputStream division : divisions) {
//				futures.add(asyncService.saveAllData(division));
//			}
//
//			// Wait for all futures to complete
//			List<List<AsyncDataEntity>> allData = new ArrayList<>();
//			for (CompletableFuture<List<AsyncDataEntity>> future : futures) {
//				allData.add(future.get());
//			}
//
//			ResponseEntity<SuccessResponseDto> responseEntity = new ResponseEntity<SuccessResponseDto>(
//					new SuccessResponseDto(SuccessKeyConstants.FILE_S031901, SuccessMessageConstants.FILE_UPLOADED),
//					HttpStatus.CREATED);
//			return responseEntity;
//
//		} catch (final Exception e) {
//			String[] split = e.getMessage().split(":");
//			if (split.length > 1) {
//				LOGGER.error("Failed to upload file {}", e.getMessage());
//				ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
//						new ErrorResponseDto(ErrorKeyConstants.FILE_E032101, split[1].trim()), HttpStatus.BAD_REQUEST);
//				return responseEntity;
//			} else {
//				ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
//						new ErrorResponseDto(ErrorKeyConstants.FILE_E032101, e.getMessage()), HttpStatus.BAD_REQUEST);
//				return responseEntity;
//			}
//		}
//	}
//
//	private List<InputStream> divideFile(InputStream inputStream, int numDivisions) throws IOException {
//		byte[] data = IOUtils.toByteArray(inputStream);
//		int fileSize = data.length;
//		int divisionSize = fileSize / numDivisions;
//
//		List<InputStream> divisions = new ArrayList<>();
//		for (int i = 0; i < numDivisions; i++) {
//			int start = i * divisionSize;
//			int end = (i == numDivisions - 1) ? fileSize : (i + 1) * divisionSize;
//			divisions.add(new ByteArrayInputStream(Arrays.copyOfRange(data, start, end)));
//		}
//
//		return divisions;
//	}

	@PostMapping("/file")
	public ResponseEntity<?> uploadFile(@RequestParam(value = "files") MultipartFile files) {
		try {
//			ArrayList<List<AsyncDataEntity>> saveData = new ArrayList<>();
//			for (final MultipartFile file : files) {
//				saveData.add(asyncService.saveData(file.getInputStream()));
//			}
//			return ResponseEntity.status(HttpStatus.CREATED).build();
//			boolean flag = true;
//			for (int i = 0; i < saveData.size(); i++) {
//				if (saveData.get(i) == null) {
//					flag = false;
//					System.err.println("Null");
//				}
//			}
//			if (flag) {
			asyncService.saveData(files.getInputStream());
			ResponseEntity<SuccessResponseDto> responseEntity = new ResponseEntity<SuccessResponseDto>(
					new SuccessResponseDto(SuccessKeyConstants.FILE_S031901, SuccessMessageConstants.FILE_UPLOADED),
					HttpStatus.CREATED);
			return responseEntity;
//			}
////			ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
////					new ErrorResponseDto(ErrorKeyConstants.FILE_E032101, ErrorMessageConstants.INVALID_FILE),
////					HttpStatus.BAD_REQUEST);
////
////			return responseEntity;

		} catch (final Exception e) {
			ResponseEntity<ErrorResponseDto> responseEntity = new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.FILE_E032101, e.getMessage()), HttpStatus.BAD_REQUEST);

			return responseEntity;
		}

	}

	@GetMapping("/allData")
	public CompletableFuture<ResponseEntity<ListResponseDto>> getAllData(
			@RequestParam(value = "pageNumber", defaultValue = Constants.PAGE_NUMBER, required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = Constants.PAGE_SIZE, required = false) Integer pageSize) {
		CompletableFuture<Page<IAsyncDataListDto>> allData = this.asyncService.getAllData(pageNumber, pageSize);

		CompletableFuture<ListResponseDto> response = allData.thenApply(slice -> {
			List<IAsyncDataListDto> posts = slice.getContent();

			PeginationResponse paginationResponse = new PeginationResponse();
			paginationResponse.setPageNumber(slice.getNumber());
			paginationResponse.setPageSize(slice.getSize());
			paginationResponse.setTotal(slice.getTotalElements());

			return new ListResponseDto(posts, paginationResponse);
		});

		return response.thenApply(ResponseEntity::ok).exceptionally(handleGetDataFailure);
	}

	private static Function<Throwable, ResponseEntity<ListResponseDto>> handleGetDataFailure = throwable -> {
		LOGGER.error("Failed to read records: {}", throwable);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	};

	@GetMapping("/data")
	public ResponseEntity<?> getAll(
			@RequestParam(value = "pageNumber", defaultValue = Constants.PAGE_NUMBER, required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = Constants.PAGE_SIZE, required = false) Integer pageSize) {

		Page<IAsyncDataListDto> dataList = asyncService.getData(pageNumber, pageSize);
		List<IAsyncDataListDto> content = dataList.getContent();
		PeginationResponse paginationResponse = new PeginationResponse();
		paginationResponse.setPageNumber(dataList.getNumber());
		paginationResponse.setPageSize(dataList.getSize());
		paginationResponse.setTotal(dataList.getTotalElements());

		return new ResponseEntity<ListResponseDto>(new ListResponseDto(content, paginationResponse), HttpStatus.OK);
	}

}
