package com.todoapplication.controller;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todoapplication.dto.EnumDataDto;
import com.todoapplication.payloads.ErrorResponseDto;
import com.todoapplication.payloads.SuccessResponseDto;

@RestController
@RequestMapping("/api/enum")
public class EnumDataController {

	@PostMapping
	public ResponseEntity<?> addEnumData(@Valid @RequestBody EnumDataDto enumDataDto) {
		try {
			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto("Success", "Enum Added"),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(new ErrorResponseDto("Invalid data", "Invalid data"),
					HttpStatus.BAD_REQUEST);
		}
	}
}
