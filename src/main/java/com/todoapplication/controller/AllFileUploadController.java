package com.todoapplication.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.todoapplication.dto.AllFileDto;
import com.todoapplication.service.AllFileService;

@RestController
@RequestMapping("/api/allFiles")
public class AllFileUploadController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(AllFileUploadController.class);

	@PostMapping("/upload")
	public CompletableFuture<ResponseEntity<String>> uploadFiles(@RequestParam("files") MultipartFile[] files) {
		List<CompletableFuture<AllFileDto>> futureList = Arrays.stream(files)
				.map(file -> CompletableFuture.supplyAsync(() -> {
					try {
						return new AllFileDto(file.getOriginalFilename(), file.getContentType(), file.getBytes());
					} catch (IOException e) {
						throw new RuntimeException("Error uploading file");
					}
				})).collect(Collectors.toList());

		CompletableFuture<Void> allFutures = CompletableFuture.allOf(futureList.toArray(new CompletableFuture[0]));

		return allFutures.thenApplyAsync(v -> {
			List<AllFileDto> fileDTOs = futureList.stream().map(CompletableFuture::join).collect(Collectors.toList());

			fileUploadService.uploadFiles(fileDTOs);

			return ResponseEntity.ok("Files uploaded successfully");
		});
	}

	@Autowired
	private AllFileService fileUploadService;

	@Value("${images}")
	private String imagesPath;

	@Value("${videos}")
	private String videosPath;

	@Value("${excel}")
	private String excelPath;

	@Value("${pdf}")
	private String pdfPath;

	@Value("${word}")
	private String wordPath;

	@Value("${documents}")
	private String documentsPath;

	@GetMapping("/download")
	public ResponseEntity<Resource> downloadFiles() {
		List<Resource> files = fileUploadService.getFiles();
		String zipFileName = "files.zip";

		try {
			// Create a temporary file to hold the zip archive
			Path zipFilePath = Files.createTempFile(zipFileName, "");
			OutputStream outputStream = Files.newOutputStream(zipFilePath);

			// Create a zip output stream
			ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);

			// Iterate over the list of files and add them to the zip archive
			for (Resource file : files) {
				String fileName = file.getFilename();
				ZipEntry zipEntry = new ZipEntry(fileName);
				zipOutputStream.putNextEntry(zipEntry);
				InputStream inputStream = file.getInputStream();
				IOUtils.copy(inputStream, zipOutputStream);
				inputStream.close();
				zipOutputStream.closeEntry();
			}

			// Close the zip output stream
			zipOutputStream.finish();
			zipOutputStream.close();

			// Create a resource object for the zip archive and return it in the response
			// entity
			ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(zipFilePath));
			return ResponseEntity.ok().header("Content-Disposition", "attachment; filename=\"" + zipFileName + "\"")
					.body(resource);

		} catch (IOException e) {
			// Handle exception
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

}
