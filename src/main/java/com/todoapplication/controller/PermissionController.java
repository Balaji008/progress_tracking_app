package com.todoapplication.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todoapplication.dto.PermissionDto;
import com.todoapplication.ilistdto.IPermissionListDto;
import com.todoapplication.payloads.ErrorResponseDto;
import com.todoapplication.payloads.SuccessResponseDto;
import com.todoapplication.service.PermissionService;
import com.todoapplication.util.ErrorKeyConstants;
import com.todoapplication.util.ErrorMessageConstants;
import com.todoapplication.util.SuccessKeyConstants;
import com.todoapplication.util.SuccessMessageConstants;

@RestController
@RequestMapping("/permissions")
public class PermissionController {

	@Autowired
	private PermissionService permissionService;

	@PreAuthorize("hasRole('PermissionAdd')")
	@PostMapping
	public ResponseEntity<?> addPermission(@Valid @RequestBody PermissionDto permissionDto) {

		try {
			PermissionDto addedPermission = this.permissionService.addPermission(permissionDto);

			if (addedPermission != null) {
				return new ResponseEntity<SuccessResponseDto>(
						new SuccessResponseDto(SuccessKeyConstants.PERMISSION_S031301,
								SuccessMessageConstants.PERMISSION_ADDED, addedPermission),
						HttpStatus.CREATED);
			} else {
				return new ResponseEntity<ErrorResponseDto>(new ErrorResponseDto(ErrorKeyConstants.PERMISSION_E031302,
						ErrorMessageConstants.PERMISSION_ALREADY_EXISTS), HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.PERMISSION_E031302, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('PermissionUpdate')")
	@PutMapping("/{id}")
	public ResponseEntity<?> updatePermission(@Valid @RequestBody PermissionDto permissionDto, @PathVariable Long id) {

		try {
			PermissionDto updatedPermission = this.permissionService.updatePermission(permissionDto, id);
			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.PERMISSION_S031302,
					SuccessMessageConstants.PERMISSION_UPDATED, updatedPermission), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(new ErrorResponseDto(ErrorKeyConstants.PERMISSION_E031301,
					ErrorMessageConstants.PERMISSION_NOT_FOUND), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('PermissionDelete')")
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deletePermission(@PathVariable Long id) {
		try {
			this.permissionService.deletePermission(id);

			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.PERMISSION_S031303,
					SuccessMessageConstants.PERMISSION_DELETED), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.PERMISSION_E031301, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasRole('PermissionList')")
	@GetMapping
	public ResponseEntity<?> getAllPermissions() {
		try {
			List<IPermissionListDto> permissions = this.permissionService.getAllPermissions();

			return new ResponseEntity<SuccessResponseDto>(new SuccessResponseDto(SuccessKeyConstants.PERMISSION_S031304,
					SuccessMessageConstants.PERMISSION_FETCHED, permissions), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponseDto>(
					new ErrorResponseDto(ErrorKeyConstants.PERMISSION_E031301, e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}
}
