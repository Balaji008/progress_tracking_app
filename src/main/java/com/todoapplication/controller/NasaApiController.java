package com.todoapplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.todoapplication.serviceimpl.NasaApiServiceImpl;
import com.todoapplication.util.NasaResponse;

@RestController
@RequestMapping("/nasa")
public class NasaApiController {

	@Autowired
	private NasaApiServiceImpl nasaApiServiceImpl;

	@GetMapping("/apod")
	public NasaResponse getNasaData() {
		return nasaApiServiceImpl.getNasaData();
	}
}
