package com.todoapplication.interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class TodoInterceptorAppConfig implements WebMvcConfigurer {

	@Autowired
	private TodoInterceptor todoInterceptor;

	@Autowired
	private AuthenticationLogger authenticationLogger;

	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(todoInterceptor);
		registry.addInterceptor(authenticationLogger);
	}

}
