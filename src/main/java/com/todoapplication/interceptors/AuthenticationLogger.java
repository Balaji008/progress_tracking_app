package com.todoapplication.interceptors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.todoapplication.configurations.JwtTokenUtil;
import com.todoapplication.entities.UserEntity;
import com.todoapplication.service.AuthService;
import com.todoapplication.util.ApiUrls;

@Component
public class AuthenticationLogger implements HandlerInterceptor {

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private AuthService authService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		String authHeader = request.getHeader("Authorization");
		String token = (authHeader != null) ? authHeader.split("\\s")[1] : null;

		List<String> urlsWithoutHeader = new ArrayList<String>(Arrays.asList(ApiUrls.API_URLS_WITHOUT_HEADER));

		final String requestUrl = request.getRequestURI();
		if (!urlsWithoutHeader.contains(requestUrl) && token != null) {
			final String email = this.jwtTokenUtil.getUsernameFromToken(token);
			UserEntity user = this.authService.userInformation(email);

			if (user != null) {
				List<String> permissions = this.authService.permissions(user.getId());
				request.setAttribute("User-permissions", permissions);
				request.setAttribute("User-id", user.getId());
			}
		}
		return HandlerInterceptor.super.preHandle(request, response, handler);

	}
}
