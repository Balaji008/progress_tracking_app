package com.todoapplication.service;

public interface ZoomApiService {

	public String createMeeting(String topic, String startTime, int duration, String token) throws Exception;
}
