package com.todoapplication.service;

import java.util.List;

import com.todoapplication.dto.UserRoleDto;
import com.todoapplication.ilistdto.IUserRoleListDto;

public interface UserRoleService {

	List<IUserRoleListDto> assignRoleToUser(UserRoleDto userRoleDto);

	List<IUserRoleListDto> updateUserRole(UserRoleDto userRoleDto, Long id);

	void deleteUserRole(Long id);

	List<IUserRoleListDto> getAllUserRoles();
}
