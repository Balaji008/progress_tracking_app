package com.todoapplication.service;

import java.util.List;

import com.todoapplication.dto.RoleDto;
import com.todoapplication.ilistdto.IRoleListDto;

public interface RoleService {

	RoleDto createRole(RoleDto roleDto);

	RoleDto updateRole(RoleDto roleDto, Long roleId);

	IRoleListDto getRoleById(Long roleId);

	List<IRoleListDto> getAllRoles();

	void deleteRole(Long roleId);
}
