package com.todoapplication.service;

import java.util.List;

import com.todoapplication.dto.UserTaskDto;
import com.todoapplication.ilistdto.IUserTaskListDto;

public interface UserTaskService {

	UserTaskDto assignTaskToUser(UserTaskDto userTaskDto);

	UserTaskDto updateTaskOfUser(UserTaskDto userTaskDto, Long id);

	void deleteTaskOfUser(Long id);

	List<IUserTaskListDto> getTasksOfAllUsers(List<String> permissions, Long userId);

	List<IUserTaskListDto> getTaskOfUserById(Long userId);

	// List<IUserTaskListDto> getOwnTasks();

	List<IUserTaskListDto> updateTaskStatus(Long userId, Long taskId);
}
