package com.todoapplication.service;

import java.io.IOException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.todoapplication.entities.UserEntity;

public interface SAMLService {
	String generateRedirectUrl();

	String ssoAuthentication(String samlResponse)
			throws ParserConfigurationException, SAXException, IOException, Exception;

	Map<Object, Object> generateAccessToken(String email) throws Exception;

	UserEntity getUserByEmail(String email) throws Exception;

	String validateUUID(String uuid) throws Exception;

	Map<Object, Object> generateAccessToken(Long userId) throws Exception;

	String generateRedirectUrlUsingXMLDocument() throws ParserConfigurationException, TransformerException;
}
