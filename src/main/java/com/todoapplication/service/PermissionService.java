package com.todoapplication.service;

import java.util.List;
import com.todoapplication.dto.PermissionDto;
import com.todoapplication.ilistdto.IPermissionListDto;

public interface PermissionService {

	PermissionDto addPermission(PermissionDto permissionDto);

	PermissionDto updatePermission(PermissionDto permissionDto, Long id);

	void deletePermission(Long id);

	List<IPermissionListDto> getAllPermissions();
}
