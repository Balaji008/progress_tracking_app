package com.todoapplication.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.todoapplication.dto.UserDto;
import com.todoapplication.entities.UserEntity;
import com.todoapplication.payloads.JwtAuthenticationRequest;
import com.todoapplication.payloads.JwtAuthenticationResponse;

public interface AuthService extends UserDetailsService {

	JwtAuthenticationResponse login(JwtAuthenticationRequest request);

	UserDto registerUser(UserDto userDto);

	String refreshAndGetAuthenticationToken(String refreshToken) throws Exception;

	// List<String> permissions(UserEntity user);

	List<String> permissions(Long userId);

	UserEntity userInformation(String email);

	JwtAuthenticationResponse loginWithOtp(String email);

}
