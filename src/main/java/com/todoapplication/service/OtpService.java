package com.todoapplication.service;

import com.todoapplication.dto.OtpDto;

public interface OtpService {

	String sendOtp(OtpDto otpDto);

	boolean validateOtp(String otp, String email);

//	Object validateOtp(String otp, String email);
}
