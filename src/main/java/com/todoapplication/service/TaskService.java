package com.todoapplication.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.todoapplication.dto.TaskDto;
import com.todoapplication.ilistdto.ITaskListDto;

public interface TaskService {

	TaskDto createTask(TaskDto taskDto);

	TaskDto updateTask(TaskDto taskDto, Long id);

	void deleteTask(long id);

	List<ITaskListDto> getAllTasks();

	public List<TaskDto> save(MultipartFile file);

}
