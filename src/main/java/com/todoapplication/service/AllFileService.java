package com.todoapplication.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.core.io.Resource;

import com.todoapplication.dto.AllFileDto;

public interface AllFileService {

	CompletableFuture<Void> uploadFiles(List<AllFileDto> fileDTOs);

//	CompletableFuture<List<AllFilesEntity>> getAllFiles();
//
//	List<Resource> getFiles(List<String> fileNames, String fileType);
//
//	List<byte[]> getFile(List<String> path, List<String> filename);

	public List<Resource> getFiles();
}
