package com.todoapplication.service;

public interface AsyncService {

	String runInBackground();
}
