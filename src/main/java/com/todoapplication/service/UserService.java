package com.todoapplication.service;

import java.util.List;

import com.todoapplication.ilistdto.IListUserDto;

public interface UserService {

	void deleteUser(Long userId);

	List<IListUserDto> getAllUsers();

	List<IListUserDto> getUserById(Long userId);

}
