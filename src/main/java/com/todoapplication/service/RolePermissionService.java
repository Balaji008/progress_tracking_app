package com.todoapplication.service;

import java.util.List;

import com.todoapplication.dto.RolePermissionDto;
import com.todoapplication.ilistdto.IRolePermissionListDto;

public interface RolePermissionService {

	IRolePermissionListDto assignPermissionsToRole(RolePermissionDto rolePermissionDto);

	IRolePermissionListDto updatePermissionsOfRole(RolePermissionDto rolePermissionDto, Long id);

	void deletePermissionOfRole(Long id);

	List<IRolePermissionListDto> getAllPermissionsOfRole();
	
	IRolePermissionListDto getAllPermissionsByRoleId(Long roleId);
}
