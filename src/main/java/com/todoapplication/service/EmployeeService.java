package com.todoapplication.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.todoapplication.dto.EmployeeDto;

public interface EmployeeService {

	List<EmployeeDto> uploadCsvFile(MultipartFile file);
}
