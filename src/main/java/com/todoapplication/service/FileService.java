package com.todoapplication.service;

import java.io.InputStream;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.todoapplication.dto.FileDto;

public interface FileService {

	FileDto addFile(FileDto fileDto, String path, MultipartFile file);

	FileDto getFileDto(Long id);

	public InputStream getFile(Long id, String path);

	String addFile(String path, MultipartFile file) throws Exception;

	List<InputStream> getFiles(int pageNumber, int pageSize, String path);

	List<byte[]> getFileBytesList(Integer pageNumber, Integer pageSize, String path);

//	List<byte[]> getFileBytesList(int pageNumber, int pageSize, String path);

}
