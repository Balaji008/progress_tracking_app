package com.todoapplication.service;

import com.todoapplication.entities.InviteEntity;

public interface InviteService {

	InviteEntity add(String uuid, String emailId);

	void deleteByEmail(String email) throws Exception;
}
