package com.todoapplication.ilistdto;

import java.io.Serializable;

public interface IUserTaskListDto extends Serializable {

	String getUserName();

	Long getTaskId();

	String getTaskName();

	String getTaskStatus();

}
