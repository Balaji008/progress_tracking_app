package com.todoapplication.ilistdto;

public interface ITaskListDto {

	Long getId();

	String getTaskName();
}
