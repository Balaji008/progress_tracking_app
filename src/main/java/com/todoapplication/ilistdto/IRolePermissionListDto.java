package com.todoapplication.ilistdto;

import java.util.List;

public interface IRolePermissionListDto {

	String getRoleName();

	List<String> getPermissionName();
}
