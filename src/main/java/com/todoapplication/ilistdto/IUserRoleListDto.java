package com.todoapplication.ilistdto;

import java.util.List;

public interface IUserRoleListDto {

	List<String> getUserName();

	List<String> getRoleName();
}
