package com.todoapplication.ilistdto;

public interface IListUserDto {

	Long getId();

	String getName();

	String getEmail();

}
