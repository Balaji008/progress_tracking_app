package com.todoapplication.ilistdto;

public interface IAsyncDataListDto {

	public Long getId();

	public Long getAge();

	public String getAttrition();

	public String getBusinessTravel();

	public Long getDailyRate();

	public String getDepartment();

	public Long getDistanceFromHome();

	public Long getEducation();

	public String getEducationField();

	public Long getEmployeeCount();

	public Long getEmployeeNumber();

	public Long getEnvironmentSatisfaction();

	public String getGender();

	public Long getHourlyRate();

	public Long getJobInvolver();

	public Long getJobLevel();

	public String getJobRole();

	public Long getJobSatisfaction();

	public String getMaritalStatus();

	public Long getMonthlyIncome();

	public Long getMonyhlyRate();

	public Long getNumCompaniesWorked();

	public String getOver18();

	public String getOverTime();

	public Long getPercentSalaryHike();

	public Long getPerformanceRating();

	public Long getRelationshipSatisfaction();

	public Long getStandardHours();

	public Long getStockOptionLevel();

	public Long getTotalWorkingYears();

	public Long getTrainingTimesLastYears();

	public Long getWorkLifeBalance();

	public Long getYearsAtCompany();

	public Long getYearsInCurrentRole();

	public Long getYearsSinceLastPromotion();

	public Long getYearsWithCurrentManager();
}
