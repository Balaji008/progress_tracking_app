package com.todoapplication.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "async_data")
public class AsyncDataEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "age")
	private Long age;

	@Column(name = "attrition")
	private String attrition;

	@Column(name = "business_travel")
	private String businessTravel;

	@Column(name = "daily_rate")
	private Long dailyRate;

	@Column(name = "department")
	private String department;

	@Column(name = "distance_from_home")
	private Long distanceFromHome;

	@Column(name = "education")
	private Long education;

	@Column(name = "education_field")
	private String educationField;

	@Column(name = "employee_count")
	private Long employeeCount;

	@Column(name = "employee_number")
	private Long employeeNumber;

	@Column(name = "environment_satisfaction")
	private Long environmentSatisfaction;

	@Column(name = "gender")
	private String gender;

	@Column(name = "hourly_rate")
	private Long hourlyRate;

	@Column(name = "job_involver")
	private Long jobInvolver;

	@Column(name = "job_level")
	private Long jobLevel;

	@Column(name = "job_role")
	private String jobRole;

	@Column(name = "job_satisfaction")
	private Long jobSatisfaction;

	@Column(name = "marital_status")
	private String maritalStatus;

	@Column(name = "monthly_income")
	private Long monthlyIncome;

	@Column(name = "monyhly_rate")
	private Long monyhlyRate;

	@Column(name = "num_companies_worked")
	private Long numCompaniesWorked;

	@Column(name = "over18")
	private String over18;

	@Column(name = "over_time")
	private String overTime;

	@Column(name = "percent_salary_hike")
	private Long percentSalaryHike;

	@Column(name = "performance_rating")
	private Long performanceRating;

	@Column(name = "relationship_satisfaction")
	private Long relationshipSatisfaction;

	@Column(name = "standard_hours")
	private Long standardHours;

	@Column(name = "stock_option_level")
	private Long stockOptionLevel;

	@Column(name = "total_working_years")
	private Long totalWorkingYears;

	@Column(name = "training_times_last_year")
	private Long trainingTimesLastYears;

	@Column(name = "work_life_balance")
	private Long workLifeBalance;

	@Column(name = "years_at_company")
	private Long yearsAtCompany;

	@Column(name = "years_in_current_role")
	private Long yearsInCurrentRole;

	@Column(name = "years_since_last_promotion")
	private Long yearsSinceLastPromotion;

	@Column(name = "years_with_current_manager")
	private Long yearsWithCurrentManager;

	public AsyncDataEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AsyncDataEntity(Long id, Long age, String attrition, String businessTravel, Long dailyRate,
			String department, Long distanceFromHome, Long education, String educationField, Long employeeCount,
			Long employeeNumber, Long environmentSatisfaction, String gender, Long hourlyRate, Long jobInvolver,
			Long jobLevel, String jobRole, Long jobSatisfaction, String maritalStatus, Long monthlyIncome,
			Long monyhlyRate, Long numCompaniesWorked, String over18, String overTime, Long percentSalaryHike,
			Long performanceRating, Long relationshipSatisfaction, Long standardHours, Long stockOptionLevel,
			Long totalWorkingYears, Long trainingTimesLastYears, Long workLifeBalance, Long yearsAtCompany,
			Long yearsInCurrentRole, Long yearsSinceLastPromotion, Long yearsWithCurrentManager) {
		super();
		this.id = id;
		this.age = age;
		this.attrition = attrition;
		this.businessTravel = businessTravel;
		this.dailyRate = dailyRate;
		this.department = department;
		this.distanceFromHome = distanceFromHome;
		this.education = education;
		this.educationField = educationField;
		this.employeeCount = employeeCount;
		this.employeeNumber = employeeNumber;
		this.environmentSatisfaction = environmentSatisfaction;
		this.gender = gender;
		this.hourlyRate = hourlyRate;
		this.jobInvolver = jobInvolver;
		this.jobLevel = jobLevel;
		this.jobRole = jobRole;
		this.jobSatisfaction = jobSatisfaction;
		this.maritalStatus = maritalStatus;
		this.monthlyIncome = monthlyIncome;
		this.monyhlyRate = monyhlyRate;
		this.numCompaniesWorked = numCompaniesWorked;
		this.over18 = over18;
		this.overTime = overTime;
		this.percentSalaryHike = percentSalaryHike;
		this.performanceRating = performanceRating;
		this.relationshipSatisfaction = relationshipSatisfaction;
		this.standardHours = standardHours;
		this.stockOptionLevel = stockOptionLevel;
		this.totalWorkingYears = totalWorkingYears;
		this.trainingTimesLastYears = trainingTimesLastYears;
		this.workLifeBalance = workLifeBalance;
		this.yearsAtCompany = yearsAtCompany;
		this.yearsInCurrentRole = yearsInCurrentRole;
		this.yearsSinceLastPromotion = yearsSinceLastPromotion;
		this.yearsWithCurrentManager = yearsWithCurrentManager;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAge() {
		return age;
	}

	public void setAge(Long age) {
		this.age = age;
	}

	public String getAttrition() {
		return attrition;
	}

	public void setAttrition(String attrition) {
		this.attrition = attrition;
	}

	public String getBusinessTravel() {
		return businessTravel;
	}

	public void setBusinessTravel(String businessTravel) {
		this.businessTravel = businessTravel;
	}

	public Long getDailyRate() {
		return dailyRate;
	}

	public void setDailyRate(Long dailyRate) {
		this.dailyRate = dailyRate;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Long getDistanceFromHome() {
		return distanceFromHome;
	}

	public void setDistanceFromHome(Long distanceFromHome) {
		this.distanceFromHome = distanceFromHome;
	}

	public Long getEducation() {
		return education;
	}

	public void setEducation(Long education) {
		this.education = education;
	}

	public String getEducationField() {
		return educationField;
	}

	public void setEducationField(String educationField) {
		this.educationField = educationField;
	}

	public Long getEmployeeCount() {
		return employeeCount;
	}

	public void setEmployeeCount(Long employeeCount) {
		this.employeeCount = employeeCount;
	}

	public Long getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(Long employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public Long getEnvironmentSatisfaction() {
		return environmentSatisfaction;
	}

	public void setEnvironmentSatisfaction(Long environmentSatisfaction) {
		this.environmentSatisfaction = environmentSatisfaction;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Long getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(Long hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public Long getJobInvolver() {
		return jobInvolver;
	}

	public void setJobInvolver(Long jobInvolver) {
		this.jobInvolver = jobInvolver;
	}

	public Long getJobLevel() {
		return jobLevel;
	}

	public void setJobLevel(Long jobLevel) {
		this.jobLevel = jobLevel;
	}

	public String getJobRole() {
		return jobRole;
	}

	public void setJobRole(String jobRole) {
		this.jobRole = jobRole;
	}

	public Long getJobSatisfaction() {
		return jobSatisfaction;
	}

	public void setJobSatisfaction(Long jobSatisfaction) {
		this.jobSatisfaction = jobSatisfaction;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public Long getMonthlyIncome() {
		return monthlyIncome;
	}

	public void setMonthlyIncome(Long monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}

	public Long getMonyhlyRate() {
		return monyhlyRate;
	}

	public void setMonyhlyRate(Long monyhlyRate) {
		this.monyhlyRate = monyhlyRate;
	}

	public Long getNumCompaniesWorked() {
		return numCompaniesWorked;
	}

	public void setNumCompaniesWorked(Long numCompaniesWorked) {
		this.numCompaniesWorked = numCompaniesWorked;
	}

	public String getOver18() {
		return over18;
	}

	public void setOver18(String over18) {
		this.over18 = over18;
	}

	public String getOverTime() {
		return overTime;
	}

	public void setOverTime(String overTime) {
		this.overTime = overTime;
	}

	public Long getPercentSalaryHike() {
		return percentSalaryHike;
	}

	public void setPercentSalaryHike(Long percentSalaryHike) {
		this.percentSalaryHike = percentSalaryHike;
	}

	public Long getPerformanceRating() {
		return performanceRating;
	}

	public void setPerformanceRating(Long performanceRating) {
		this.performanceRating = performanceRating;
	}

	public Long getRelationshipSatisfaction() {
		return relationshipSatisfaction;
	}

	public void setRelationshipSatisfaction(Long relationshipSatisfaction) {
		this.relationshipSatisfaction = relationshipSatisfaction;
	}

	public Long getStandardHours() {
		return standardHours;
	}

	public void setStandardHours(Long standardHours) {
		this.standardHours = standardHours;
	}

	public Long getStockOptionLevel() {
		return stockOptionLevel;
	}

	public void setStockOptionLevel(Long stockOptionLevel) {
		this.stockOptionLevel = stockOptionLevel;
	}

	public Long getTotalWorkingYears() {
		return totalWorkingYears;
	}

	public void setTotalWorkingYears(Long totalWorkingYears) {
		this.totalWorkingYears = totalWorkingYears;
	}

	public Long getTrainingTimesLastYears() {
		return trainingTimesLastYears;
	}

	public void setTrainingTimesLastYears(Long trainingTimesLastYears) {
		this.trainingTimesLastYears = trainingTimesLastYears;
	}

	public Long getWorkLifeBalance() {
		return workLifeBalance;
	}

	public void setWorkLifeBalance(Long workLifeBalance) {
		this.workLifeBalance = workLifeBalance;
	}

	public Long getYearsAtCompany() {
		return yearsAtCompany;
	}

	public void setYearsAtCompany(Long yearsAtCompany) {
		this.yearsAtCompany = yearsAtCompany;
	}

	public Long getYearsInCurrentRole() {
		return yearsInCurrentRole;
	}

	public void setYearsInCurrentRole(Long yearsInCurrentRole) {
		this.yearsInCurrentRole = yearsInCurrentRole;
	}

	public Long getYearsSinceLastPromotion() {
		return yearsSinceLastPromotion;
	}

	public void setYearsSinceLastPromotion(Long yearsSinceLastPromotion) {
		this.yearsSinceLastPromotion = yearsSinceLastPromotion;
	}

	public Long getYearsWithCurrentManager() {
		return yearsWithCurrentManager;
	}

	public void setYearsWithCurrentManager(Long yearsWithCurrentManager) {
		this.yearsWithCurrentManager = yearsWithCurrentManager;
	}
}
