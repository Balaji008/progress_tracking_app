package com.todoapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableScheduling
@EnableWebMvc
public class TodoApplication
//implements CommandLineRunner
{

	public static void main(String[] args) {
		SpringApplication.run(TodoApplication.class, args);
	}

//	@Override
//	public void run(String... args) throws Exception {
//		// TODO Auto-generated method stub
//		BackgroundProcess backgroundProcess = new BackgroundProcess();
//		backgroundProcess.start();
//	}

}
