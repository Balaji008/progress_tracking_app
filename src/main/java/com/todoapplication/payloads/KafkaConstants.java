package com.todoapplication.payloads;

public class KafkaConstants {

	public static final String TOPIC = "Nasa-Iss-Data";
	public static final String GROUP_ID = "group_Nasa-ISS-Data";
	public static final String HOST = "localhost:9092";
}
