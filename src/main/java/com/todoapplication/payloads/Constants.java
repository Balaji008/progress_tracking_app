package com.todoapplication.payloads;

public class Constants {

	public static final String EMAIL_REGEX = "[a-z0-9A-Z][a-z0-9A-Z_.]*@[a-z0-9A-Z]+([.][a-zA-Z]+)+";

	public static final String PASSWORD_REGEX = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=.*[@#$%^&+=])"
			+ "(?=\\S+$).{8,20}$";

	public static final String PAGE_SIZE = "10";

	public static final String PAGE_NUMBER = "0";

	public static final String SORT_BY = "";

	public static final String SAML_PROTOCOL_NAMESPACE = "urn:oasis:names:tc:SAML:2.0:protocol";
	public static final String SAML_ASSERTION_NAMESPACE = "urn:oasis:names:tc:SAML:2.0:assertion";
	public static final String SAML_BINDING_HTTP_POST = "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST";
	public static final String SAML_NAMEID_FORMAT = "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified";

	public static final String SIGNATURE_VALUE = "SignatureValue";
	public static final String ATTRIBUTE = "Attribute";
	public static final String X509CERTIFICATE = "X509Certificate";
	public static final String DSX509CERTIFICATE = "ds:X509Certificate";

	public static final String STATUS_TAG = "samlp:Status";
	public static final String STATUS_CODE_TAG = "samlp:StatusCode";
	public static final String VALUE = "Value";
	public static final String SUCCESS_STATUS = "urn:oasis:names:tc:SAML:2.0:status:Success";
	public static final String NAME = "Name";
	public static final String EMAIL_TAG = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress";
	public static final String ATTRIBUTE_VALUE = "AttributeValue";
	public static final String USER_NAME = "http://schemas.microsoft.com/identity/claims/displayname";

}
