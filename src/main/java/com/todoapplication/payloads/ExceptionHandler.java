package com.todoapplication.payloads;

import org.springframework.http.ResponseEntity;

@FunctionalInterface
public interface ExceptionHandler<T> {
    ResponseEntity<T> handleException(Throwable throwable);
}