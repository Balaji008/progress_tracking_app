package com.todoapplication.payloads;

public class SuccessResponseDto {

	private String key;

	private String message;

	private Object data;

	public SuccessResponseDto(String key, String message) {
		super();
		this.key = key;
		this.message = message;
	}

	public SuccessResponseDto(String key, String message, Object data) {
		super();
		this.key = key;
		this.message = message;
		this.data = data;
	}

	public SuccessResponseDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "SuccessResponseDto [key=" + key + ", message=" + message + ", data=" + data + "]";
	}

}
