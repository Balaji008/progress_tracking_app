package com.todoapplication.payloads;

public class ListResponseDto {

	private Object data;

	private Object pagination;

	public ListResponseDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ListResponseDto(Object data, Object pagination) {
		super();
		this.data = data;
		this.pagination = pagination;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Object getPagination() {
		return pagination;
	}

	public void setPagination(Object pagination) {
		this.pagination = pagination;
	}

}
