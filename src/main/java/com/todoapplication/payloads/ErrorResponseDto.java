package com.todoapplication.payloads;

public class ErrorResponseDto {

	private String key;

	private String message;

	public ErrorResponseDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ErrorResponseDto(String message) {
		super();
		this.message = message;
	}

	public ErrorResponseDto(String key, String message) {
		super();
		this.key = key;
		this.message = message;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ErrorResponseDto [key=" + key + ", message=" + message + "]";
	}

}
