package com.todoapplication.payloads;

import org.springframework.http.ResponseEntity;

@FunctionalInterface
public interface ResponseExceptionHandler {
	ResponseEntity<?> handle(Throwable ex);
}
