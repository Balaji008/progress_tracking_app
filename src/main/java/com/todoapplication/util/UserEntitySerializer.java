package com.todoapplication.util;

import java.io.IOException;

import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.todoapplication.entities.UserEntity;

public class UserEntitySerializer implements RedisSerializer<UserEntity> {

	private final ObjectMapper mapper = new ObjectMapper();

	@Override
	public byte[] serialize(UserEntity user) throws SerializationException {
		try {
			return mapper.writeValueAsBytes(user);
		} catch (JsonProcessingException e) {
			throw new SerializationException("Error serializing Person", e);
		}
	}

	@Override
	public UserEntity deserialize(byte[] bytes) throws SerializationException {
		try {
			return mapper.readValue(bytes, UserEntity.class);
		} catch (IOException e) {
			throw new SerializationException("Error deserializing Person", e);
		}
	}

}
