package com.todoapplication.util;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Quote {
	private int id;
	private boolean dialogue;
	@JsonProperty("private")
	private boolean privateQuote;
	private List<String> tags;
	private String url;
	@JsonProperty("favorites_count")
	private int favoritesCount;
	@JsonProperty("upvotes_count")
	private int upvotesCount;
	@JsonProperty("downvotes_count")
	private int downvotesCount;
	private String author;
	@JsonProperty("author_permalink")
	private String authorPermalink;
	private String body;

	public Quote() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Quote(int id, boolean dialogue, boolean privateQuote, List<String> tags, String url, int favoritesCount,
			int upvotesCount, int downvotesCount, String author, String authorPermalink, String body) {
		super();
		this.id = id;
		this.dialogue = dialogue;
		this.privateQuote = privateQuote;
		this.tags = tags;
		this.url = url;
		this.favoritesCount = favoritesCount;
		this.upvotesCount = upvotesCount;
		this.downvotesCount = downvotesCount;
		this.author = author;
		this.authorPermalink = authorPermalink;
		this.body = body;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isDialogue() {
		return dialogue;
	}

	public void setDialogue(boolean dialogue) {
		this.dialogue = dialogue;
	}

	public boolean isPrivateQuote() {
		return privateQuote;
	}

	public void setPrivateQuote(boolean privateQuote) {
		this.privateQuote = privateQuote;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getFavoritesCount() {
		return favoritesCount;
	}

	public void setFavoritesCount(int favoritesCount) {
		this.favoritesCount = favoritesCount;
	}

	public int getUpvotesCount() {
		return upvotesCount;
	}

	public void setUpvotesCount(int upvotesCount) {
		this.upvotesCount = upvotesCount;
	}

	public int getDownvotesCount() {
		return downvotesCount;
	}

	public void setDownvotesCount(int downvotesCount) {
		this.downvotesCount = downvotesCount;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthorPermalink() {
		return authorPermalink;
	}

	public void setAuthorPermalink(String authorPermalink) {
		this.authorPermalink = authorPermalink;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
