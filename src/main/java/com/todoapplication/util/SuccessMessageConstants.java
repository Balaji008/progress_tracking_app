package com.todoapplication.util;

public class SuccessMessageConstants {

	public static final String USER_REGISTERED = "Registration successful";
	public static final String USER_UPDATED = "User updated successfully";
	public static final String USER_DELETED = "User deleted successfully";
	public static final String USER_FETCHED = "User fetched successfully";
	public static final String PASSWORD_UPDATED = "Password updated successfully";
	public static final String EMAIL_UPDATED = "Email updated successfully";
	public static final String LOGIN_SUCCESSFUL = "Login successful";

	public static final String ROLE_ADDED = "Role added successfully";
	public static final String ROLE_UPDATED = "Role updated successfully";
	public static final String ROLE_DELETED = "Role deleted successfully";
	public static final String ROLE_FETCHED = "Role fetched successfully";

	public static final String PERMISSION_ADDED = "Permission added successfully";
	public static final String PERMISSION_UPDATED = "Permission updated successfully";
	public static final String PERMISSION_DELETED = "Permission deleted successfully";
	public static final String PERMISSION_FETCHED = "Permission fetched successfully";

	public static final String TASK_ADDED = "Task added successfully";
	public static final String TASK_UPDATED = "Task updated successfully";
	public static final String TASK_DELETED = "Task deleted successfully";
	public static final String TASK_FETCHED = "Task fetched successfully";

	public static final String USER_ROLE_ADDED = "Role assigned to user successfully";
	public static final String USER_ROLE_UPDATED = "Role of user updated successfully";
	public static final String USER_ROLE_DELETED = "Role of user deleted successfully";
	public static final String USER_ROLE_FETCHED = "Roles of user fetched successfully";

	public static final String ROLE_PERMISSION_ADDED = "Permission assigned to role successfully";
	public static final String ROLE_PERMISSION_UPDATED = "Permission of role updated successfully";
	public static final String ROLE_PERMISSION_DELETED = "Permission of role deleted successfully";
	public static final String ROLE_PERMISSION_FETCHED = "Permissions of role fetched successfully";

	public static final String USER_TASK_ADDED = "Task assigned to user successfully";
	public static final String USER_TASK_UPDATED = "Task of user updated successfully";
	public static final String USER_TASK_DELETED = "Task of user deleted successfully";
	public static final String USER_TASK_FETCHED = "Tasks of user fetched successfully";
	public static final String USER_TASK_STATUS_UPDATED = "Task status updated successfully";

	public static final String TOKEN_GENERATED = "Token generated successfully";

	public static final String FILE_UPLOADED = "File uploaded successfully";

	public static final String OTP_SENT = "Otp sent successfully";
	public static final String OTP_VALIDATED = "Otp validated successfully";

}
