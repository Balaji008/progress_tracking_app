package com.todoapplication.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

@Service
public class GplApiIntegrationServiceImpl {

	public String gplApi() {
		String urlStr = "https://api.godrejproperties.com:8085/api-container/getEmpData";
		String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJuaW1hcEBnb2RyZWpwcm9wZXJ0aWVzLmNvbSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIiwiaWF0IjoxNjc4Nzk5NDE1LCJleHAiOjE2Nzg4ODU4MTV9.qpfjk5z1Kdunhuvya-r9xDrvo6EtXqy_NzMCp8v2GQk";
		WebClient webClient = WebClient.create(urlStr);

		Mono<String> response = webClient.post().uri(uriBuilder -> uriBuilder.path("/api-container/getEmpData").build())
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
				.body(BodyInserters.fromValue("{ \"email\": \"rohit.soni@godrejproperties.com\" }")).retrieve()
				.bodyToMono(String.class);

		return response.block(); // block to wait for response and return the body
	}

//	public String gplApi() {
//		String urlStr = "https://api.godrejproperties.com:8085/api-container/getEmpData";
//		String email = "rohit.soni@godrejproperties.com";
//		String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJuaW1hcEBnb2RyZWpwcm9wZXJ0aWVzLmNvbSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIiwiaWF0IjoxNjc4Nzk5NDE1LCJleHAiOjE2Nzg4ODU4MTV9.qpfjk5z1Kdunhuvya-r9xDrvo6EtXqy_NzMCp8v2GQk";
//
//		try {
//			// Create the URL object
//			URL url = new URL(urlStr);
//
//			// Open a connection to the URL
//			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//
//			// Set the request method and headers
//			connection.setRequestMethod("GET");
//			connection.setRequestProperty("Authorization", "Bearer " + token);
//			connection.setRequestProperty("Content-Type", "application/json");
//
//			// Create a JSON object with the email field
//			JSONObject emailJson = new JSONObject();
//			emailJson.put("email", email);
//
//			// Convert the JSON object to a string
//			String data = emailJson.toString();
//
//			// Set the request body
//			connection.setDoOutput(true);
//			connection.getOutputStream().write(data.getBytes());
//
//			// Read the response from the server
//			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//			StringBuilder response = new StringBuilder();
//			String line;
//			while ((line = reader.readLine()) != null) {
//				response.append(line);
//			}
//			reader.close();
//
//			// Return the response
//			return response.toString();
//		} catch (Exception e) {
//			System.err.println("Api error");
//			e.printStackTrace();
//			return null;
//		}
//	}

//	public String gplApi() {
//		String url = "https://api.godrejproperties.com:8085/api-container/getEmpData";
//		String email = "{\"email\":\"rohit.soni@godrejproperties.com\"}";
//		String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJuaW1hcEBnb2RyZWpwcm9wZXJ0aWVzLmNvbSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIiwiaWF0IjoxNjc4Nzk5NDE1LCJleHAiOjE2Nzg4ODU4MTV9.qpfjk5z1Kdunhuvya-r9xDrvo6EtXqy_NzMCp8v2GQk";
//		try {
//			URI uri = new URI(url);
//
//			// Create the request headers with the bearer token
//			Header[] headers = { new BasicHeader("Authorization", "Bearer " + token),
//					new BasicHeader("Content-Type", "application/json") };
//
//			URIBuilder uriBuilder = new URIBuilder(uri);
//			uriBuilder.setParameters(email);
//			URI requestUri = uriBuilder.build();
//
//			// Create the GET request with headers and URI
//			HttpGet request = new HttpGet(requestUri);
//			request.setHeaders(headers);
//
//			// Create an instance of HttpClient
//			HttpClient httpClient = HttpClients.createDefault();
//
//			// Execute the GET request and get the response entity
//			org.apache.http.HttpResponse response = httpClient.execute(request);
//			String responseBody = EntityUtils.toString(response.getEntity());
//			System.out.println(responseBody);
//			return responseBody;
//		} catch (Exception e) {
//			return null;
//		}
//	}
//
//	private List<NameValuePair> getParametersFromJson(String json) {
//		List<NameValuePair> params = new ArrayList<>();
//		// TODO: Parse the JSON string and add each field as a parameter
//		return params;
//	}
//	public String gplApi() {
//		String url = "https://api.godrejproperties.com:8085/api-container/getEmpData";
//		String email = "rohit.soni@godrejproperties.com";
//		String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJuaW1hcEBnb2RyZWpwcm9wZXJ0aWVzLmNvbSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIiwiaWF0IjoxNjc4Nzk5NDE1LCJleHAiOjE2Nzg4ODU4MTV9.qpfjk5z1Kdunhuvya-r9xDrvo6EtXqy_NzMCp8v2GQk";
//
//		RestTemplate restTemplate = new RestTemplate();
//
//		HttpHeaders headers = new HttpHeaders();
//		headers.set("Authorization", "Bearer " + token);
//		headers.setContentType(MediaType.APPLICATION_JSON);
//
//		// Create a JSON object with the email field
//		JSONObject emailJson = new JSONObject();
//		emailJson.put("email", email);
//		System.err.println(emailJson.toString());
//		try {
//			HttpEntity<?> requestEntity = new HttpEntity<>(emailJson.toString(), headers);
//
//			ResponseEntity<JsonObject> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
//					JsonObject.class);
//			JsonObject body = responseEntity.getBody();
//			String responseBody = body.toString();
//
//			return responseBody;
//		} catch (Exception e) {
//			System.err.println("Api error");
//			e.printStackTrace();
//			return null;
//		}
//	}

//	String url = "https://api.godrejproperties.com:8085/api-container/getEmpData";
//	String email = "rohit.soni@godrejproperties.com";
//	String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJuaW1hcEBnb2RyZWpwcm9wZXJ0aWVzLmNvbSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIiwiaWF0IjoxNjc4Nzk5NDE1LCJleHAiOjE2Nzg4ODU4MTV9.qpfjk5z1Kdunhuvya-r9xDrvo6EtXqy_NzMCp8v2GQk";
//
//	OkHttpClient client = new OkHttpClient();
// 

//	MediaType mediaType = MediaType.parse("application/json");
//	RequestBody requestBody = RequestBody.create(mediaType, "{\"email\":\"" + email + "\"}");
//
//	Request request = new Request.Builder().url(url).header("Authorization", "Bearer " + token)
//			.header("Content-Type", "application/json").post(requestBody).build();
//
//	try (Response response = client.newCall(request).execute()) {
//		String responseBody = response.body().string();
//		System.out.println(responseBody);
//		return responseBody;
//	} catch (IOException e) {
//		e.printStackTrace();
//		return null;
//	}

//	if (responseCode == HttpURLConnection.HTTP_OK) { // success
//	BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
//	String inputLine;
//
//	while ((inputLine = in.readLine()) != null) {
//		response.append(inputLine);
//	}
//	in.close();
//
//	System.out.println(response.toString());
//} else {
//	System.out.println("GET request failed, response code: " + responseCode);
//}
}
