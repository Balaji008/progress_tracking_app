package com.todoapplication.util;

public class ErrorMessageConstants {

	public static final String USER_NOT_FOUND = "User not found";
	public static final String EMAIL_ALREADY_EXISTS = "Email already exists";
	public static final String INVALID_EMAIL_OR_PASSWORD = "Invalid email or password";
	public static final String INVALID_EMAIL = "Invalid email";
	public static final String INVALID_EMAIL_OR_OTP = "Invalid email or otp";

	public static final String ROLE_NOT_FOUND = "Role not found";
	public static final String ROLE_ALREADY_EXISTS = "Role already exists";

	public static final String TASK_NOT_FOUND = "Task not found";
	public static final String TASK_ALREADY_EXISTS = "Task already exists";

	public static final String PERMISSION_NOT_FOUND = "Permission not found";
	public static final String PERMISSION_ALREADY_EXISTS = "Permission already exists";

	public static final String USER_ROLE_NOT_FOUND = "User role not found";
	public static final String USER_ROLE_ALREADY_EXISTS = "Role is already assigned to user";

	public static final String ROLE_PERMISSION_NOT_FOUND = "Role permission not found";
	public static final String ROLE_PERMISSION_ALREADY_EXISTS = "Permission is already assigned to role";

	public static final String USER_TASK_NOT_FOUND = "User task not found";
	public static final String USER_TASK_ALREADY_EXISTS = "Task is already assigned to user";
	public static final String USER_TASK_STATUS_ALREADY_COMPLETED = "Task is already completed";

	public static final String INVALID_DATA = "Invalid data";
	public static final String DATA_ALREADY_PRESENT = "Data already present";

	public static final String INVALID_TOKEN = "Invalid token";
	public static final String TOKEN_EXPIRED = "Token expired";

	public static final String URL_NOT_FOUND = "Url not found";
	public static final String INVALID_URL = "Invalid url";

	public static final String INVALID_FILE = "Invalid file format";

	public static final String OTP_NOT_FOUND = "OTP NOT FOUND";
	public static final String INVALID_OTP = "Invalid otp";
}
