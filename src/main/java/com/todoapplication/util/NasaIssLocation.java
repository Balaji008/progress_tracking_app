package com.todoapplication.util;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NasaIssLocation {

	private String message;

	@JsonProperty("iss_position")
	private IssPosition issPosition;

	private Long timestamp;

	@Override
	public String toString() {
//		return "NasaIssLocation [message=" + message + ", latitude=" + issPosition.getLatitude() + ", longitude=" + issPosition.getLongitude() + ", timestamp=" + timestamp
//				+ "]";

		return "{\"latitude\":\"" + issPosition.getLatitude() + "\",\"longitude\":\"" + issPosition.getLongitude()
				+ "\"}";
	}

	public NasaIssLocation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NasaIssLocation(String message, IssPosition issPosition, Long timestamp) {
		super();
		this.message = message;
		this.issPosition = issPosition;
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public IssPosition getIssPosition() {
		return issPosition;
	}

	public void setIssPosition(IssPosition issPosition) {
		this.issPosition = issPosition;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

}
