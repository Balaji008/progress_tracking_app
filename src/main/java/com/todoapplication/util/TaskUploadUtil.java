package com.todoapplication.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import com.todoapplication.dto.TaskDto;

public class TaskUploadUtil {

	public static boolean checkExcelFormat(MultipartFile file) {

		String contentType = file.getContentType();

		boolean flag = contentType.equalsIgnoreCase("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
				? true
				: false;

		return flag;

	}

	public static List<TaskDto> convertExcelToListOfTasks(InputStream inputStream) {
		List<TaskDto> tasks = new ArrayList<>();

		try (Workbook workbook = new XSSFWorkbook(inputStream);) {

			Sheet sheet = workbook.getSheetAt(0);

			for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
				Row row = sheet.getRow(rowIndex);
				TaskDto task = new TaskDto();
				task.setTaskName(row.getCell(0).getStringCellValue());
				tasks.add(task);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return tasks;
	}

}
