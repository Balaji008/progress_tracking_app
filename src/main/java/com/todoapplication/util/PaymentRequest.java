package com.todoapplication.util;

public class PaymentRequest {

	private int amount;
	private String receiptId;

	public PaymentRequest() {
	}

	public PaymentRequest(int amount, String receiptId) {
		this.amount = amount;
		this.receiptId = receiptId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}

	// getters and setters
}
