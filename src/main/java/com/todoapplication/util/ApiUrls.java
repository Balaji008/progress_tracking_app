package com.todoapplication.util;

public class ApiUrls {

	public static final String AUTH = "/auth";
	public static final String LOGIN = AUTH + "/login";
	public static final String REGISTER = AUTH + "/register";
	public static final String GENERATE_ACCESS_TOKEN = "/refreshToken";
	public static final String USERS = "/users";
	public static final String ROLES = "/roles";
	public static final String PERMISSIONS = "/permissions";
	public static final String TASKS = "/tasks";
	public static final String USER_ROLES = "/userRole";
	public static final String ROLE_PERMISSION = "/rolePermissions";
	public static final String USER_TASKS = "/userTasks";

	public static final String[] API_URLS_WITHOUT_HEADER = { LOGIN, REGISTER, GENERATE_ACCESS_TOKEN };
}
