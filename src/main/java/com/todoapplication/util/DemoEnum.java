package com.todoapplication.util;

public enum DemoEnum {

	VALUE1(1), VALUE2(2), VALUE3(3);

	private final int id;

	DemoEnum(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
}
