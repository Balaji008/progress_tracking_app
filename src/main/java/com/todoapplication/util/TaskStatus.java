package com.todoapplication.util;

public enum TaskStatus {

	TO_DO(1), IN_PROGRESS(2), COMPLETED(3), ALREADY_COMPLETED(4);

	private final int id;

	TaskStatus(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

}
