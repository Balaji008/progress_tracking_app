package com.todoapplication.util;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QuoteOfTheDay {

	@JsonProperty("qotd_date")
	private String qotdDate;
	private Quote quote;

	public QuoteOfTheDay() {
		super();
		// TODO Auto-generated constructor stub
	}

	public QuoteOfTheDay(String qotdDate, Quote quote) {
		super();
		this.qotdDate = qotdDate;
		this.quote = quote;
	}

	// getters and setters
	public String getQotdDate() {
		return qotdDate;
	}

	public void setQotdDate(String qotdDate) {
		this.qotdDate = qotdDate;
	}

	public Quote getQuote() {
		return quote;
	}

	public void setQuote(Quote quote) {
		this.quote = quote;
	}

}
