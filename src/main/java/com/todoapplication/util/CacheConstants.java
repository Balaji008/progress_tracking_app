package com.todoapplication.util;

public class CacheConstants {

	public static final String USER = "User ";
	public static final String PERMISSIONS = "Permissions";
	public static final String USER_TASK_LIST = "userTaskList";
}
