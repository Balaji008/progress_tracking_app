package com.todoapplication.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.web.multipart.MultipartFile;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import com.todoapplication.dto.EmployeeDto;

public class EmployeeBulkUploadUtil {

	public static boolean checkCsvFormat(MultipartFile file) {
		String contentType = file.getContentType();

		boolean flag = contentType.equalsIgnoreCase("text/csv") ? true : false;

		return flag;
	}

	public static List<EmployeeDto> csvToListOfEmployee(MultipartFile file) {
		List<EmployeeDto> tasks = new ArrayList<>();
		try (CSVReader reader = new CSVReader(new InputStreamReader(file.getInputStream()))) {
			@SuppressWarnings("unused")
			String[] header = reader.readNext(); // skip the first line (header)
			String[] row;
			while ((row = reader.readNext()) != null) {
				EmployeeDto data = new EmployeeDto();
				data.setFirstName(row[1]);
				data.setLastName(row[2]);
				DateTimeFormatter formatter = new DateTimeFormatterBuilder().parseCaseInsensitive()
						.appendPattern("dd-MMM-yy").toFormatter(Locale.ENGLISH);
				LocalDate date = LocalDate.parse(row[5], formatter);
				data.setHireDate(date);
				data.setJobId(row[6]);
				data.setSalary(Double.parseDouble(row[7]));
				tasks.add(data);
			}
			return tasks;
		} catch (IOException e) {

			e.printStackTrace();
			return null;
		} catch (CsvValidationException e) {

			e.printStackTrace();
			return null;
		} catch (NumberFormatException e) {

			e.printStackTrace();
			return null;
		} catch (Exception e) {
			return null;
		}

	}
}
