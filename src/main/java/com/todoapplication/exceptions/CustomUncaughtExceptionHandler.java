package com.todoapplication.exceptions;

import java.lang.Thread.UncaughtExceptionHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomUncaughtExceptionHandler implements UncaughtExceptionHandler {

	public static final Logger Log = LoggerFactory.getLogger(CustomUncaughtExceptionHandler.class);

	@Override
	public void uncaughtException(Thread thread, Throwable throwable) {

	}
}
