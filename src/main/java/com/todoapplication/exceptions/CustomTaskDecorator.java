package com.todoapplication.exceptions;

import org.springframework.core.task.TaskDecorator;

public class CustomTaskDecorator implements TaskDecorator {

	@Override
	public Runnable decorate(Runnable runnable) {
		return () -> {
			try {
				runnable.run();
			} catch (Exception e) {
				// Handle exception here
				e.getMessage();
			}
		};
	}
}
